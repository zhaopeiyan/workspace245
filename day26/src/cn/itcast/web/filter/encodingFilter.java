package cn.itcast.web.filter;

import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet Filter implementation class encodingFilter
 */
public class encodingFilter implements Filter {

    /**
     * Default constructor. 
     */
    public encodingFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}
	/**
	 * 使用动态代理处理中文乱码
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		//1、强转(创建被代理对象)
		final HttpServletRequest req = (HttpServletRequest) request;
		//2、创建代理对象
		/**
		 * 第一个参数：通过被代理对象获得的类加载器
		 * 第二个参数：通过被代理对象获得接口
		 * 第三个接口：代理对象处理类(接口)
		 */
		HttpServletRequest strongReq = (HttpServletRequest)Proxy.newProxyInstance(req.getClass().getClassLoader(), req.getClass().getInterfaces(), new InvocationHandler(){
			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
				// 3、对指定方法进行加强
				if("getParameter".equalsIgnoreCase(method.getName())){
					//4、对不同的请求做不同处理
					if("get".equalsIgnoreCase(req.getMethod())){
						//5、调用原有的方法获得请求参数
						String parameter = (String) method.invoke(req, args);
						//6、解决乱码
						parameter = new String(parameter.getBytes("iso8859-1"),"utf-8");
						return parameter;
					}else if("post".equalsIgnoreCase(req.getMethod())){
						//7、解决post请求乱码问题
						req.setCharacterEncoding("utf-8");
					}
					//8.对其它方式进行处理，直接调用原有的方法
					return method.invoke(req, args);
				}
				//9.处理其它方法
				return method.invoke(req, args);
			}
		});
		//10、放行
		chain.doFilter(strongReq, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
