package cn.itcast.test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import cn.itcast.domain.NormalPerson;
import cn.itcast.interfaces.IPerson;

public class PersonProxy {
	public static void main(String[] args) {
		//1、创建被增强对象
		final IPerson person = new NormalPerson();
		//2、动态创建增强对象
		IPerson strongPerson = (IPerson) Proxy.newProxyInstance(person.getClass().getClassLoader(), person.getClass().getInterfaces(), new InvocationHandler(){

			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
				// TODO Auto-generated method stub
				/*if(method.getName().equals("run")){
					System.out.print("老司机");
					method.invoke(person, args);
					
				}*/
				method.invoke(person, args);
				System.out.print("飞");
				return "你好";
			}
		});
		/*person.run();
		strongPerson.run();*/
		strongPerson.eat("臭豆腐");
		String str = strongPerson.sleep();
		System.out.println(str);
	}
}
