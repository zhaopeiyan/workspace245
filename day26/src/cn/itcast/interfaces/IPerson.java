package cn.itcast.interfaces;

public interface IPerson {
	void run();
	String sleep();
	void eat(String food);
}
