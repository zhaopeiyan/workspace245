package cn.itcast.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

import cn.itcast.domain.User;

public class Demo {
	
	@Test
	public void test1 (){
		//1.加载配置文件
		Configuration cfg = new Configuration().configure();
		//2.创建一个SessionFactory
		SessionFactory sessionFactory = cfg.buildSessionFactory();
		//3.创建Session(会话)对象,类似于Connection对象
		Session session = sessionFactory.openSession();
		//4.开启事务
		Transaction ts = session.beginTransaction();
		//5.执行相关操作
		User user = new User();
		user.setUsername("James");
		user.setPassword("777");
		
		session.save(user);
//		session.update(user);
		//6.事务提交
		ts.commit();
		//7.释放资源
		session.close();
	}
}
