package cn.itcast.test;

import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.junit.Test;

public class solrJ {

	/**
	 * solrJ简单查询
	 * 
	 * @throws Exception
	 */
	@Test
	public void searchIndex() throws Exception {
		// 创建solr服务对象
		String url = "http://localhost:8080/solr/products";
		HttpSolrServer httpSolrServer = new HttpSolrServer(url);

		// 创建查询对象
		SolrQuery solrQuery = new SolrQuery();
		// 设置查询表达式*:*表示查询所有
		solrQuery.setQuery("*:*");
		// 执行查询,返回response
		QueryResponse response = httpSolrServer.query(solrQuery);
		// 获取结果集
		SolrDocumentList solrDocumentList = response.getResults();
		// 输出文档总数
		System.out.println("命中文档总数是:" + solrDocumentList.getNumFound());
		for (SolrDocument s : solrDocumentList) {
			System.out.println(s.get("id"));
			System.out.println(s.get("product_name"));
			System.out.println(s.get("product_price"));
		}
	}

	
	@Test
	public void queryIndex() throws Exception {
		// 1.指定远程服务器索引库地址
		String url = "http://localhost:8080/solr/products";
		// 2.链接solr远程索引库
		HttpSolrServer httpSolrServer = new HttpSolrServer(url);
		// 3.创建封装查询条件对象
		SolrQuery solrQuery = new SolrQuery();
		// 4.设置主查询条件
		solrQuery.setQuery("台灯");

		// 5.设置过滤查询条件
		// a.查询商品类别是
//		solrQuery.addFilterQuery("product_catalog_name:时尚卫浴");
		// b.商品价格在1-20
		solrQuery.addFilterQuery("product_price:[* TO 20]");

		// 6.设置排序
		solrQuery.setSort("product_price", ORDER.asc);

		// 7.分页
		solrQuery.setStart(1);
		solrQuery.setRows(20);

		// 8.设置过滤字段 多个字段之间可以使用逗号","和空格分隔
		// solrQuery.setFields("product_name,product_price");
		// solrQuery.setFields("product_name product_price");

		// 9.设置高亮
		// 开启高亮
		solrQuery.setHighlight(true);
		// 指定字段高亮显示
		solrQuery.addHighlightField("product_name");
		// 指定高亮前缀
		solrQuery.setHighlightSimplePre("<font color='red'>");
		// 指定高亮后缀
		solrQuery.setHighlightSimplePost("</font>");

		// 10.设置默认查询字段 默认查询通常和主查询条件结合使用
		solrQuery.set("df", "product_keywords");

		// 使用solrServer执行查询
		QueryResponse response = httpSolrServer.query(solrQuery);
		// 获取结果集对象
		SolrDocumentList results = response.getResults();
		// 获取命中总记录数
		long numFound = results.getNumFound();
		System.out.println("命中总记录数:" + numFound);

		// 循环获取每一个文档对象
		for (SolrDocument s : results) {
			String id = (String) s.get("id");
			System.out.println("文档id:" + id);
			String productName = (String) s.get("product_name");

			// 获取高亮
			Map<String, Map<String, List<String>>> highlighting = response.getHighlighting();
			// 第一个map的key就是id
			Map<String, List<String>> map = highlighting.get(id);
			// 第二个map的key就是域名
			List<String> list = map.get("product_name");

			if (list != null && list.size() > 0) {
				productName = list.get(0);
			}

			System.out.println("商品名称：" + productName);

			Float productPrice = (Float) s.get("product_price");
			System.out.println("商品价格：" + productPrice);
			String productDescription = (String) s.get("product_description");
			System.out.println("商品描述：" + productDescription);
			String productPicture = (String) s.get("product_picture");
			System.out.println("商品图片：" + productPicture);
			String productCatalogName = (String) s.get("product_catalog_name");
			System.out.println("商品类别：" + productCatalogName);
		}
	}

}
