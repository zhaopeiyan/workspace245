package cn.itcast.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.itcast.pojo.Result;
import cn.itcast.service.SearchService;

@Controller
public class SearchController {

	@Autowired
	private SearchService searchService;
	
	/**
	 * 实现搜索功能
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/search")
	public String queryProduct(Model model,
			String queryString,
			String catalog_name,
			String price,
			@RequestParam(defaultValue = "1")String sort,
			@RequestParam(defaultValue = "1")Integer page,
			Integer rows) throws Exception{
		
		System.err.println(queryString);
		
		//根据条件搜索
		Result result = searchService.queryProduct(queryString,catalog_name,price,sort,page);
		
		//把结果集放到模型中
		model.addAttribute("result",result);
		
		//搜索条件数据回显
		model.addAttribute("queryString", queryString);
		model.addAttribute("catalog_name", catalog_name);
		model.addAttribute("price", price);
		model.addAttribute("sort", sort);
		return "/product_list";
	}
	
}
