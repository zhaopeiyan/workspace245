package cn.itcast.service;

import cn.itcast.pojo.Result;

public interface SearchService {

	/**
	 * 条件查询
	 * @param queryString
	 * @param catalog_name
	 * @param price
	 * @param sort
	 * @param page
	 * @return
	 * @throws Exception 
	 */
	Result queryProduct(String queryString, String catalog_name, String price, String sort, Integer page) throws Exception;

}
