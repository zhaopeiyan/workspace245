package cn.itcast.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.itcast.pojo.Product;
import cn.itcast.pojo.Result;
import cn.itcast.service.SearchService;

@Service
public class SearchServiceImpl implements SearchService {

//	@Autowired
//	private HttpSolrServer httpSolrServer;
	@Autowired
	private SolrServer solrServer;
	
	@Override
	public Result queryProduct(String queryString, String catalog_name, String price, String sort, Integer page) throws Exception {

		//创建SolrQuery对象
		SolrQuery solrQuery = new SolrQuery();
		
		//设置查询关键词
		if(queryString != null && !("".equals(queryString))){
			solrQuery.setQuery(queryString);
		}else{
			solrQuery.setQuery("*:*");
		}
		
		//设置默认域
		solrQuery.set("df", "product_keywords");
		
		//设置商品类名过滤条件
		//设置商品分类名称
		if(catalog_name != null && !("".equals(catalog_name))){
			catalog_name = "product_catalog_name:"+catalog_name;
		}
		
		//设置商品价格
		if(price != null && !("".equals(price))){
			String[] split = price.split("-");
			if(split != null && split.length == 2){
				price = "product_price:[" + split[0] + " TO " + split[1] + "]";
			}
		}
		
		solrQuery.setFilterQueries(catalog_name,price);
		
		//商品排序
		if("1".equals(sort)){
			solrQuery.setSort("product_price", ORDER.asc);
		}else{
			solrQuery.setSort("product_price", ORDER.desc);
		}
		
		//设置分页
		if(page == null){
			page = 1;
		}
		solrQuery.setStart((page - 1) * 20);
		solrQuery.setRows(20);
		
		//设置高亮
		solrQuery.setHighlight(true);
		solrQuery.addHighlightField("product_name");
		solrQuery.setHighlightSimplePre("<font color='red'>");
		solrQuery.setHighlightSimplePost("</font>");
		
		
		Result pageBean = new Result();
		
		//查询数据
		QueryResponse response = solrServer.query(solrQuery);
		
		//查询总数
		SolrDocumentList results = response.getResults();
		Long total = results.getNumFound();
		
		pageBean.setRecordCount(total);
		
		//解析结果集,存放到Product中
		List<Product> products = new ArrayList<>();
		for (SolrDocument solrDocument : results) {
			Product product = new Product();
			//product.setPid(solrDocument.get("id").toString());
			
			String id = (String) solrDocument.get("id");
			product.setPid(id);
			
			String productName = (String) solrDocument.get("product_name");
			
			//获取高亮数据
			Map<String, Map<String, List<String>>> map = response.getHighlighting();
			
			
			//第一个map的key是商品id
			Map<String, List<String>> map2 = map.get(id);
			
			//商品标题,设置高亮
			List<String> list = map2.get("product_name");
			if(list != null && list.size() > 0){
				productName = list.get(0);
			}
			/*else{
				product.setName(solrDocument.get("product_name").toString());
			}*/
			
			product.setName(productName);
			
			product.setPrice((Float) solrDocument.get("product_price"));
			//商品价格
			//Double priceD = (Double) solrDocument.get("product_price");
			//product.setPrice(priceD);
			
			//商品图片
			String priture = (String) solrDocument.get("product_picture");
			product.setPicture(priture);
			
			products.add(product);
			
		}
		
		//封装返回对象
		Result result = new Result();
		result.setCurPage(page);
		result.setRecordCount(total);
		result.setProductList(products);
		
		//总页数计算公式
		result.setPageCount((int)(total + 20 -1)/20);
			
		return result;
	}

}













