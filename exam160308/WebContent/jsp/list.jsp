<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>后台管理系统</title>
<link href="${pageContext.request.contextPath }/images/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/Css/style.css" />
    <script type="text/javascript" src="${pageContext.request.contextPath }/Js/jquery.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/Js/bootstrap.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/Js/ckform.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/Js/common.js"></script>
<style type="text/css">
body {
	background:#FFF
}
</style>
</head>
<body>
<form class="form-inline definewidth m20" action="${pageContext.request.contextPath }/user" method="post">
<input type="hidden" name="method" value="findAll">
    用户名称：
    <input type="text" name="username" id="username"class="abc input-default" placeholder="" value="">&nbsp;&nbsp; 
	联系电话:
	<input type="text" name="menuname" id="menuname"class="abc input-default" placeholder="" value="">&nbsp;&nbsp; 
	用户地址:
	<input type="text" name="menuname" id="menuname"class="abc input-default" placeholder="" value="">&nbsp;&nbsp; 
    <button type="submit" class="btn btn-primary">查询</button>
</form>
<table class="table table-bordered table-hover definewidth m10">${msg }
    <thead>
    <tr>
        <th>用户名称</th>
        <th>用户地址</th>
        <th>联系电话</th>
        <th>备注</th>
        <th>所属部门</th>
        <th>操作</th>
    </tr>
    </thead>
        <tr>
        <c:forEach items="${plist }" var="p">
        <tr>
         <td>${p.username }</td>
                <td>${p.address}</td>
                <td>${p.telephone }</td>
                <td>${p.remake }</td>
                <td>${p.department }</td>
                <td><a href="javascript:void(0)" onclick="editUserById('${p.id}')">编辑</a>|<a href="javascript:void(0)" onclick="deleteUser('${p.id}')">删除</a></td>
         <tr>
        </c:forEach> 
            </tr></table>
</body>
<script type="text/javascript">
function deleteUser(id){
	if(confirm("您确定删除该用户吗？")){
		location.href="${pageContext.request.contextPath}/deleteUserById?id="+id;
	}
}

function editUserById(id){
	location.href="${pageContext.request.contextPath}/findUserById?id="+id;
}


</script>
</html>
