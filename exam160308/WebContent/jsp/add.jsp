<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>用户管理系统</title>
<link href="${pageContext.request.contextPath }/bootstrap2.3.2/css/bootstrap.min.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath }/images/style.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath }/images/Common.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath }/images/Index2.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/Css/style.css" />
    <script type="text/javascript" src="${pageContext.request.contextPath }/Js/jquery.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/Js/bootstrap.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/Js/ckform.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/Js/common.js"></script>
<style type="text/css">
body {
	background:#FFF
}
</style>
<script>
    $(function (){      
    	
		$('#backid').click(function(){
				location.href="${pageContext.request.contextPath}/findAll";
		 });
		$("#username").blur(function (){
			$("h5").remove();
			$("#submit").removeAttr("disabled");
			var username = $(this).val();
			if(username == ""){
				$("#td_username").append("<h5 class='wrong' style='color:red'>用户名不能为空</h5>");
				$("#submit").prop("disabled","disabled");
			}
		});
		$("#telephone").blur(function (){
			$("h5").remove();
			$("#submit").removeAttr("disabled");
			var username = $(this).val();
			if(username == ""){
				$("#td_telephone").append("<h5 class='wrong' style='color:red'>电话不能为空</h5>");
				$("#submit").prop("disabled","disabled");
			}
		});
    });
</script>
</head>
<body>
<form id="form" action="${pageContext.request.contextPath }/addUserServlet" method="post" class="definewidth m20" target="right">
<input type="hidden" name="method" value="addUser">${msg }
<table class="table table-bordered table-hover m10">  
    <tr>
        <td class="tableleft">用户名称</td>
        <td><input type="text"  name="username" id="username" value="${user.username }"/></td>
        <td id="td_username"><span ></span></td>
    </tr>
    <tr>
        <td class="tableleft">用户地址</td>
        <td><input type="text" name="address" value="${user.address }"/></td>
        <td></td>
    </tr>
    <tr>
        <td class="tableleft">联系电话</td>
        <td><input type="text" name="telephone" id="telephone" value="${user.telephone }"/></td>
         <td id="td_telephone"></td>
    </tr>
    <tr>
        <td class="tableleft">备注</td>
        <td><input type="text" name="remake" value="${user.remake }"/></td>
        <td></td>
    </tr>
	<tr>
        <td width="10%" class="tableleft">所属部门</td>
        <td>
            <input type="text" name="department value="${user.department }">
        </td>
        <td></td>
    </tr>
    <tr>
        <td class="tableleft"></td>
        <td>
            <button type="submit" class="btn btn-primary" type="button" disabled="disabled">保存</button> &nbsp;&nbsp;<button type="button" class="btn btn-success" name="backid" id="backid">返回列表</button>
        </td>
        <td></td>
    </tr>
</table>
</form>

</body>
</html>
