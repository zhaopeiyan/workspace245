<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>修改用户</title>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/Css/style.css" />
    <script type="text/javascript" src="${pageContext.request.contextPath }/Js/jquery.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/Js/bootstrap.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/Js/ckform.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/Js/common.js"></script>


<script>
    $(function () {       
		$('#backid').click(function(){
				location.href="${pageContext.request.contextPath}/findAll";
		 });
		
		$("#username").blur(function (){
			$("h5").remove();
			$("#submit").removeAttr("disabled");
			var username = $(this).val();
			if(username == ""){
				$("#td_username").append("<h5 class='wrong' style='color:red'>用户名不能为空</h5>");
				$("#submit").prop("disabled","disabled");
			}
		});
		$("#telephone").blur(function (){
			$("h5").remove();
			$("#submit").removeAttr("disabled");
			var username = $(this).val();
			if(username == ""){
				$("#td_telephone").append("<h5 class='wrong' style='color:red'>电话不能为空</h5>");
				$("#submit").prop("disabled","disabled");
			}
		});
    });
</script>

<style type="text/css">
body {
	background:#FFF
}
</style>
</head>
<body>
<form action="${pageContext.request.contextPath }/modifyUser" method="post" class="definewidth m20">
<input type="hidden" name="method" value="editUser">
<input type="hidden" name="id" value="${bean.id }">
<table class="table table-bordered table-hover m10">   
    <tr>
        <td class="tableleft">用户名称</td>
        <td><input type="text" name="username"  id="username" value="${bean.username }"/></td>
         <td id="td_username"><span ></span></td>
    </tr>
    <tr>
        <td class="tableleft">用户地址</td>
        <td><input type="text" name="address" value="${bean.address }"/></td>
        <td></td>
    </tr>
    <tr>
        <td class="tableleft">联系电话</td>
        <td><input type="text" name="telephone"  id="telephone" value="${bean.telephone }"/></td>
        <td id="td_telephone"></td>
    </tr>
    <tr>
        <td class="tableleft">备注</td>
        <td><input type="text" name="remake" value="${bean.remake }"/></td>
        <td></td>
    </tr>
	<tr>
        <td width="10%" class="tableleft">所属部门</td>
        <td>
        <input type="text" name="department" value="${bean.department }">
        </td>
        <td></td>
    </tr>
    <tr>
        <td class="tableleft"></td>
        <td>
            <button type="submit" id="submit" class="btn btn-primary" type="button" disabled="disabled" >修改</button> &nbsp;&nbsp;<button type="button" class="btn btn-success" name="backid" id="backid">返回列表</button>
        </td>
    </tr>
</table>
</form>
</body>
</html>
