package cn.itcast.user.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import cn.itcast.user.domain.User;
import cn.itcast.user.service.UserService;

public class addUserServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		User user = new User();
		//1、获取用户输入数据,封装到user
		try {
			BeanUtils.populate(user, request.getParameterMap());
			String department = request.getParameter("department");
			user.setDepartment(department);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("数据封装异常" + e);
		}
		//2、没有要单独封装的数据，调用service层，处理业务逻辑
		UserService us = new UserService();
		int i = us.addUser(user);
		if(i > 0){
			request.getRequestDispatcher("/findAll").forward(request, response);
		}else{
			request.setAttribute("user", user);
			request.setAttribute("msg", "<h5 style='color:red'>添加数据失败</h5>");
			request.getRequestDispatcher("jsp/add.jsp").forward(request, response);
		}
		
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}