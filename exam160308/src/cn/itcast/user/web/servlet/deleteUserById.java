package cn.itcast.user.web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.user.domain.User;
import cn.itcast.user.service.UserService;

public class deleteUserById extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		//1、获取要删除数据的id
		int id = Integer.parseInt(request.getParameter("id"));
		//2、调用service层方法
		UserService us = new UserService();
		User existUser = (User) request.getSession().getAttribute("existUser");
		int i = existUser.getId();
		if(id == i){
			request.setAttribute("msg", "<h5 style='color:red'>当前登陆用户不能删除！</h5>");
			request.getRequestDispatcher("/findAll").forward(request, response);
		}else{
			us.deleteUserById(id);
			request.getRequestDispatcher("/findAll").forward(request, response);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}