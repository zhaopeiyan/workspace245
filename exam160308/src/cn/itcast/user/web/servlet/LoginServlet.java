package cn.itcast.user.web.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import cn.itcast.user.domain.User;
import cn.itcast.user.service.UserService;

public class LoginServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		User user = new User();
		//1、获取用户输入的数据
		try {
			BeanUtils.populate(user, request.getParameterMap());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("封装数据异常"+e);
		}
		//2、调用service层
		UserService us = new UserService();
		User existUser = us.login(user);
		if(existUser != null){
			request.getSession().setAttribute("existUser", existUser);
			request.getRequestDispatcher("jsp/index.jsp").forward(request, response);
		}else{
			request.setAttribute("msg", "<h5 style='color:red'>用户名或密码错误，请重新登陆</h5>");
			request.getRequestDispatcher("jsp/login.jsp").forward(request, response);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}