package cn.itcast.user.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.user.domain.User;
import cn.itcast.user.service.UserService;

public class findUserById extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		//1、获取用户id
		int id = Integer.parseInt(request.getParameter("id"));
		//2、调用service层,查询用户信息进行回显
		UserService us = new UserService();
		User user = us.findUserById(id);
		request.setAttribute("bean", user);
		request.getRequestDispatcher("/jsp/edit.jsp").forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}