package cn.itcast.user.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.user.domain.User;
import cn.itcast.user.service.UserService;

public class findAll extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		//1、查询所有用户信息，不需要参数
		UserService us = new UserService();
		List<User> list = us.findAll();
		if(list.size() > 0 && list != null){
			request.setAttribute("plist", list);
			request.getRequestDispatcher("jsp/list.jsp").forward(request, response);
		}else{
			request.setAttribute("msg", "<h5 style='color:red'>查询用户信息异常</h5>");
			request.getRequestDispatcher("jsp/index.jsp").forward(request, response);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}