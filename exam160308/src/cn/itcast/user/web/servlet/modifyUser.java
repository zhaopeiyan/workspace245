package cn.itcast.user.web.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import cn.itcast.user.domain.User;
import cn.itcast.user.service.UserService;

public class modifyUser extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		//1、获取数据
		User user = new User();
		try {
			BeanUtils.populate(user, request.getParameterMap());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("数据封装异常" + e);
		}
		//2、调用service层
		UserService us = new UserService();
		us.modifyUserById(user);
		request.getRequestDispatcher("/findAll").forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}