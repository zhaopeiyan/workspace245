package cn.itcast.user.dao;

import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import cn.itcast.user.domain.User;
import cn.itcast.user.utils.C3P0Utils;

public class UserDao {

	/**
	 * 用户登录
	 * @param user
	 * @return
	 */
	public User login(User user) {
		// TODO Auto-generated method stub
		User existUser = null;
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "select * from m_user where username=? and password=?";
		try {
			existUser = (User) qr.query(sql, new BeanHandler<>(User.class), user.getUsername(),user.getPassword());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("用户登录异常" + e);
		}
		return existUser;
	}

	/**
	 * 添加用户
	 * @param user
	 */
	public int addUser(User user) {
		// TODO Auto-generated method stub
		int i = 0;
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "insert into m_user values(null,?,null,?,?,?,?)";
		Object[] params = {user.getUsername(),user.getAddress(),user.getTelephone(),user.getRemake(),user.getDepartment()};
		try {
			i = qr.update(sql, params);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("添加用户异常" + e);
		}
		return i;
	}

	/**
	 * 查询所有用户
	 * @return
	 */
	public List<User> findAll() {
		// TODO Auto-generated method stub
		List<User> list = null;
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "select * from m_user ";
		try {
			list = qr.query(sql, new BeanListHandler<>(User.class));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("查询所有用户异常" + e);
		}
		return list;
	}

	/**
	 * 根据id删除用户信息
	 * @param id
	 */
	public void deleteUserById(int id) {
		// TODO Auto-generated method stub
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "delete from m_user where id=?";
		try {
			qr.update(sql, id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("根据id删除用户信息异常" + e);
		}
	}

	public User findUserById(int id) {
		// TODO Auto-generated method stub
		User user = null;
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "select * from m_user where id=?";
		try {
			user = qr.query(sql, new BeanHandler<>(User.class), id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("" + e);
		}
		return user;
	}

	/**
	 * 修改用户信息
	 * @param user
	 */
	public void midifyYserById(User user) {
		// TODO Auto-generated method stub
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "update m_user set username=?,address=?,telephone=?,remake=?,department=? where id=?";
		Object[] params = {user.getUsername(),user.getAddress(),user.getTelephone(),user.getRemake(),user.getDepartment(),user.getId()};
		try {
			qr.update(sql, params);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("" + e);
		}
	}
}

