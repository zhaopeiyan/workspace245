package cn.itcast.user.service;

import java.util.List;

import cn.itcast.user.dao.UserDao;
import cn.itcast.user.domain.User;

public class UserService {

	public User login(User user) {
		// TODO Auto-generated method stub
		UserDao ud = new UserDao();
		return ud.login(user);
	}

	public int addUser(User user) {
		// TODO Auto-generated method stub
		UserDao ud = new UserDao();
		return ud.addUser(user);
	}

	public List<User> findAll() {
		// TODO Auto-generated method stub
		UserDao ud = new UserDao();
		return ud.findAll();
	}

	public void deleteUserById(int id) {
		// TODO Auto-generated method stub
		UserDao ud = new UserDao();
		ud.deleteUserById(id);
				
	}

	public User findUserById(int id) {
		// TODO Auto-generated method stub
		UserDao ud = new UserDao();
		return ud.findUserById(id);
	}

	public void modifyUserById(User user) {
		// TODO Auto-generated method stub
		UserDao ud = new UserDao();
		ud.midifyYserById(user);
	}

}
