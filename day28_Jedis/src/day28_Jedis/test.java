package day28_Jedis;

import org.junit.Test;

import cn.itcast.utils.JedisUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class test {

	/**
	 * 单实例链接
	 */
	@Test
	public void getJedis() {
		Jedis jedis = new Jedis("192.168.190.128", 6379);
		jedis.set("name", "赵培焱");
		String set = jedis.get("name");
		System.out.println(set);
		jedis.close();
	}

	/**
	 * 连接池链接
	 */
	@Test
	public void testJedisPool() {
		// 1.1、获取连接池配置对象，设置配置项
		JedisPoolConfig config = new JedisPoolConfig();
		// 1.2、设置最大连接数
		config.setMaxTotal(30);
		// 1.3、最大空闲连接数
		config.setMaxIdle(10);
		// 2、获得连接池
		JedisPool jedisPool = new JedisPool(config, "192.168.190.128", 6379);
		// 3、获得核心对象
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			// 4、设置数据
			jedis.set("itcast", "lebron.james");
			// 5、获得数据
			String name = jedis.get("itcast");
			System.out.println(name);
		} catch (Exception e) {
			// TODO: handle exception
			throw new RuntimeException("" + e);
		} finally {
			if (jedis != null) {
				jedis.close();
			}
			// 虚拟机关闭时，释放pool资源
			if (jedisPool != null) {
				jedisPool.close();
			}
		}
	}

	/**
	 * 利用工具类获得jedis链接对象
	 */
	@Test
	public void testJedisTool(){
		Jedis jedis = JedisUtils.getJedis();
		String itcast = jedis.get("itcast");
		System.out.println(itcast);
	}

}
