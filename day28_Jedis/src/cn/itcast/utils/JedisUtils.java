package cn.itcast.utils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisUtils {
	
	/**
	 * 使用池获取Jedis链接
	 */
	public static  Jedis getJedis(){
		//获取到连接池配置对象，设置连接池的所需的各项属性
		JedisPoolConfig jpc = new JedisPoolConfig();
		jpc.setMaxTotal(100);
		jpc.setMaxIdle(10);
		//获取连接池对象
		JedisPool jp = new JedisPool(jpc,"192.168.190.128",6379);
		Jedis jedis = jp.getResource();
		return jedis;
	}
}
