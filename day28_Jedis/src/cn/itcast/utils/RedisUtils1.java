package cn.itcast.utils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisUtils1 {
	
	/**
	 * 通过Jedis连接池获取连接对象
	 */
	public static Jedis getJedis(){
		//获取连接池配置对象
		JedisPoolConfig jpc = new JedisPoolConfig();
		jpc.setMaxTotal(100);
		jpc.setMaxIdle(10);
		//获取连接池对象
		JedisPool jp = new JedisPool(jpc,"192.168.190.128",6379);
		//获取jedis对象
		Jedis jedis = jp.getResource();
		return jedis;
	}
}
