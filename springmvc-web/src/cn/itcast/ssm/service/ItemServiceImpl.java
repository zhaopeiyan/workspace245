package cn.itcast.ssm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.itcast.ssm.mapper.ItemsMapper;
import cn.itcast.ssm.pojo.Items;
import cn.itcast.ssm.pojo.QueryVo;

@Service
public class ItemServiceImpl implements ItemService {

	@Autowired
	private ItemsMapper itemsMapper;
	
	@Override
	public List<Items> queryItemList() {
		//从数据库查询商品信息
		List<Items> list = this.itemsMapper.selectByExample(null);
		return list;
	}

	@Override
	public Items queryItemById(int id) {
		Items items = itemsMapper.selectByPrimaryKey(id);
		return items;
	}

	@Override
	public void updateItemById(Items items) {
		itemsMapper.updateByPrimaryKeySelective(items);
	}

	@Override
	public List<Items> queryItemListByQueryVo(QueryVo queryVo) {
//		itemsMapper.se
		return null;
	}


}
