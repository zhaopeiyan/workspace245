package cn.itcast.ssm.service;

import java.util.List;

import cn.itcast.ssm.pojo.Items;
import cn.itcast.ssm.pojo.QueryVo;

public interface ItemService {
	
	/**
	 * 查询所有商品信息
	 * @return
	 */
	List<Items> queryItemList();

	/**
	 * 根据商品id查询商品
	 * @param id
	 * @return
	 */
	Items queryItemById(int id);
	
	/**
	 * 根据id更新商品
	 * @param items
	 */
	void updateItemById(Items items);
	
	/**
	 * 根据条件查询
	 * @return
	 */
	List<Items> queryItemListByQueryVo(QueryVo queryVo);

	
}
