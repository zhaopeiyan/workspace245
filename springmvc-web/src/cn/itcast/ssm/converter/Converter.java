package cn.itcast.ssm.converter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Converter implements org.springframework.core.convert.converter.Converter<String, Date> {

	@Override
	public Date convert(String source) {
		try { // 把字符串转换为日期类型
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = simpleDateFormat.parse(source);
			return date;
		} catch (Exception e) { // TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * @Override public Float convert(Float arg0) { // TODO Auto-generated
	 * method stub float floatValue = arg0; return floatValue; }
	 */
}
