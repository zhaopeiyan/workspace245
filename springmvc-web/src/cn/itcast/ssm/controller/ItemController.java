package cn.itcast.ssm.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.annotations.ResultMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cn.itcast.ssm.pojo.Items;
import cn.itcast.ssm.pojo.QueryVo;
import cn.itcast.ssm.service.ItemService;

@Controller
public class ItemController {

	@Autowired
	private ItemService itemService;
	
	//绑定包数据类型
	@RequestMapping("/queryitem")
	public String quertItem(QueryVo queryVo){
		System.err.println(queryVo.getItems().getId());
		System.err.println(queryVo.getItems().getName());
		return "success";
	}
	
	/**
	 * 通过id更新items表
	 * @param items
	 * @return
	 */
	@RequestMapping("/updateitem")
	public String updateItem(Items items){
		//调用服务更新商品
		itemService.updateItemById(items);
		
		//返回逻辑视图
		return "success";
	}
	
	/**
	 * 根据id查询商品
	 * @param request
	 * @return
	 */
	@RequestMapping("/itemEdit")
	/*public String queryItemById(HttpServletRequest request,Model model){
		//从request 中获取请求参数
		String strId = request.getParameter("id");
		//根据id查询商品信息
		Items items = itemService.queryItemById(Integer.parseInt(strId));
		
		model.addAttribute("item", items);
		
		return "editItem";
	}*/
	public String queryItemById(int id,ModelMap model){
		//根据id查询商品信息
		Items items = itemService.queryItemById(id);
		//把商品数据放在模型中
		model.addAttribute("item", items);
		
		return "editItem";
	}
/*	public ModelAndView queryItemById(HttpServletRequest request){
		//从request 中获取请求参数
		String strId = request.getParameter("id");
		//根据id查询商品信息
		Items items = itemService.queryItemById(Integer.parseInt(strId));
		//把结果传给页面
		ModelAndView modelAndView = new ModelAndView();
		//把商品数据放在模型中
		modelAndView.addObject("item",items);
		//设置逻辑视图
		modelAndView.setViewName("editItem");
		
		return modelAndView;
	}
*/	
	
	/**
	 * 查询所有
	 * @return
	 */
	@RequestMapping("/itemList")
	public ModelAndView queryItemList(){
		//获取商品数据
		List<Items> list = this.itemService.queryItemList();
		ModelAndView modelAndView = new ModelAndView();
		//把商品数据放到模型中
		modelAndView.addObject("itemList", list);
		//设置逻辑视图
		modelAndView.setViewName("itemList");
		return modelAndView;
	}
}
