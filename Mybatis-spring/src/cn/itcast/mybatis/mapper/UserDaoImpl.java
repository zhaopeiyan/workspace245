package cn.itcast.mybatis.mapper;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;

import cn.itcast.mybatis.pojo.User;

public class UserDaoImpl extends SqlSessionDaoSupport implements UserDao {

	@Override
	public User queryUserById(int id) {
		//获取SQLSession
		SqlSession sqlSession = super.getSqlSession();
		//使用SQLSession执行操作
		User user = sqlSession.selectOne("queryUserById",id);
		
		//不需要关闭sqlSession
		return user;
		
	}

	@Override
	public List<User> queryUserByName(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addUser(User user) {
		// TODO Auto-generated method stub

	}

}
