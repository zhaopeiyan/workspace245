package cn.itcast.mybatis.mapper;

import java.util.List;

import cn.itcast.mybatis.pojo.User;

public interface UserMapper {
	
	/**
	 * 根据id查询用户信息
	 * @param id
	 * @return
	 */
	User queryUserById(int id);
	
	/**
	 * 根据用户名模糊查询用户
	 * @param username
	 * @return
	 */
	List<User> queryUserByName(String username);
	
	/**
	 * 保存用户
	 * @param user
	 */
	void addUser(User user);


}
