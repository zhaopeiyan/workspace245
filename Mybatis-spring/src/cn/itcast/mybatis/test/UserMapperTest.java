package cn.itcast.mybatis.test;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.itcast.mybatis.mapper.UserMapper;
import cn.itcast.mybatis.pojo.User;

public class UserMapperTest {
	
	private ApplicationContext context;
	
	@Before
	public void setUp(){
		this.context = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
	}

	@Test
	public void testQueryUserById() {
		//获取Mapper
		UserMapper userMapper = this.context.getBean(UserMapper.class);
		User user = userMapper.queryUserById(1);
		System.err.println(user.toString());
	}

	@Test
	public void testQueryUserByName() {
		UserMapper userMapper = this.context.getBean(UserMapper.class);
		List<User> list = userMapper.queryUserByName("张");
		for(User user : list){
			System.err.println(user.toString());
		}
	}

	@Test
	public void testAddUser() {
		UserMapper userMapper = this.context.getBean(UserMapper.class);
		User user = new User();
		user.setUsername("扬继盛");
		user.setSex("1");
		user.setBirthday(new Date());
		user.setAddress("知其不可而为之");
		
		userMapper.addUser(user);
		System.err.println(user.toString());
	}

}
