package cn.itcast.mybatis.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.itcast.mybatis.mapper.UserDao;
import cn.itcast.mybatis.pojo.User;

public class UserDaoImplTest {
	
	private ApplicationContext context;
	
	@Before
	public void setUp(){
		this.context = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
	}
	
	/*private UserDao userDao;
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}*/

	@Test
	public void testQueryUserById() {
		//获取userDao
		UserDao userDao = (UserDao) this.context.getBean("userDao");
		User user = userDao.queryUserById(1);
		System.err.println(user.toString());
	}

	@Test
	public void testQueryUserByName() {
		fail("Not yet implemented");
	}

	@Test
	public void testAddUser() {
		fail("Not yet implemented");
	}

}
