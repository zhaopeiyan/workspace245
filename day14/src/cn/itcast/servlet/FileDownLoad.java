package cn.itcast.servlet;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;

import sun.misc.BASE64Encoder;

public class FileDownLoad extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		request.setCharacterEncoding("utf-8");

		// 1、接收参数
		String filename = request.getParameter("filename");
		// 解决get请求乱码问题
		filename = new String(filename.getBytes("iso8859-1"), "utf-8");
		String path = this.getServletContext().getRealPath("/download/" + filename);
		// 2、设置两个头，动态获得文件的MIME类型
		String fileType = this.getServletContext().getMimeType(filename);
		response.setHeader("Content-Type", fileType);

		// 解决下载页面文件的中文问题，获取请求头User-Agent String header =
		String header = request.getHeader("User-Agent");
		if (header.contains("Firefox")) {
			// 火狐浏览器 
			filename = base64EncodeFileName(filename);
		} else { // 其它浏览器 filename =
			filename = URLEncoder.encode(filename, "utf-8");
		}

		// 告诉浏览器，访问的文件请使用下载的方式打开
		response.addHeader("Content-Disposition", "attachment;filename=" + filename);
		// 3、设置输入流
		InputStream is = new FileInputStream(path);
		OutputStream os = response.getOutputStream();
		// 输入输出流对接
		/*
		 * int len = 0; byte[] b = new byte[1024]; while((len = is.read(b)) !=
		 * -1){ os.write(b, 0, len); } is.close();
		 */
		IOUtils.copy(is, os);
	}

	public static String base64EncodeFileName(String fileName) {
		BASE64Encoder base64Encoder = new BASE64Encoder();
		try {
			return "=?UTF-8?B?" + new String(base64Encoder.encode(fileName.getBytes("UTF-8"))) + "?=";
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}