package cn.itcast.servlet;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;

public class FileDownLoad2 extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		request.setCharacterEncoding("utf-8");
		/**
		 * 两个头一个流
		 * setHeader("Content-Type",mime-type)
		 * addHeader("Content-Disposition", "attachment;filename="+filename)
		 */
		//第一步   后去文件名
		String filename = request.getParameter("filename");
		
		//解决中文名乱码，先将取到的filename转码成字节码对象,再将字节码文件对象转码成iso8859-1,再用String的有参构造将其转码成utf-8
		filename = new String(filename.getBytes("iso8859-1"),"utf-8");
		System.out.println(filename);
//		response.getWriter().println(filename);
		//获取文件类型
		String mimeType = request.getServletContext().getMimeType(filename);
		//两个头
		response.setHeader("Content-Type", mimeType);
		//告诉浏览器用下载的方式打开文件
		response.addHeader("Content-Disposition", "attachment;filename="+filename);
		
		String path = this.getServletContext().getRealPath("/download/" + filename);
		System.out.println(path);
		InputStream is = new FileInputStream(path);
		OutputStream os = response.getOutputStream();
		IOUtils.copy(is, os);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}