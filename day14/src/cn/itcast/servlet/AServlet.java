package cn.itcast.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Response;

public class AServlet extends HttpServlet {

	private ServletContext servletContext;

	@Override
	public void init(ServletConfig config) throws ServletException {
		// 获取servletContext域对象
		servletContext = config.getServletContext();
		servletContext.setAttribute("count", 1);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		request.setCharacterEncoding("utf-8");
		// 因为doGet是继承自GenericServlet类
		// 访问一次就count+1
		int count = (int) servletContext.getAttribute("count");
		// System.out.println("第"+count+"次访问此网站！");
		response.getWriter().write("第" + count + "次访问此网站！");
		count++;
		servletContext.setAttribute("count", count);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}