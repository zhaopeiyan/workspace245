package cn.itcast.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		
		//重定向代码
		//1、设置响应码为302，表示重定向
		response.setStatus(302);
		//设置新的请求的URL
		//response.setHeader("Location", "http://nsdual.boxuegu.com/view/home.html");
		
		response.setHeader("Refresh","0, url=AServlet");
		//便捷写法
//		response.sendRedirect("AServlet");
		//sendRedirect优先级比setHeader高
		System.out.println("==========");
//		response.setIntHeader("http://nsdual.boxuegu.com/view/home.html", 132);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}