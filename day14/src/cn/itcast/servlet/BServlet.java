package cn.itcast.servlet;

import java.io.IOException;
import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class BServlet extends GenericServlet {

	private ServletContext servletContext;

	@Override
	public void init() throws ServletException {
		servletContext = this.getServletContext();
		servletContext.setAttribute("count", 1);
		System.out.println(servletContext.getAttribute("count"));
	}

	@Override
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		request.setCharacterEncoding("utf-8");
		// 因为doGet是继承自GenericServlet类
		int count = (int) servletContext.getAttribute("count");
		response.getWriter().write("访问量：" + count);
		count++;
		servletContext.setAttribute("count", count);
	}
}