package cn.itcast.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ReadFileServlet2 extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		request.setCharacterEncoding("utf-8");
		
		ServletContext context = this.getServletContext();
		PrintWriter out = response.getWriter();
		//获取相对路径中的输入流对象
		InputStream in = context.getResourceAsStream("/WEB-INF/classes/1.properties");
		System.out.println(in);
		Properties prop = new Properties();
		prop.load(in);
		
		System.out.println(prop.getProperty("company"));
		out.println("Company="+prop.getProperty("company"+"<br>"));
		out.println("Address="+prop.getProperty("address"+"<br>"));
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}