package cn.itcast.servlet;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ReadFileServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		request.setCharacterEncoding("utf-8");
		
		ServletContext context = this.getServletContext();
		PrintWriter out = response.getWriter();
		//获取相对路径中的输入流对象
		InputStream in = context.getResourceAsStream("/WEB-INF/classes/1.properties");
//		System.out.println(in);
		
		//获取项目发布的路径
		String realPath = context.getRealPath("/WEB-INF/classes/1.properties");
		FileInputStream fis = new FileInputStream(realPath);
		System.out.println(realPath);
		Properties prop = new Properties();
		prop.load(fis);
		
		System.out.println(prop.getProperty("company"));
		System.out.println(prop.getProperty("address"));
		out.println("cCompany="+prop.getProperty("company"));
		out.println("aAddress="+prop.getProperty("address"));
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
