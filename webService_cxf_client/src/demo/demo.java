package demo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.itcast.ws.cxf.service.WeatherInterface;

public class demo {
	
	public static void main(String[] args) {
		//1.通过spring工厂加载spring容器
		ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		//2.通过spring工厂获取接口实例
		WeatherInterface weatherInterface = (WeatherInterface) classPathXmlApplicationContext.getBean("weatherInterface");
		//3.调用查询方法
		String weather = weatherInterface.queryWeather("Miami");
		System.out.println(weather);
				
	}

}
