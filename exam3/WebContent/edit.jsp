<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags"  prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <link href="${pageContext.request.contextPath}/bootstrap2.3.2/css/bootstrap.min.css" rel="stylesheet"/>
<title>修改页面</title>
<style type="text/css">
.td{border:solid #add9c0; border-width:0px 1px 1px 0px; padding:10px 0px;}
.table{border:solid #add9c0; border-width:1px 0px 0px 1px;}
</style>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript">

	$(function (){
		alert("${editCustomer.user.uid}");
		findAll($("#userInfo"),'${editCustomer.user.uid}');
	})

	function findAll(element,uuid){
		$.post("${pageContext.request.contextPath }/user_findAll.action",{},
				function (data){
					var option = "<option value=''>--请选择--</option>";
					alert("123");
					$(data).each(function(i,n){
						/* alert(n.dict_item_name); */
						option += "<option value='"+n.uid+"'>"+n.username+"</option>";
					})
					element.html(option);
					
					element.children("option[value='"+uuid+"']").prop("selected",true);
				},"json") 
	}

	function submitForm(){
		document.getElementById("form").submit();
	}
</script>
</head>
<body>
<br/><br/>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">修改客户</h3>
            </div>
            <br/><br/><br/>
            <form id="form" name="customerForm" action="${pageContext.request.contextPath}/customer_update.action" class="form-horizontal" method="post">
           	<input type="hidden" name="cid" value="${editCustomer.cid}">
            <table border="1" class="table">
            	<tr>
            		
            		<td class="td" >
            			 <label  for="cust_name">客户名称</label>
            		</td>
            		<td class="td">
            			 <input type="text" id="cust_name" name="cust_name" placeholder="客户名称" value="${editCustomer.cust_name}">
            		</td>
            	</tr>
            	<tr>
            		
            		<td class="td">
            			<label  for="cust_type">客户类型</label>
            		</td >
            		<td class="td">
            			<input type="text" id="cust_type" name="cust_type" placeholder="客户类型" value="${editCustomer.cust_type}">
            		</td>
            	</tr>
            	<tr>
            		
            		<td class="td">
            			 <label for="cust_phone">联系电话</label>
            		</td>
            		<td class="td">
            			 <input type="text" id="cust_phone" name="cust_phone" placeholder="联系电话" value = "${editCustomer.cust_phone}">
            		</td>
            	</tr>
            	<tr>
            		
            		<td class="td">
            			  <label  for="cust_address">联系地址</label>
            		</td>
            		<td class="td">
            			 <input type="text" id="cust_address" name="cust_address" placeholder="联系地址" value = "${editCustomer.cust_address}">
            		</td>
            	</tr>
            	<tr>
            		
            		<td class="td" >
            			 <label for="uList">所属员工</label>
            		</td>
            		<td class="td">
            		<%-- <input name="user.uid" value="${editCustomer.user.username }"> --%>
            		
            		<select id="userInfo" name="user.uid">
            			
            		</select>
            		<%-- <s:select name="user.uid" list="#list" headerKey="" headerValue="--请选择--" listKey="uid" listValue="username"></s:select> --%>
            		</td>
            	</tr>
            	<tr>
            		<td class="td" colspan="2" align="center">
            			<button class="btn btn-primary" onclick="submitForm();">修改</button>
            			<button class="btn" onclick="javascript:history.back();return false;">返回</button>
            		</td>
            	</tr>
            </table>
            </form>
            
</body>
</html>