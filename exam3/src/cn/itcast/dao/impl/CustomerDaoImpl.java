package cn.itcast.dao.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;

import cn.itcast.dao.CustomerDao;
import cn.itcast.domain.Customer;

@SuppressWarnings("unchecked")
public class CustomerDaoImpl extends HibernateDaoSupport implements CustomerDao {

	@Override
	public List<Customer> list(DetachedCriteria criteria) {
		// TODO Auto-generated method stub
		List<Customer> list = (List<Customer>) this.getHibernateTemplate().findByCriteria(criteria);
		return list;
	}

	@Override
	public Customer findById(DetachedCriteria criteria) {
		// TODO Auto-generated method stub
		List<Customer> list = (List<Customer>) this.getHibernateTemplate().findByCriteria(criteria);
		if(list.size()>0){
			return list.get(0);
		}
		return null;
	}

	@Override
	public void update(Customer customer) {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().update(customer);
	}

	@Override
	public void delete(Customer customer) {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().delete(customer);
	}

}
