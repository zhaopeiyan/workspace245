package cn.itcast.dao.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;

import cn.itcast.dao.UserDao;
import cn.itcast.domain.User;

@SuppressWarnings("unchecked")
public class UserDaoImpl extends HibernateDaoSupport implements UserDao {

	@Override
	public User login(DetachedCriteria criteria) {
		// TODO Auto-generated method stub
		List<User> list = null;
		list = (List<User>) this.getHibernateTemplate().findByCriteria(criteria);
		if(list.size()>0&&list!=null){
			return list.get(0);
		}
		return null;
	}

	@Override
	public List<User> findAll(DetachedCriteria criteria) {
		// TODO Auto-generated method stub
		List<User> list = (List<User>) this.getHibernateTemplate().findByCriteria(criteria);
		if(list.size()>0&&list!=null){
			return list;
		}
		return null;
	}
}
