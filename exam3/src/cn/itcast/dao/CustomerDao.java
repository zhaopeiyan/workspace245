package cn.itcast.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import cn.itcast.domain.Customer;

public interface CustomerDao {

	List<Customer> list(DetachedCriteria criteria);

	Customer findById(DetachedCriteria criteria);

	void update(Customer customer);

	void delete(Customer customer);

}
