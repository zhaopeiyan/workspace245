package cn.itcast.web;

import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.http.converter.json.GsonBuilderUtils;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.domain.User;
import cn.itcast.service.UserService;
import net.sf.json.JSONArray;

public class UserAction extends ActionSupport implements ModelDriven<User>{

	private UserService userService;
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	
	
	public String findAll() throws Exception{
		DetachedCriteria criteria = DetachedCriteria.forClass(user.getClass());
		//criteria.add(Restrictions.eq("dict_type_code", user.getDict_type_code()));
		
		List<User> list = userService.findAll(criteria);
		
		JSONArray json = JSONArray.fromObject(list);
		ServletActionContext.getResponse().setContentType("text/html;charset=utf-8");
		ServletActionContext.getResponse().getWriter().write(json.toString());
		
		
		return NONE;
	}
	
	private User existUser ;
	public User getExistUser() {
		return existUser;
	}



	public String login(){
		DetachedCriteria criteria = DetachedCriteria.forClass(user.getClass());
		criteria.add(Restrictions.eq("username", user.getUsername()));
		criteria.add(Restrictions.eq("password", user.getPassword()));
		existUser = userService.login(criteria);
		if(existUser==null){
			this.addFieldError("loginWrong","用户名或密码错误!");
			return "login";
		}else{
//			ActionContext.getContext().getSession().put("existUser", existUser);
			ServletActionContext.getRequest().getSession().setAttribute("existUser", existUser);
		}
		return "left";
	}
	
	
	private User user = new User();
	@Override
	public User getModel() {
		// TODO Auto-generated method stub
		return user;
	}

}
