package cn.itcast.web;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.domain.Customer;
import cn.itcast.service.CustomerService;

public class CustomerAction extends ActionSupport implements ModelDriven<Customer> {

	private CustomerService customerService;
	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}
	
	
	private List<Customer> customerList;
	public List<Customer> getCustomerList() {
		return customerList;
	}
	
	private Customer editCustomer;
	public Customer getEditCustomer() {
		return editCustomer;
	}

	public String update(){
		
		customerService.update(customer);
		return "update";
	}
	
	
	public String delete(){
		
		customerService.delete(customer);
		return "delete";
	}
	
	
	public String edit(){
		
		DetachedCriteria criteria = DetachedCriteria.forClass(customer.getClass());
		criteria.add(Restrictions.eq("cid", customer.getCid()));
		editCustomer = customerService.findById(criteria);
		
		return "edit";
	}
	
	
	public String findByCondation(){
		
		DetachedCriteria criteria = DetachedCriteria.forClass(customer.getClass());
		if(customer.getCust_name()!=null&&!(customer.getCust_name().trim().equals(""))){
			criteria.add(Restrictions.like("cust_name", "%"+customer.getCust_name()+"%"));
		}
		if(customer.getCust_type()!=null&&!(customer.getCust_type().trim().equals(""))){
			criteria.add(Restrictions.like("cust_type", "%"+customer.getCust_type()+"%"));
		}
		
		customerList = customerService.list(criteria);
		
		return "list";
	}
	
	
	public String list(){
		
		DetachedCriteria criteria = DetachedCriteria.forClass(customer.getClass());
		/*if(customer.getCust_name()!=null&&!(customer.getCust_name().trim().equals(""))){
			criteria.add(Restrictions.like("cust_name", customer.getCust_name()));
		}
		if(customer.getCust_type()!=null&&!(customer.getCust_type().trim().equals(""))){
			criteria.add(Restrictions.like("cust_type", customer.getCust_type()));
		}*/
		
		customerList = customerService.list(criteria);
		
		return "list";
	}
	
	
	private Customer customer = new Customer();
	@Override
	public Customer getModel() {
		// TODO Auto-generated method stub
		return customer;
	}

}
