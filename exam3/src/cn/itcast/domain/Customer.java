package cn.itcast.domain;

import java.io.Serializable;

public class Customer implements Serializable{

	/*`cid` int(11) NOT NULL AUTO_INCREMENT,
	  `cust_name` varchar(255) DEFAULT NULL,
	  `cust_type` varchar(255) DEFAULT NULL,
	  `cust_phone` varchar(255) DEFAULT NULL,
	  `cust_address` varchar(255) DEFAULT NULL,
	  `cust_link_user` int(11) DEFAULT NULL,*/
	
	private Integer cid;
	private String cust_name;
	private String cust_type;
	private String cust_phone;
	private String cust_address;
	/*private Integer cust_link_user;
	public Integer getCust_link_user() {
		return cust_link_user;
	}
	public void setCust_link_user(Integer cust_link_user) {
		this.cust_link_user = cust_link_user;
	}*/

	public User user;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	
	
	public Integer getCid() {
		return cid;
	}
	public void setCid(Integer cid) {
		this.cid = cid;
	}
	public String getCust_name() {
		return cust_name;
	}
	public void setCust_name(String cust_name) {
		this.cust_name = cust_name;
	}
	public String getCust_type() {
		return cust_type;
	}
	public void setCust_type(String cust_type) {
		this.cust_type = cust_type;
	}
	public String getCust_phone() {
		return cust_phone;
	}
	public void setCust_phone(String cust_phone) {
		this.cust_phone = cust_phone;
	}
	public String getCust_address() {
		return cust_address;
	}
	public void setCust_address(String cust_address) {
		this.cust_address = cust_address;
	}
	
	public Customer() {
		// TODO Auto-generated constructor stub
	}
	
}
