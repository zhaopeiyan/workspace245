package cn.itcast.service.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import cn.itcast.dao.UserDao;
import cn.itcast.domain.User;
import cn.itcast.service.UserService;

public class UserServiceImpl implements UserService {

	private UserDao userDao;
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}


	@Override
	public User login(DetachedCriteria criteria) {
		// TODO Auto-generated method stub
		return userDao.login(criteria);
	}


	@Override
	public List<User> findAll(DetachedCriteria criteria) {
		// TODO Auto-generated method stub
		return userDao.findAll(criteria);
	}

}
