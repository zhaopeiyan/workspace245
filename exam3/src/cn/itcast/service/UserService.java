package cn.itcast.service;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import cn.itcast.domain.User;

public interface UserService {

	User login(DetachedCriteria criteria);

	List<User> findAll(DetachedCriteria criteria);

}
