<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsontest</title>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript">
	function requestJson() {
		$.ajax({
			type : 'post',
			url : '${pageContext.request.contextPath}/requestJson.action',
			contentType : 'application/json;charset=utf-8',
			//数据格式是json串,商品信息
			data:'{"name":"手机","price":"9999"}',
			success:function(data){//返回json结果
				alert(data);
			}
		})
	}
	function requestKeyValue(){
		$.ajax({
			type : 'post',
			url : '${pageContext.request.contextPath}/requestKeyValue.action',
			//默认请求就是key/value形式,所以不用设置contentType
			//contentType : 'application/json;charset=utf-8',
			//数据格式是json串,商品信息
			data:'{"name":"手机","price":"9999"}',
			success:function(data){//返回json结果
				alert(data);
			}
		})
	}
</script>
</head>
<body>
<input type="button" onclick="requestJson()" value="发送json请求"/>
<input type="button" onclick="requestKeyValue()" value="发送key/value请求"/>
</body>
</html>