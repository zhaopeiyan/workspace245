package cn.itcast.ssm.pojo;

import java.util.List;

public class QueryVo {

	private Items items;
	
	//绑定数组类型的数据
	private Integer[] ids;
	
	
	//绑定List类型数据
	private List<Items> itemList;
	
	
	public List<Items> getItemList() {
		return itemList;
	}

	public void setItemList(List<Items> itemList) {
		this.itemList = itemList;
	}

	public Integer[] getIds() {
		return ids;
	}

	public void setIds(Integer[] ids) {
		this.ids = ids;
	}

	public Items getItems() {
		return items;
	}

	public void setItems(Items items) {
		this.items = items;
	}
	
	
}
