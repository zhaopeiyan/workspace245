package cn.itcast.ssm.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class HandlerInterceptor1 implements HandlerInterceptor {

	/**
	 * controller执行后且视图返回后调用此方法
	 * 可以得到执行controller时的异常信息
	 * 可以记录操作日志
	 */
	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		System.err.println("Handler Interceptor1...afterCompletion");

	}

	/**
	 * controller执行后但未返回视图前调用此方法
	 * 可以在返回用户前对模型数据加工处理,比如加入公共信息(天气,日历等信息)
	 */
	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO Auto-generated method stub
		System.err.println("Handler Interceptor1...postHandle");

	}

	/**
	 * controller执行前调用此方法
	 * 返回true表示继续执行,返回false中止执行
	 * 可以加入登录校验\权限拦截等
	 */
	@Override
	public boolean preHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2) throws Exception {
		System.err.println("Handler Interceptor1...perHandle");
		// 设置为true,测试使用
		return true;
	}

}
