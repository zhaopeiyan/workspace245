package cn.itcast.ssm.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * <p>Title:LoginInterceptor</p>
 * <p>Description:登录认证拦截器</p>
 * <p>Company:</p>
 * @author PeiyanZhao
 * @date2017年5月14日 上午1:35:45
 */
public class LoginInterceptor implements HandlerInterceptor {

	

	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object arg2, Exception exception)
			throws Exception {
		// TODO Auto-generated method stub
	
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object arg2) throws Exception {
		// TODO Auto-generated method stub
		//获取请求URL
		String uri = request.getRequestURI();
		//判断URL是否是公开地址(实际使用时将公开地址配置在配置文件中)
		//此处公开地址是登录提交地址
		if(uri.indexOf("login.action") >= 0){
			//进行登录提交
			return true;
		}
		
		//得到session
		HttpSession session = request.getSession();
		String username = (String) session.getAttribute("username");
		if(username != null){
			return true;
		}
		
		//既不是访问的公共页面,也没有通过登录验证,跳到login.jsp页面
		request.getRequestDispatcher("/login.jsp").forward(request, response);
		
		return false;
	}

}
