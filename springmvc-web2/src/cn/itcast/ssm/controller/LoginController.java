package cn.itcast.ssm.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {

	
	//login
	@RequestMapping("/login")
	public String login(HttpSession session,String username,String password){
		
		//调用service进行用户省份认证
		
		//在session中保存用户省份信息
		session.setAttribute("username", username);
		
		//重定向到商品列表页面
		return "redirect:itemList.action";
	}
	
	//logout
	@RequestMapping("/logout")
	public String logout(HttpSession session){
		
		//调用service进行用户省份认证
		
		//在session中保存用户省份信息
		session.invalidate();
		
		//重定向到商品列表页面
		return "redirect:/itemList.action";
	}
}
