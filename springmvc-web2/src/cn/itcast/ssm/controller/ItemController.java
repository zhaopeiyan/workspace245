package cn.itcast.ssm.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import cn.itcast.ssm.exception.MyException;
import cn.itcast.ssm.pojo.Items;
import cn.itcast.ssm.pojo.QueryVo;
import cn.itcast.ssm.service.ItemService;

@Controller
//@RequestMapping("item")//指定通用前缀
public class ItemController {

	@Autowired
	private ItemService itemService;
	
	
	@RequestMapping("testJson")
	public @ResponseBody Items testJson(@RequestBody Items items){
		return items;
	}
	
	
	//绑定包数据类型
	@RequestMapping("/queryitem")
	public String quertItem(QueryVo queryVo,Integer[] ids){
		System.err.println(queryVo.getItems().getId());
		System.err.println(queryVo.getItems().getName());
		System.err.println(queryVo.getIds().length);
		System.err.println(ids.length);
		return "success";
	}
	
	/**
	 * 通过id更新items表
	 * @param items
	 * @return
	 */
	@RequestMapping("/updateitem")
	public String updateItem(Items items,MultipartFile pictureFile){
		
		//图片上传
		//设置图片名称,不能重复,可以使用uuid
		String picName = UUID.randomUUID().toString();
		
		//获取文件名
		String oriName = pictureFile.getOriginalFilename();
		//获取图片后缀
		String extName = oriName.substring(oriName.lastIndexOf("."));
		
		//开始上传
		try {
			pictureFile.transferTo(new File("C:\\Users\\PeiyanZhao\\Desktop\\upload\\image\\"+picName+extName));
		} catch (IllegalStateException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//设置图片名到商品中
		items.setPic(picName+extName);
		
		
		
		//调用服务更新商品
		itemService.updateItemById(items);
		
		//返回逻辑视图
		//return "success";
		
		//重定向到指定路径
		//return "redirect:/itemEdit.action?id="+items.getId();
		
		//请求转发
		return "forward:/itemEdit.action";
	}
	
	/**
	 * 根据id查询商品
	 * @param request
	 * @return
	 */
	@RequestMapping("/itemEdit")
	/*public String queryItemById(HttpServletRequest request,Model model){
		//从request 中获取请求参数
		String strId = request.getParameter("id");
		//根据id查询商品信息
		Items items = itemService.queryItemById(Integer.parseInt(strId));
		
		model.addAttribute("item", items);
		
		return "editItem";
	}*/
	public String queryItemById(int id,ModelMap model){
		//根据id查询商品信息
		Items items = itemService.queryItemById(id);
		//把商品数据放在模型中
		model.addAttribute("item", items);
		//URL路径映射
		return "editItem";
	}
/*	public ModelAndView queryItemById(HttpServletRequest request){
		//从request 中获取请求参数
		String strId = request.getParameter("id");
		//根据id查询商品信息
		Items items = itemService.queryItemById(Integer.parseInt(strId));
		//把结果传给页面
		ModelAndView modelAndView = new ModelAndView();
		//把商品数据放在模型中
		modelAndView.addObject("item",items);
		//设置逻辑视图
		modelAndView.setViewName("editItem");
		
		return modelAndView;
	}
*/	
	
	/**
	 * 查询所有商品列表
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = {"itemList","itemListAll"},method = {RequestMethod.POST,RequestMethod.GET})
	public ModelAndView queryItemList() throws Exception{
		
		//自定义异常
		if(false){
			throw new MyException("自定义异常~~~~~~~");
		}
		
		//运行时异常
//		int i = 1/0;
		
		//获取商品数据
		List<Items> list = this.itemService.queryItemList();
		ModelAndView modelAndView = new ModelAndView();
		//把商品数据放到模型中
		modelAndView.addObject("itemList", list);
		//设置逻辑视图
		modelAndView.setViewName("itemList");
		return modelAndView;
	}

	/**
	 * restful风格路径实现,返回json串
	 * @param id
	 * @return
	 */
	@RequestMapping("item/{id}")
	public @ResponseBody Items queryItemById(@PathVariable("id") Integer id){
		Items items = itemService.queryItemById(id);
		return items;
	}
	
	
	//json交互测试
	/**
	 * 请求json,输出json
	 * @param items
	 * @return
	 */
	@RequestMapping("/requestJson")
	public @ResponseBody Items requestJson(@RequestBody Items items){
		return items;
	}
	/**
	 * 请求key/value,输出json
	 * @param items
	 * @return
	 */
	@RequestMapping("/requestKeyValue")
	public @ResponseBody Items requestKeyValue(@RequestBody Items items){
		return items;
	}

}
