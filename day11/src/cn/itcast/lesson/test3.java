package cn.itcast.lesson;

import java.io.File;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class test3 {

	public static void main(String[] args) throws Exception {
		SAXReader reader = new SAXReader();
		Document document = reader.read(new File("03.schame/web2.xml"));
		Element rootElement = document.getRootElement();
		List<Element> list = rootElement.elements();
		System.out.println(list.size());
		Map<String ,String> map = new HashMap<>();
		for (Element e : list) {
			String name = e.getName();
			if("servlet".equals(name)){
				System.out.println("++++++++++");
				String servletName = e.elementText("servlet-name");
				String servletClass = e.elementText("servlet-class");
				System.out.println(servletClass);
				map.put(servletName, servletClass);
			}
			
			if("servlet-mapping".equals(name)){
				String servletName = e.elementText("servlet-name");
				String url = e.elementText("url-pattern");
				System.out.println(url);
				String servletClass = map.get(servletName);
				map.put(url, servletClass);
				map.remove(servletName);
			}
		}
		System.out.println(map);
		demo(map);
	}
	
	public static void demo(Map<String, String> map) throws Exception{
		String url = "/hello";
		String servletClass = map.get(url);
		Class clazz = Class.forName(servletClass);
		
		/*
		 * 用getMethod获取单个方法
		 */
		/*Method method = clazz.getMethod("init", null);
		Object object = clazz.newInstance();
		method.invoke(object, null);*/
		
		/*
		 * 用getMethods获取所有方法
		 */
		/*Method[] methods = clazz.getMethods();
		Object object = clazz.newInstance();
		for (Method m : methods) {
			m.invoke(object, null);
		}
		return;*/
		
		/*
		 * 用clazz实例化字节码文件对象之后强制转换成MyServlet接口对象
		 */
		MyServlet myServlet = (MyServlet) clazz.newInstance();
		myServlet.init();
		myServlet.service();
		myServlet.destory();
	}
	
}
