package cn.itcast.lesson;

import java.io.File;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.junit.Test;

public class test {
	
	@Test
	public void demo() throws Exception{
		//1、获得document
		SAXReader reader = new SAXReader();
		Document document = reader.read(new File("03.schame\\web2.xml"));
		//2、获得根元素
		Element rootElement = document.getRootElement();
		//打印version属性值
		String version = rootElement.attributeValue("version");
		System.out.println(version);
		//获得所有子元素
		List<Element> allChildElement = rootElement.elements();
		//遍历所有
		for (Element e : allChildElement) {
			//打印元素名
			String name = e.getName();
			System.out.println(name);
			//处理servlet，并获得子标签的内容
			if("servlet".equals(e)){
				Element servletNameElement = e.element("servlet-name");
				String servletName = servletNameElement.getText();
				System.out.println("\t"+servletName);
				
				//获得元素文本值
				String servletClass = e.elementText("servlet-class");
				System.out.println("\t"+servletClass);
			}
		}
	}

}
