package cn.itcast.lesson;

import java.io.File;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.junit.Before;
import org.junit.Test;

public class test2 {
	
private Map<String,String> data = new HashMap<String,String>();
	
	@Before
	public void demo4Before() throws Exception {
		//在执行前执行，解析xml，并将结果存放到Map<路径，实现类>中
		//1.获得document
		SAXReader saxReader = new SAXReader();
		Document document = saxReader.read(new File("03.schame/web2.xml"));///day11/03.schame/web2.xml
		//2.获得跟元素
		Element rootElement = document.getRootElement();
		//3.获得所有子元素
		List<Element> allChildElement = rootElement.elements();
		/*
		 * 4.遍历所有
		 * 1)解析到<servlet>,将其子标签<servlet-name>与<servlet-class>存放到Map中
		 * 2)解析到<servlet-mapping>,获得子标签<servlet-name>和<url-pattern>,
		 * 从map中获得内容，组合成url = class的键值树
		 */
		for (Element e : allChildElement) {
			//4.1获得元素名
			String eleName = e.getName();
			//4.2如果是servlet，将解析内容存放到map中
			if("servlet".equals(eleName)){
				String servletName = e.elementText("servlet-name");
				String servletClass = e.elementText("servlet-class");
				data.put(servletName, servletClass);
			}
			System.out.println(data);
			System.out.println("++++++++++++++++++++++++++++++++++++++++++++");
			//如果是servlet-mapping,获得之前内容，组成key=url，value=class并添加到map中
			if("servlet-mapping".equals(eleName)){
				String servletName = e.elementText("servlet-name");
				String urlPattern = e.elementText("url-pattern");
				//获得<servlet-name>之前存放在Map中<servlet-class>值
				String servletClass = data.get(servletName);
				//存放新的内容 url = class
				data.put(urlPattern, servletClass);
				//将之前存放的数据删除
				data.remove(servletName);
			}
			
			//打印信息
			System.out.println(data);
		}
	}
	
	@Test
	public void demo3() throws Exception{
		//1.模拟路径
		String url = "/hello";
		
		//2.通过路径获得对应的实现类
		String servletClass = data.get(url);
		
		//3.获得字符串实现类实例
		Class clazz = Class.forName(servletClass);
		Method[] methods = clazz.getMethods();
		Method method = clazz.getMethod("init",null);
		Object object = clazz.newInstance();
		method.invoke(object, null);
		/*	
		for (Method m : methods) {
			m.invoke(object, null);
		}*/
//		MyServlet myServlet = (MyServlet) clazz.newInstance();
		
		//4.执行对象的方法
//		myServlet.init();
//		myServlet.service();
//		myServlet.destory();
	}

}

