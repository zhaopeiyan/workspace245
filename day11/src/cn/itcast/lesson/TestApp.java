package cn.itcast.lesson;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.junit.Before;
import org.junit.Test;

public class TestApp {

	@Test
	public void demo(){
		//手动创建执行
		MyServlet myServlet = new HelloMyServlet();
		myServlet.init();
		myServlet.service();
		myServlet.destory();
	}

	@Test
	public void demo2() throws Exception {
		/*
		 * 反射创建执行
		 * 1)Class.forName 返回指定接口或类的class对象
		 * 2)newInstance()通过Class对象创建类的实例对象，相当于new Xxx();
		 */
		String servletClass = "cn.itcast.lesson.HelloMyServlet";
		
		//3.获得字符串实现类实例
		Class clazz = Class.forName(servletClass);
		MyServlet myServlet = (MyServlet) clazz.newInstance();
		//4.执行对象的方法
		myServlet.init();
		myServlet.service();
		myServlet.destory();
	}
	
	private Map<String,String> data = new HashMap<String,String>();
	
	@Before
	public void demo4Before() throws Exception {
		//在执行前执行，解析xml，并将结果存放到Map<路径，实现类>中
		//1.获得document
		SAXReader saxReader = new SAXReader();
		Document document = saxReader.read(new File("03.schame/web2.xml"));///day11/03.schame/web2.xml
		//2.获得跟元素
		Element rootElement = document.getRootElement();
		//3.获得所有子元素
		List<Element> allChildElement = rootElement.elements();
		/*
		 * 4.遍历所有
		 * 1)解析到<servlet>,将其子标签<servlet-name>与<servlet-class>存放到Map中
		 * 2)解析到<servlet-mapping>,获得子标签<servlet-name>和<url-pattern>,
		 * 从map中获得内容，组合成url = class的键值树
		 */
		for (Element e : allChildElement) {
			//4.1获得元素名
			String eleName = e.getName();
			//4.2如果是servlet，将解析内容存放到map中
			if("servlet".equals(eleName)){
				String servletName = e.elementText("servlet-name");
				String urlPattern = e.elementText("url-pattren");
				//获得<servlet-name>之前存放在Map中<servlet-class>值
				String servletClass = data.get(servletName);
				data.put(urlPattern, servletClass);
				data.remove(servletName);
			}
			
			//打印信息
			System.out.println(data);
		}
	}
	
	@Test
	public void demo3() throws Exception{
		//1.模拟路径
		String url = "/hello";
		
		//2.通过路径获得对应的实现类
		String servletClass = data.get(url);
		
		//3.获得字符串实现类实例
		Class clazz = Class.forName(servletClass);
		MyServlet myServlet = (MyServlet) clazz.newInstance();
		//4.执行对象的方法
		myServlet.init();
		myServlet.service();
		myServlet.destory();
	}

}
