package cn.itcast.strruts2.service;

import java.util.List;

import cn.itcast.domain.Customer;

public interface CustomerService {

	List<Customer> findAll();

}
