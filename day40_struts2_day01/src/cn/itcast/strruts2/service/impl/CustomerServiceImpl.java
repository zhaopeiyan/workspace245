package cn.itcast.strruts2.service.impl;

import java.util.List;

import cn.itcast.domain.Customer;
import cn.itcast.strruts2.dao.CustomerDao;
import cn.itcast.strruts2.dao.impl.CustomerDaoImpl;
import cn.itcast.strruts2.service.CustomerService;

public class CustomerServiceImpl implements CustomerService {

	@Override
	public List<Customer> findAll() {
		// TODO Auto-generated method stub
		CustomerDao cd = new CustomerDaoImpl();
		return cd.findAll();
	}

}
