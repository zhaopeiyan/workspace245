package cn.itcast.strruts2.dao;

import java.util.List;

import cn.itcast.domain.Customer;

public interface CustomerDao {

	List<Customer> findAll();

}
