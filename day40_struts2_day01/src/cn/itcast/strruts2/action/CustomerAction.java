package cn.itcast.strruts2.action;

import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

import cn.itcast.domain.Customer;
import cn.itcast.strruts2.service.CustomerService;
import cn.itcast.strruts2.service.impl.CustomerServiceImpl;

public class CustomerAction extends ActionSupport {
	
	
	public String findAll(){
		CustomerService cs = new CustomerServiceImpl();
		List<Customer> list = cs.findAll();
		//获得request对象,并且保存到request中
		ServletActionContext.getRequest().setAttribute("list",list);
		return "findAll";
	}
	
}
