package cn.itcast.strruts2.service;

import cn.itcast.domain.User;

public interface UserDao {

	User login(String user_code, String user_password);

}
