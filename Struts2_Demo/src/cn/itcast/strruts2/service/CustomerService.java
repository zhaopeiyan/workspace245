package cn.itcast.strruts2.service;

import java.util.List;

import cn.itcast.domain.Customer;

public interface CustomerService {

	List<Customer> findAll();

	void save(Customer customer);

	void deleteCustomer(Long cust_id);

	Customer findCustomerByID(Long cust_id);

	void modifyCustomer(Customer customer);

}
