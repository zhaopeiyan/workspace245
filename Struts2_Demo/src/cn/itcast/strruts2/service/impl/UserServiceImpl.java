package cn.itcast.strruts2.service.impl;

import cn.itcast.domain.User;
import cn.itcast.strruts2.service.UserDao;
import cn.itcast.strruts2.service.UserService;

public class UserServiceImpl implements UserService {

	@Override
	public User login(String user_code, String user_password) {
		UserDao userDao = new UserDaoImpl();
		return userDao.login(user_code,user_password);
	}

}
