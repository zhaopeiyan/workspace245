package cn.itcast.strruts2.service.impl;

import java.util.List;

import cn.itcast.domain.Customer;
import cn.itcast.strruts2.dao.CustomerDao;
import cn.itcast.strruts2.dao.impl.CustomerDaoImpl;
import cn.itcast.strruts2.service.CustomerService;

public class CustomerServiceImpl implements CustomerService {

	@Override
	public List<Customer> findAll() {
		// TODO Auto-generated method stub
		CustomerDao cd = new CustomerDaoImpl();
		
		return cd.findAll();
	}

	@Override
	public void save(Customer customer) {
		// TODO Auto-generated method stub
		CustomerDao cd = new CustomerDaoImpl();
		cd.save(customer);
	}

	@Override
	public void deleteCustomer(Long cust_id) {
		// TODO Auto-generated method stub
		CustomerDao cd = new CustomerDaoImpl();
		cd.deleteCustomer(cust_id);
	}

	@Override
	public Customer findCustomerByID(Long cust_id) {
		// TODO Auto-generated method stub
		CustomerDao cd = new CustomerDaoImpl();
		
		return cd.findCustomerByID(cust_id);
	}

	@Override
	public void modifyCustomer(Customer customer) {
		// TODO Auto-generated method stub
		CustomerDao cd = new CustomerDaoImpl();
		cd.modifyCustomer(customer);
	}

}
