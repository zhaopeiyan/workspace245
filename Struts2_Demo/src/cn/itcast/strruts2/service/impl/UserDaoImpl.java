package cn.itcast.strruts2.service.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import cn.itcast.domain.User;
import cn.itcast.strruts2.service.UserDao;
import cn.itcast.utils.HibernateUtils;

public class UserDaoImpl implements UserDao {

	@Override
	public User login(String user_code, String user_password) {
		// TODO Auto-generated method stub
		
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		Query query = session.createQuery("from User where user_code=? and user_password=?");
		query.setParameter(0, user_code);
		query.setParameter(1, user_password);
		
		User user = (User) query.uniqueResult();
		
		transaction.commit();
		session.close();
		return user;
	}

}
