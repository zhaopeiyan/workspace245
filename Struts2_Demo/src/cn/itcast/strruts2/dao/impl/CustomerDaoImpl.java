package cn.itcast.strruts2.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import cn.itcast.domain.Customer;
import cn.itcast.strruts2.dao.CustomerDao;
import cn.itcast.utils.HibernateUtils;

public class CustomerDaoImpl implements CustomerDao {

	/**
	 * 查询所有customer信息
	 */
	@Override
	public List<Customer> findAll() {
		// TODO Auto-generated method stub
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		List<Customer> list = session.createQuery("from Customer").list();
		
		transaction.commit();
		session.close();
		return list;
	}

	/**
	 * 添加customer信息
	 */
	@Override
	public void save(Customer customer) {
		// TODO Auto-generated method stub
		
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		session.save(customer);
		
		transaction.commit();
		session.close();
	}

	/**
	 * 删除customer信息
	 */
	@Override
	public void deleteCustomer(Long cust_id) {
		// TODO Auto-generated method stub
		Session session = HibernateUtils.openSession();
//		Session session = HibernateUtils.openCurrentSession();
		Transaction transaction = session.beginTransaction();
		
		Customer customer = session.get(Customer.class, cust_id);
		
		session.delete(customer);
		transaction.commit();
	}

	/**
	 * 通过id查询customer字段
	 */
	@Override
	public Customer findCustomerByID(Long cust_id) {
		// TODO Auto-generated method stub
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		Customer customer = session.get(Customer.class, cust_id);
		return customer;
	}

	/**
	 * 修改customer记录
	 */
	@Override
	public void modifyCustomer(Customer customer) {
		// TODO Auto-generated method stub
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		session.update(customer);
		transaction.commit();
	}
}
