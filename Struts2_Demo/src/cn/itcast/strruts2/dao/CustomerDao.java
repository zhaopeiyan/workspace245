package cn.itcast.strruts2.dao;

import java.util.List;

import cn.itcast.domain.Customer;

public interface CustomerDao {

	List<Customer> findAll();

	void save(Customer customer);

	void deleteCustomer(Long cust_id);

	Customer findCustomerByID(Long cust_id);

	void modifyCustomer(Customer customer);

}
