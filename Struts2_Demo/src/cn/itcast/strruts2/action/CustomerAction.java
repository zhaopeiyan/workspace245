package cn.itcast.strruts2.action;

import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.domain.Customer;
import cn.itcast.strruts2.service.CustomerService;
import cn.itcast.strruts2.service.impl.CustomerServiceImpl;

public class CustomerAction extends ActionSupport implements ModelDriven<Customer> {

	/**
	 * 修改客户记录
	 */
	public String modifyCustomer(){
		
		CustomerService cs = new CustomerServiceImpl();
		cs.modifyCustomer(customer);
		
		return "modifySuccess";
	}
	
	/**
	 * 通过id查询客户记录
	 */
	public String findCustomerByID(){
		
		//创建业务逻辑类的对象
		CustomerService cs = new CustomerServiceImpl();
		Customer exitCustomer = cs.findCustomerByID(customer.getCust_id());
		
		ServletActionContext.getRequest().setAttribute("customer", exitCustomer);
		ActionContext.getContext().getValueStack().set("customer", exitCustomer);
		return "findCustomreByID";
	}
	
	/**
	 * 删除客户信息方法
	 */
	public String deleteCustomer(){
		System.out.println(customer.getCust_id());
		//创建业务逻辑类的对象
		CustomerService cs = new CustomerServiceImpl();
		cs.deleteCustomer(customer.getCust_id());
		return "deleteSuccess";
	}
	
	/**
	 * 查询客户列表的方法
	 */
	public String findAll(){
		//创建业务层的类的对象
		CustomerService customerService = new CustomerServiceImpl();
		//调用业务逻辑层的方法查询所有客户
		List<Customer> list = customerService.findAll();
		
		ActionContext.getContext().getValueStack().set("list", list);
		ServletActionContext.getRequest().setAttribute("list", list);
		return "findAll";
	}
	
	/**
	 * 跳转到用户添加页面
	 * 
	 */
	public String saveUI(){
		
		return "saveUI";
	}

	
	//模型驱动使用的对象
	private Customer customer = new Customer();
	@Override
	public Customer getModel() {
		// TODO Auto-generated method stub
		return customer;
	}
	
	/**
	 * 添加客户
	 */
	
	public String save(){
		CustomerService cs = new CustomerServiceImpl();
		cs.save(customer);
		return "saveSuccess";
	}
}
