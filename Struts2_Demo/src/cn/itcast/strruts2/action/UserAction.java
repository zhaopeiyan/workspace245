package cn.itcast.strruts2.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.domain.User;
import cn.itcast.strruts2.service.UserService;
import cn.itcast.strruts2.service.impl.UserServiceImpl;

public class UserAction extends ActionSupport implements ModelDriven<User> {

	//使用模型驱需要使用的对象
	private User user = new User();
	
	@Override
	public User getModel() {
		// TODO Auto-generated method stub
		return user;
	}

	/**
	 * 编写一个执行的方法login
	 */
	public String login(){
		
		UserService userService = new UserServiceImpl();
		User loginUser = userService.login(user.getUser_code(),user.getUser_password());
		
		//判断登录是否成功
		if(loginUser == null){
			//登录失败
			return "login";
		}else{
			//登陆成功,将用户信息存入到session中
			ActionContext.getContext().getSession().put("loginUser", loginUser);
			return "index";
		}
	}
	
	
	@Override
	public String execute() throws Exception {
		// TODO Auto-generated method stub
		return super.execute();
	}
}
