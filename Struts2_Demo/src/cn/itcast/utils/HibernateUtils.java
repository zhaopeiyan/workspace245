package cn.itcast.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtils {
	private static Configuration configuration;
	private static SessionFactory sessionFactory;
	
	static{
		configuration = new Configuration().configure();
		sessionFactory = configuration.buildSessionFactory();
	}
	
	/**
	 * 获得session方法
	 */
	public static Session openSession(){
		return sessionFactory.openSession();
	}
	public static Session openCurrentSession(){
		return sessionFactory.getCurrentSession();
		
	}
	
}
