package cn.itcast.web.interceptor;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

import cn.itcast.domain.User;

public class PrivilegeInterceptor extends MethodFilterInterceptor {

	@Override
	public String doIntercept(ActionInvocation invocation) throws Exception {
		// TODO Auto-generated method stub

		// 判断是否已经登录
		User loginUser = (User) ServletActionContext.getRequest().getSession().getAttribute("loginUser");
		if (loginUser == null) {
			// 没有登录
			return "login";
		} 
		// 已经登录,放行
		return invocation.invoke();

	}
}
