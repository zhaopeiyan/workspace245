package cn.itcast.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.domain.User;
import cn.itcast.service.UserService;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		try {
			// 1、获取到网页输入值
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			// 2、将获取到的值封装到javaBean中
			User user = new User();
			user.setUsername(username);
			user.setPassword(password);
			// 3、调用业务逻辑层处理数据
			UserService userService = new UserService();
			User existUser = userService.login(user);
			// 4、根据处理结果显示信息
			if (existUser == null) {
				// 登录失败
				response.getWriter().write("登录失败！");
				
				System.out.println("登录失败~~");
			} else {
				response.getWriter().write("登陆成功！");;
				System.out.println("登陆成功~~");
			}
		} catch (Exception e) {
			// TODO: handle exception
			throw new RuntimeException("servlet执行异常" + e);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
