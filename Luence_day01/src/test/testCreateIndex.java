package test;

import java.io.File;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.NumericRangeQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.junit.Test;
import org.wltea.analyzer.lucene.IKAnalyzer;

/**
 * <p>Title:testCreateIndex</p>
 * <p>Description:创建索引的过程</p>
 * <p>Company:</p>
 * @author PeiyanZhao
 * @date2017年5月15日 下午1:53:51
 */
public class testCreateIndex {

	/**
	 * 创建索引
	 * @throws Exception
	 */
	@Test
	public void testCreateIndex1() throws Exception{
		//创建document对象
		Document document = new Document();
		//Document document1 = new Document();
		
		//document文档中添加field
		document.add(new StringField("id","1",Store.YES));
		document.add(new TextField("title","Luence入门",Store.YES));
		document.add(new TextField("content","传智播客,Luence经典教程",Store.YES));
		
//		document1.add(new StringField("id","2",Store.YES));
//		document1.add(new TextField("title","Solr入门",Store.YES));
//		document1.add(new TextField("content","solr索引go语言Python",Store.YES));
		

		//创建建立索引的对象
		FSDirectory fsDirectory = FSDirectory.open(new File("C:\\Users\\PeiyanZhao\\Desktop\\index"));
		//Analyzer analyzer = new StandardAnalyzer();
		IKAnalyzer analyzer = new IKAnalyzer();
		IndexWriterConfig indexWriterConfig = new IndexWriterConfig(Version.LATEST,analyzer);
		IndexWriter indexWriter = new IndexWriter(fsDirectory, indexWriterConfig);
		
		//将文档添加到索引库
//		indexWriter.addDocument(document1);
		indexWriter.addDocument(document);
		indexWriter.close();
	}
	
	/**
	 * ik分词器
	 * @throws Exception
	 */
	@Test
	public void testSearcherIndex() throws Exception{
		//将检索内容转成query对象
		String text = "go";
		IKAnalyzer analyzer = new IKAnalyzer();
		
		//指定对哪个字段检索并且指定使用哪个分析器
		QueryParser queryParser = new QueryParser("content",analyzer);
		Query query = queryParser.parse(text);
		
		//创建检索的对象,需要指定从哪里检索
		Directory directory = FSDirectory.open(new File("C:\\Users\\PeiyanZhao\\Desktop\\index"));
		IndexReader indexReader = DirectoryReader.open(directory);
		IndexSearcher indexSearcher = new IndexSearcher(indexReader);
		
		//通过search方法检索
		TopDocs topDocs = indexSearcher.search(query, Integer.MAX_VALUE);
		System.err.println("总条数:"+topDocs.totalHits);
		ScoreDoc[] scoreDocs = topDocs.scoreDocs;
		
		//便利检索结果
		for(ScoreDoc scoreDoc : scoreDocs){
			System.err.println("得分:"+scoreDoc.score);
			Document doc = indexSearcher.doc(scoreDoc.doc);
			System.err.println("id:"+doc.get("id"));
			System.err.println("title:"+doc.get("title"));
			System.err.println("content:"+doc.get("content"));
		}
	}
	
	/**
	 * 指定多个字段检索并分词
	 * @throws Exception
	 */
	@Test
	public void testMultiFieldQuery() throws Exception{
		//将检索内容转成query对象
		String text = "solr基金";
		IKAnalyzer analyzer = new IKAnalyzer();
		
		//指定对哪个字段检索并且指定使用哪个分析器
		MultiFieldQueryParser multiFieldQueryParser = new MultiFieldQueryParser(new String[]{"title","content"}, analyzer);
		Query query = multiFieldQueryParser.parse(text);
		
		//创建检索的对象,需要指定从哪里检索
		Directory directory = FSDirectory.open(new File("C:\\Users\\PeiyanZhao\\Desktop\\index"));
		IndexReader indexReader = DirectoryReader.open(directory);
		IndexSearcher indexSearcher = new IndexSearcher(indexReader);
		
		//通过search方法检索
		TopDocs topDocs = indexSearcher.search(query, Integer.MAX_VALUE);
		System.err.println("总条数:"+topDocs.totalHits);
		ScoreDoc[] scoreDocs = topDocs.scoreDocs;
		
		//便利检索结果
		for(ScoreDoc scoreDoc : scoreDocs){
			System.err.println("得分:"+scoreDoc.score);
			Document doc = indexSearcher.doc(scoreDoc.doc);
			System.err.println("id:"+doc.get("id"));
			System.err.println("title:"+doc.get("title"));
			System.err.println("content:"+doc.get("content"));
		}
		
	}

	/**
	 * WildcardQuery
	 * @throws Exception
	 */
	
	@Test
	public void testTermQuery() throws Exception{

		String text = "p";
//		TermQuery query = new TermQuery(new Term("context",text));
		Term term = new Term("content", text+"*");
		WildcardQuery query = new WildcardQuery(term);
		
		
		//创建检索的对象,需要指定从哪里检索
		Directory directory = FSDirectory.open(new File("C:\\Users\\PeiyanZhao\\Desktop\\index"));
		IndexReader indexReader = DirectoryReader.open(directory);
		IndexSearcher indexSearcher = new IndexSearcher(indexReader);
		
		//通过search方法检索
		TopDocs topDocs = indexSearcher.search(query, Integer.MAX_VALUE);
		System.err.println("总条数:"+topDocs.totalHits);
		ScoreDoc[] scoreDocs = topDocs.scoreDocs;
		
		//便利检索结果
		for(ScoreDoc scoreDoc : scoreDocs){
			System.err.println("得分:"+scoreDoc.score);
			Document doc = indexSearcher.doc(scoreDoc.doc);
			System.err.println("id:"+doc.get("id"));
			System.err.println("title:"+doc.get("title"));
			System.err.println("content:"+doc.get("content"));
		}
		
	}
	
	/**
	 * FuzzyQuery相似度检索
	 * @throws Exception
	 */
	@Test
	public void testFuzzyQuery() throws Exception{
		
		String text = "apacpt";
//		TermQuery query = new TermQuery(new Term("context",text));
		Term term = new Term("content", text);
		FuzzyQuery query = new FuzzyQuery(term);
		
		
		//创建检索的对象,需要指定从哪里检索
		Directory directory = FSDirectory.open(new File("C:\\Users\\PeiyanZhao\\Desktop\\index"));
		IndexReader indexReader = DirectoryReader.open(directory);
		IndexSearcher indexSearcher = new IndexSearcher(indexReader);
		
		//通过search方法检索
		TopDocs topDocs = indexSearcher.search(query, Integer.MAX_VALUE);
		System.err.println("总条数:"+topDocs.totalHits);
		ScoreDoc[] scoreDocs = topDocs.scoreDocs;
		
		//便利检索结果
		for(ScoreDoc scoreDoc : scoreDocs){
			System.err.println("得分:"+scoreDoc.score);
			Document doc = indexSearcher.doc(scoreDoc.doc);
			System.err.println("id:"+doc.get("id"));
			System.err.println("title:"+doc.get("title"));
			System.err.println("content:"+doc.get("content"));
		}
		
	}
	
	
	/**
	 * 数字范围搜索
	 * @throws Exception
	 */
	@Test
	public void testNumericRangeQuery() throws Exception{

		//设置查询id范围
		NumericRangeQuery<Long> query = NumericRangeQuery.newLongRange("id", 0L, 1L, true, true);
		
		//创建检索的对象,需要指定从哪里检索
		Directory directory = FSDirectory.open(new File("C:\\Users\\PeiyanZhao\\Desktop\\index"));
		IndexReader indexReader = DirectoryReader.open(directory);
		IndexSearcher indexSearcher = new IndexSearcher(indexReader);
		
		//通过search方法检索
		TopDocs topDocs = indexSearcher.search(query, Integer.MAX_VALUE);
		System.err.println("总条数:"+topDocs.totalHits);
		ScoreDoc[] scoreDocs = topDocs.scoreDocs;
		
		//便利检索结果
		for(ScoreDoc scoreDoc : scoreDocs){
			System.err.println("得分:"+scoreDoc.score);
			Document doc = indexSearcher.doc(scoreDoc.doc);
			System.err.println("id:"+doc.get("id"));
			System.err.println("title:"+doc.get("title"));
			System.err.println("content:"+doc.get("content"));
		}
	}
	
}










































































