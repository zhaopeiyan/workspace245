package test;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.junit.Test;
import org.wltea.analyzer.lucene.IKAnalyzer;

public class test2 {

	/**
	 * 删除测试IndexDelete
	 * 
	 * @throws Exception
	 */
	@Test
	public void testIndexDelete() throws Exception {
		// 创建Directory
		FSDirectory fsDirectory = FSDirectory.open(new File("C:\\Users\\PeiyanZhao\\Desktop\\index"));
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_4_10_3, null);

		// 创建写入对象
		IndexWriter indexWriter = new IndexWriter(fsDirectory, config);

		// 根据term删除索引库 name:java 通过key:value格式传递查询字段
		indexWriter.deleteDocuments(new Term("title", "solr"));
		// 释放资源
		indexWriter.close();

	}

	/**
	 * 更新索引
	 * @throws Exception
	 */
	@Test
	public void testIndexUpdate() throws Exception {
		// 创建建立索引的对象
		FSDirectory fsDirectory = FSDirectory.open(new File("C:\\Users\\PeiyanZhao\\Desktop\\index"));
		// Analyzer analyzer = new StandardAnalyzer();
		IKAnalyzer analyzer = new IKAnalyzer();
		IndexWriterConfig indexWriterConfig = new IndexWriterConfig(Version.LUCENE_4_10_3, analyzer);
		IndexWriter indexWriter = new IndexWriter(fsDirectory, indexWriterConfig);
		// 创建document对象
		Document document = new Document();
		// document文档中添加field
		document.add(new TextField("title", "Solr关山难越谁悲失路之人,萍水相逢尽是他乡之客", Store.YES));
		indexWriter.updateDocument(new Term("id","1"), document);
		// 将文档添加到索引库
		indexWriter.close();
	}
	
	
}
