package cn.itcast.ws.client;

import cn.itcast.ws.weather.impl.WeatherInterfaceImpl;
import cn.itcast.ws.weather.impl.WeatherInterfaceImplService;

public class WeatherClient {

	public static void main(String[] args) {
		//1.创建服务视图
		WeatherInterfaceImplService weatherInterfaceImplService = new WeatherInterfaceImplService();
		//2.获取实现类实例
		WeatherInterfaceImpl weatherInterfaceImpl = weatherInterfaceImplService.getPort(WeatherInterfaceImpl.class);
		//3.调用查询方法
		String weather = weatherInterfaceImpl.queryWeather("Miami");
		System.out.println(weather);
	}
}
