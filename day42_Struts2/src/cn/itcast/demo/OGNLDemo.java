package cn.itcast.demo;

import org.junit.Test;

import cn.itcast.domain.User;
import ognl.Ognl;
import ognl.OgnlContext;
import ognl.OgnlException;


public class OGNLDemo {

	@Test
	public void demo1() throws Exception{
		OgnlContext context = new OgnlContext();
		Object obj = Ognl.getValue("'zhaopeiyan'.length()", context);
		System.out.println(obj);
	}
	
	@Test
	public void demo2() throws Exception{
		OgnlContext context = new OgnlContext();
		Object object = Ognl.getValue("@java.lang.Math@random()", context,context.getRoot());
		System.out.println(object);
	}
	
	@Test
	public void demo3() throws Exception{
		OgnlContext context = new OgnlContext();
		User user = new User();
		user.setName("赵培焱");
		context.setRoot(user);
		String name =(String) Ognl.getValue("name", context,context.getRoot());
		System.out.println(name);
	}
	
}
