package cn.itcast.demo;

import org.apache.struts2.ServletActionContext;
import org.junit.Test;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;

public class demo2 {

	/**
	 * EL表达式对request对象的getAttribute方法进行增强
	 */
	
	
	
	/**
	 * 获取值栈的两种方式
	 */
	@Test
	public void test1(){
		/*
		 * 通过ActionContext获取值栈
		 */
		ActionContext context = ActionContext.getContext();
		ValueStack valueStack = context.getValueStack();
		
		/*
		 * 通过request域对象获取值栈
		 */
		ValueStack stack = (ValueStack)ServletActionContext.getRequest().getAttribute(ServletActionContext.STRUTS_VALUESTACK_KEY);
	}
	
}
