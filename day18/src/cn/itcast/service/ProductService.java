package cn.itcast.service;

import java.util.List;

import cn.itcast.dao.ProductDao;
import cn.itcast.domain.Product;

public class ProductService {
	public List<Product> searchAll() {
		return new ProductDao().searchAll();
	}
}
