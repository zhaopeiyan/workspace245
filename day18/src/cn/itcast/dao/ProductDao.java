package cn.itcast.dao;

import java.sql.SQLException;
import java.util.List;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import cn.itcast.domain.Product;
import cn.itcast.utils.C3P0Utils;

public class ProductDao {

	public List<Product> searchAll() {
		List<Product> products = null;
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "select * from tbl_product";
		try {
			products = qr.query(sql, new BeanListHandler<Product>(Product.class));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("查询数据库异常"+e);
		}
		return products;
	}

}
