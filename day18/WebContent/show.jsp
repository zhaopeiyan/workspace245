<%@page import="cn.itcast.domain.Product"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<table border="1px" align="center" width="50%">
		<tr style="color: red">
			<th>商品id</th>
			<th>商品名称</th>
			<th>商品价格</th>
			<th>商品介绍</th>
		</tr>
		<%-- <c:if test="${not empty products }" >
				<c:forEach items="${products}" var="product">
					<tr align="center">
						<td>${product.pid}</td>
						<td>${product.pname }</td>
						<td>${product.price }</td>
						<td>${product.pdesc }</td>
					</tr>
				</c:forEach>
		</c:if> --%>
		<c:choose>
			<c:when test="${empty products }" >
				<c:out value="暂无数据"></c:out>
			</c:when>
			<c:otherwise>
				<c:forEach items="${products}" var="product">
					<tr align="center">
						<td>${product.pid}</td>
						<td>${product.pname }</td>
						<td>${product.price }</td>
						<td>${product.pdesc }</td>
					</tr>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</table>
</body>
</html>