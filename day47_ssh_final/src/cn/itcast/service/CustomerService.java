package cn.itcast.service;

import cn.itcast.domain.Customer;

public interface CustomerService {

	public void save(Customer customer);

}
