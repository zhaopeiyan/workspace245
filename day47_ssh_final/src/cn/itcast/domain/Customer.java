package cn.itcast.domain;

public class Customer {
	private int cust_id;
	private String cust_name;
	private String cust_age;
	
	
	public int getCust_id() {
		return cust_id;
	}
	public void setCust_id(int cust_id) {
		this.cust_id = cust_id;
	}
	public String getCust_name() {
		return cust_name;
	}
	public void setCust_name(String cust_name) {
		this.cust_name = cust_name;
	}
	public String getCust_age() {
		return cust_age;
	}
	public void setCust_age(String cust_age) {
		this.cust_age = cust_age;
	}
	public Customer() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Customer [cust_id=" + cust_id + ", cust_name=" + cust_name + ", cust_age=" + cust_age + "]";
	}
}
