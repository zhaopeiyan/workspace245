package cn.itcast.dao.impl;

import org.hibernate.Session;
import org.hibernate.Transaction;

import cn.itcast.dao.CustomerDao;
import cn.itcast.domain.Customer;
import cn.itcast.utils.HibernateUtils;

public class CustomerDaoImpl implements CustomerDao {

	@Override
	public void save(Customer customer) {
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		System.out.println("保存");
		session.save(customer);
		transaction.commit();
		session.close();
	}
}
