package cn.itcast.service;

import cn.itcast.domain.Customer;

public interface CustomerService {

	void save(Customer customer);

}
