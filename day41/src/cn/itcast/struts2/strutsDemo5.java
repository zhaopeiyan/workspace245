package cn.itcast.struts2;

import java.util.Map;

import com.opensymphony.xwork2.ActionSupport;

import cn.itcast.domain.User;

public class strutsDemo5 extends ActionSupport {

	private Map<String, User> map;

	public Map<String, User> getMap() {
		return map;
	}

	public void setMap(Map<String, User> map) {
		this.map = map;
	}
	
	public String execute(){
		for(String key : map.keySet()){
			User user = map.get(key);
			System.out.println(user.toString());
		}
		return NONE;
	}

}
