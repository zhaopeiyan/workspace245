package cn.itcast.struts2;

import com.opensymphony.xwork2.ActionSupport;

import cn.itcast.domain.User;

public class actionDemo2 extends ActionSupport{

	private User user;
	
	public User getUser(){
		return user;
	}
	
	public void setUser(User user){
		this.user = user;
	}
	
	public String execute(){
		System.out.println(user.getName());
		System.out.println(user.getAge());
		System.out.println(user.getBirthday());
		System.out.println(user.getSalary());
		
		return NONE;
	}
	
}
