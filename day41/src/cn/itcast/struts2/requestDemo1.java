package cn.itcast.struts2;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class requestDemo1 extends ActionSupport {

	//通过ActionContext类访问Servlet的API
	public String execute(){
		
		ActionContext context = ActionContext.getContext();
		//接收参数
		Map<String, Object> map = context.getParameters();
		for(String key : map.keySet()){
			String[] values = (String[])map.get(key);
			System.out.println(key+"  "+values[0]);
		}
		
		context.put("requestName", "request");
		context.getSession().put("sessionName", "session");
		context.getApplication().put("applicationName", "application");
		
		return SUCCESS;
	}
}
