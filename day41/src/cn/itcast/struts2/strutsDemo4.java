package cn.itcast.struts2;

import java.util.List;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.domain.User;

public class strutsDemo4 extends ActionSupport implements ModelDriven<User>{

	private List<User> list;
	
	public List<User> getList() {
		return list;
	}

	public void setList(List<User> list) {
		this.list = list;
	}

	@Override
	public User getModel() {
		// TODO Auto-generated method stub
		
		return null;
	}
	
	public String execute(){
		for(User user : list){
			System.out.println(user.toString());
		}
		return NONE;
	}
	
}
