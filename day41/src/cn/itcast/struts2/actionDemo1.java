package cn.itcast.struts2;

import java.util.Date;
import com.opensymphony.xwork2.ActionSupport;

public class actionDemo1 extends ActionSupport {
	//接收参数
	private String name;
	private Integer age;
	private Date birthday;
	private Double salary;
	public void setName(String name) {
		this.name = name;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public void setSalary(Double salary) {
		this.salary = salary;
	}
	
	public  String execute(){
		System.out.println(name);
		System.out.println(age);
		System.out.println(birthday);
		System.out.println(salary);
		return NONE;
	}

}
