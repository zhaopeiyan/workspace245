package cn.itcast.struts2;

import java.util.Arrays;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class requestDemo3 extends ActionSupport {
	
	public String execute(){
		//接收参数
		HttpServletRequest request = ServletActionContext.getRequest();
		Map<String, String[]> map = request.getParameterMap();
		for(String key : map.keySet()){
			String[] value = map.get(key);
			System.out.println(key+"	"+Arrays.toString(value));
		}
		//向request域中存值
		request.setAttribute("reqName", "通过ServletActionContext设置request");
		//向session域中存值
		request.getSession().setAttribute("sessName", "通过ServletActionContext设置session");
		request.getSession().getAttribute("sessName");
		//向application域中存值
		ServletActionContext.getServletContext().setAttribute("appName", "通过ServletActionContext设置application");
		return SUCCESS;
	}
}
