package cn.itcast.struts2;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.domain.User;

public class actionDemo3 extends ActionSupport implements ModelDriven<User>{

	
	/**
	 * 手动构建对象
	 */
	private User user = new User();
	
	@Override
	public User getModel() {
		// TODO Auto-generated method stub
		return user;
	}
	
	public String execute(){
		System.out.println(user.toString());
		return NONE;
	}

}
