package cn.itcast.struts2;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;

public class requestDemo2 extends ActionSupport implements ServletRequestAware{

	private HttpServletRequest request;
	@Override
	public void setServletRequest(HttpServletRequest request) {
		// TODO Auto-generated method stub
		this.request = request;
	}
	public String execute(){
		request.setAttribute("msg", "通过ServletRequestAware接口实现了访问Servlet API");
		return SUCCESS;
	}

}
