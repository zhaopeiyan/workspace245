<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>继承ActionContext获取数据</h1>
	${requestName }
	${sessionName }
	${applicationName }
	<h1>继承ActionContext并实现ServletRequestAware获取数据</h1>
	${msg }
	<h1>使用ServletActionContext</h1>
	${reqName }
	${sessName }
	${appName }
</body>
</html>