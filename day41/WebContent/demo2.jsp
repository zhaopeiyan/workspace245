<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h3>数据的封装方式二:页面提供表达式的方式</h3>
<form action="${pageContext.request.contextPath}/actionDemo2.action " method="post">
	name:<input type="text" name="user.name">
	age:<input type="text" name="user.age">
	birthday:<input type="text" name="user.birthday">
	salary:<input type="text" name="user.salary">
	<input type="submit" value="提交">
</form>
</body>
</html>