package cn.itcast.web.servlet;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.itcast.domain.User;
import cn.itcast.service.UserService;

public class userLoginServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		//1、获取用户输入的参数
		User user = new User();
		String username = request.getParameter("username");
		user.setUsername(username);
		String password = request.getParameter("password");
		user.setPassword(password);
		//2、调用service层处理业务逻辑
		UserService us = new UserService();
		User existUser = us.login(user);
		if(existUser!=null){
			//3、判断用户是否勾选了自动登录
			String autoLogin = request.getParameter("autoLogin");
			//4、将数据保存到session
			request.getSession().setAttribute("existUser", existUser);;
			if(autoLogin!=null){
				Cookie cookie = new Cookie("autoLogin",username+":"+password);
				cookie.setMaxAge(60*60*24*7);
				cookie.setPath("/");
				response.addCookie(cookie);
			}
//			request.setAttribute("msg", "登陆成功");
			response.sendRedirect("success.jsp");
//			response.getWriter().print("登录成功");
		}else{
//			request.setAttribute("msg", "登陆失败");
			response.sendRedirect("fail.jsp");
//			response.getWriter().print("登录失败");
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}