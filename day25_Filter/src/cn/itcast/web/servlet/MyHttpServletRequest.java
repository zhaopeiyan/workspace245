package cn.itcast.web.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

//1、第一步继承与被装饰类相同的接口或继承该接口的包装类
public class MyHttpServletRequest extends HttpServletRequestWrapper {
	//3、在自己定义的方法中必须要有一个成员变量来存储已知实现类对象
	private HttpServletRequest request;
	//2、在自己定义的方法中构造方法必须有接口的已知实现类的引用(父接口、多态、形式参数)
	public MyHttpServletRequest(HttpServletRequest request) {
		super(request);
		this.request = request;
	}
	/**
	 * 4、对指定的方法进行增强，解决get请求乱码问题。此处是增强getParameter()方法
	 * @param name
	 * @return
	 */
	@Override
	public String getParameter(String name) {
		// TODO Auto-generated method stub
		String parameter = request.getParameter(name);
		try {
			parameter = new String(parameter.getBytes("iso8859-1"),"utf-8");
		} catch (Exception e) {
			throw new RuntimeException("处理get请求乱码异常" + e);
		}
		return parameter;
	}
	/**
	 * 5、对后续需要的方法进行实现
	 */
	@Override
	public String getMethod() {
		// TODO Auto-generated method stub
		return request.getMethod();
	}
}
