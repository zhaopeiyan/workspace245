package cn.itcast.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.itcast.domain.User;
import cn.itcast.service.UserService;
import cn.itcast.utils.CookieUtils;

/**
 * Servlet Filter implementation class loginFilter
 */
public class loginFilter implements Filter {

    /**
     * Default constructor. 
     */
    public loginFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here
		//强转成HttpServlet类对象
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;
		User existUser = (User) req.getSession().getAttribute("existUser");
		if(existUser != null){
			chain.doFilter(req, res);
			return;
		}
		Cookie[] cookies = req.getCookies();
		//找到浏览器cookie
		Cookie autoLogin = CookieUtils.getCookieByName(cookies,"autoLogin");
		if(autoLogin!=null){
			String value = autoLogin.getValue();
			String[] split = autoLogin.getValue().split(":");
			HttpSession session = req.getSession();
			User user = new User();
			user.setUsername(split[0]);
			user.setPassword(split[1]);
			User us = new UserService().login(user);
			session.setAttribute("existUser", us);
			System.out.println(split[0]);
			System.out.println(split[1]);
		}
		// pass the request along the filter chain
		chain.doFilter(req, res);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
