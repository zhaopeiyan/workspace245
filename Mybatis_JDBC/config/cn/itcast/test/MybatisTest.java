package cn.itcast.test;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import cn.itcast.domain.User;

public class MybatisTest {

	private SqlSessionFactory sqlSessionFactory = null;
	
	@Before
	public void init() throws Exception{
		//1.创建SQLSessionFactoryBuilder对象
		SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
		//2.加载SqlMapConfig.xml
		InputStream inputStream = Resources.getResourceAsStream("SqlMapConfig.xml");
		//3.创建SQLSessionFactory对象
		this.sqlSessionFactory = sqlSessionFactoryBuilder.build(inputStream);
	}
	
	@Test
	public void deleteUserByName(){
		//4.创建SQLSession对象
		 SqlSession sqlSession = sqlSessionFactory.openSession();
		 sqlSession.delete("deleteUserByName", "Chirst.Paul");
		 sqlSession.commit();
		 sqlSession.close();
	}
	
	@Test
	public void updateUser(){
		//4.创建SQLSession对象
		SqlSession sqlSession = sqlSessionFactory.openSession();
		
		User updateUser = new User();
		updateUser.setId(33);
		updateUser.setUsername("John.Wall");
		updateUser.setBirthday(new Date());
		updateUser.setSex("1");
		updateUser.setAddress("wahston.Wrizters");
		
		sqlSession.update("updateUser", updateUser);
		sqlSession.commit();
		sqlSession.close();
	}
	
	@Test
	public void addUserTest(){
		//4.创建SQLSession对象
		SqlSession openSession = sqlSessionFactory.openSession();
		
		User addUser = new User();
		addUser.setUsername("Cam.NewTon");
		addUser.setBirthday(new Date());
		addUser.setAddress("N.C");
		//5.执行SQLSession对象执行查询,获取结果user
		openSession.insert("addUser", addUser);
		System.err.println(addUser);
		//6.提交修改
		openSession.commit();
		//7.释放资源
		openSession.close();
	}
	@Test
	public void queryUserByNameTest(){
		//4.创建SQLSession对象
		SqlSession openSession = sqlSessionFactory.openSession();
		//5.执行SQLSession对象执行查询,获取结果user
		List<User> list = openSession.selectList("queryUserByName", "张");
		//6.处理结果集对象
		for(User user : list){
			System.err.println(user.toString());
		}
		//7.释放资源
		openSession.close();
	}
	
	@Test
	public void queryUserByIdTest() {
		//4.创建SQLSession对象
		SqlSession sqlSession = sqlSessionFactory.openSession();
		//5.执行SQLSession对象执行查询,获取结果User
		//查询多条数据使用selectList方法
		Object user = sqlSession.selectOne("queryUserById", 10);
		//6.打印结果
		System.err.println(user.toString());
		//7.释放资源
		sqlSession.close();
	}

}
