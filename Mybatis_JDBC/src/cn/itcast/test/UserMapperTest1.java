package cn.itcast.test;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import cn.itcast.dao.UserMapper;
import cn.itcast.domain.User;

public class UserMapperTest1 {
	
private SqlSessionFactory sqlSessionFactory;
	
	@Before
	public void init() throws Exception{
		//创建SQLSessionFactoryBuilder
		SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
		//加载SqlMapConfig配置文件
		InputStream inputStream = Resources.getResourceAsStream("SqlMapConfig.xml");
		//创建SqlSessionFactory
		this.sqlSessionFactory = sqlSessionFactoryBuilder.build(inputStream);
	}
	
	@Test
	public void testqueryUserById() {
		//获取SQLSession,和spring整合后由spring管理
		SqlSession sqlSession = this.sqlSessionFactory.openSession();
		//从SQLSession中获取Mapper接口的代理对象
		UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
		
		//执行查询方法
		User user = userMapper.queryUserById(1);
		System.err.println(user.toString());
		sqlSession.close();
	}

	@Test
	public void testQueryUserByName() {
		SqlSession openSession = this.sqlSessionFactory.openSession();
		UserMapper userMapper = openSession.getMapper(UserMapper.class);
		List<User> list = userMapper.queryUserByName("j");
		for(User user : list){
			System.err.println(user.toString());
		}
		openSession.close();
	}

	@Test
	public void testAddUser() {
		SqlSession openSession = this.sqlSessionFactory.openSession();
		UserMapper userMapper = openSession.getMapper(UserMapper.class);
		User user = new User();
		user.setUsername("Berzil.Rio");
		user.setBirthday(new Date());
		user.setSex("1");
		user.setAddress("巴西里约热内卢燃情海滩");
		userMapper.addUser(user );
		openSession.commit();
		System.err.println(user.toString());
		openSession.close();
	}

}
