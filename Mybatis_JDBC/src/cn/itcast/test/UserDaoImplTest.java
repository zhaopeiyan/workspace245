package cn.itcast.test;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import cn.itcast.dao.UserDaoImpl;
import cn.itcast.domain.User;

public class UserDaoImplTest {

	private SqlSessionFactory sqlSessionFactory;
	
	@Before
	public void init() throws Exception{
		//创建SQLSessionFactoryBuilder
		SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
		//加载SqlMapConfig配置文件
		InputStream inputStream = Resources.getResourceAsStream("SqlMapConfig.xml");
		//创建SqlSessionFactory
		this.sqlSessionFactory = sqlSessionFactoryBuilder.build(inputStream);
	}
	
	@Test
	public void testQueryUserById() {
		//创建DAO
		UserDaoImpl userDao = new UserDaoImpl(this.sqlSessionFactory);
		//执行查询
		User user = userDao.queryUserById(1);
		System.err.println(user.toString());
	}

	@Test
	public void testQueryUserByName() {
		UserDaoImpl userDaoImpl = new UserDaoImpl(this.sqlSessionFactory);
		List<User> list = userDaoImpl.queryUserByName("n");
		for(User user : list){
			System.err.println(user.toString());
		}
	}

	@Test
	public void testAddUser() {
		UserDaoImpl userDaoImpl = new UserDaoImpl(this.sqlSessionFactory);
		User addUser = new User();
		addUser.setUsername("Paul.Groger");
		addUser.setBirthday(new Date());
		addUser.setSex("1");
		addUser.setAddress("IndiannaBolis.Peacer");
		userDaoImpl.addUser(addUser );
		System.err.println(addUser.toString());
	}

}
