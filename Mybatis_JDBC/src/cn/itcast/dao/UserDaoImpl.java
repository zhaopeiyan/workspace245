package cn.itcast.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import cn.itcast.domain.User;

public class UserDaoImpl implements IUserDao {

	private SqlSessionFactory sqlSessionFactory;
	
	public UserDaoImpl(SqlSessionFactory sqlSessionFactory) {
		this.sqlSessionFactory = sqlSessionFactory;
	}
	
	@Override
	public User queryUserById(int id) {
		// 创建SQLSession
		SqlSession openSession = this.sqlSessionFactory.openSession();
		//执行查询逻辑
		User user = openSession.selectOne("queryUserById", id);
		System.err.println(user.toString());
		openSession.close();
		return user;
	}

	@Override
	public List<User> queryUserByName(String username) {
		// 创建SQLSession
		SqlSession openSession = this.sqlSessionFactory.openSession();
		List<User> list = openSession.selectList("queryUserByName", username);
		openSession.close();
		return list;
	}

	@Override
	public void addUser(User user) {
		// 创建SQLSession
		SqlSession openSession = this.sqlSessionFactory.openSession();
		openSession.insert("addUser", user);
		openSession.commit();
		openSession.close();
	}

}
