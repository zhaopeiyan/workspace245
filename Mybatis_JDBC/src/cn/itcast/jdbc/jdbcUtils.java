package cn.itcast.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;


/**
 * <p>Title:jdbcUtils</p>
 * <p>Description:jdbc原始方法(未经封装)实现查询数据库</p>
 * <p>Company:</p>
 * @author PeiyanZhao
 * @date2017年5月8日 下午3:55:23
 */
public class jdbcUtils {
	
	public static void main(String[] args) {
		
		jdbcBase();
	}

	@Test
	private static void jdbcBase() {
		Connection connection = null;
		PreparedStatement prepareStatement = null;
		ResultSet executeQuery = null;
		//jdbc编程步骤
		try {
			//1.加载数据库驱动
			Class.forName("com.mysql.jdbc.Driver");
			//2.创建并获取数据库连接
			connection = DriverManager.getConnection("jdbc:mysql:///mybatis?characterEncoding=utf-8", "root", "root");
			//3.创建jdbc statement对象
			//4.设置SQL语句
			String sql = 
					"select * from user where username = ?";
			prepareStatement = connection.prepareStatement(sql);
			//5.设置SQL语句中的参数(使用preparedStatement)
			prepareStatement.setString(1, "Lebron.James");
			//6.通过statement执行SQL并获取结果
			executeQuery = prepareStatement.executeQuery();
			//7.对SQL执行结果进行解析处理,遍历查询结果集
			while(executeQuery.next()){
				System.err.println(executeQuery.getString("id")+" "+
			executeQuery.getString("username"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			//8.释放资源(resultSet\preparedstatement\connection)
			if(executeQuery != null){
				try {
					executeQuery.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(prepareStatement != null){
				try {
					prepareStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
