package cn.itcast.test;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import cn.itcast.domain.Role;
import cn.itcast.domain.User;
import cn.itcast.utils.HibernateUtils;

public class demo2 {
	
	@Test
	/**
	 * 将30号用户的8号角色改为13号角色
	 */
	public void test5(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		//获取30号用户
		User user = session.get(User.class, 30l);
		
		Role role = session.get(Role.class, 8l);
		Role role2 = session.get(Role.class, 13l);
		
		user.getRoles().remove(role);
		user.getRoles().add(role2);
		
		transaction.commit();
		session.close();
	}
	
	@Test
	/**
	 * 级联删除:删除角色   级联删除用户
	 */
	public void test4(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		Role role = session.get(Role.class, 7L);
		session.delete(role);
		
		session.delete(role);
		transaction.commit();
		session.close();
	}
	
	
	@Test
	/**
	 * 级联删除:删除用户  级联删除角色
	 */
	public void test3(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		User user = session.get(User.class, 31L);
		session.delete(user);
		
		transaction.commit();
		session.close();
	}
	
	@Test
	/**
	 * 多对多只保存一边  也是不可以的
	 * 配置级联:保存用户级联角色,在User.hbm.xml中<set>上配置cascade="save-update"
	 */
	public void test2(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		//创建两个用户
		User user1 = new User();
		user1.setUser_name("赵晋勇");
		
		User user2 = new User();
		user2.setUser_name("郝强勇");
		
		//创建两个角色
		Role role1 = new Role();
		role1.setRole_name("讲师");
		Role role2 = new Role();
		role2.setRole_name("助教");
		
		//建立关系
		user1.getRoles().add(role2);
		role2.getUsers().add(user1);
		
		user2.getRoles().add(role2);
		user2.getRoles().add(role1);
		role1.getUsers().add(user2);
		role2.getUsers().add(user2);
		
		
		session.save(user1);
		session.save(user2);
		session.save(role1);
		session.save(role2);
		
		transaction.commit();
		session.close();
	}
	
	
	
	@Test
	//保存用户和角色
	public void test1(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		User user1 = new User();
		user1.setUser_name("james.harden");
		User user2 = new User();
		user2.setUser_name("cuban");
		User user3 = new User();
		user3.setUser_name("popovich");
		
		Role role1 = new Role();
		role1.setRole_name("coach");
		Role role2 = new Role();
		role2.setRole_name("player");
		Role role3 = new Role();
		role3.setRole_name("manager");
		
		//如果多对多建立了双向关联,一定要有一方放弃外键维护权
	/*	user1.getRoles().add(role1);
		user1.getRoles().add(role3);
		role1.getUsers().add(user1);
		role3.getUsers().add(user1);*/
		
		
		user2.getRoles().add(role2);
		role2.getUsers().add(user2);
		
		
		user2.getRoles().add(role3);
		role3.getUsers().add(user2);
		
		session.save(user1);
		session.save(user2);
		
		session.save(role1);
		session.save(role2);
		session.save(role3);
		
		transaction.commit();
		session.close();
	}
}
