package cn.itcast.test;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import cn.itcast.domain.Customer;
import cn.itcast.domain.Linkman;
import cn.itcast.utils.HibernateUtils;

public class demo {
	
	
	
	
	@Test
	/**
	 * 修改操作
	 * 将15号联系人(John.Jason)关联到3号客户(赵总)    通过设置一的一方交出外键的维护权优化程序
	 * 此方式会产生1条update语句     不会 产生多余的sql
	 */
	public void Test8(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		Customer customer = session.get(Customer.class, 3l);
		Linkman linkman = session.get(Linkman.class, 15l);
		
		linkman.setCustomers(customer);
		customer.getLinkmans().add(linkman);
		
		transaction.commit();
	}
	
	
	@Test
	/**
	 * 修改操作
	 * 将17号联系人(Paul.Wall)关联到8号客户(刘总)
	 * 此方式会产生2条update语句      产生了多余的sql
	 */
	public void Test7(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		Customer customer = session.get(Customer.class, 8l);
		Linkman linkman = session.get(Linkman.class, 17l);
		
		linkman.setCustomers(customer);
		customer.getLinkmans().add(linkman);
		
		transaction.commit();
	}
	
	@Test
	/**
	 * 级联删除:级联删除有方向性
	 * 删除联系人同时级联删除客户
	 * 在LinkMan.hbm.xml中many-to-one标签上配置cascade="delete"
	 */
	public void Test6(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		//级联删除,必须是先查询再删除的
		Linkman linkman = session.get(Linkman.class, 11l);
		session.delete(linkman);
		
		transaction.commit();
		session.close();
	}
	
	@Test
	/**
	 * 删除有关联关系的对象(默认情况:先将关联对象的外键置为null,删除客户对象)
	 * 
	 */
	public void test5(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		Customer customer = session.get(Customer.class, 6l);
		session.delete(customer);
		
		transaction.commit();
		session.close();
	}
	
	@Test
	/**
	 * 测试对象导航和级联操作
	 */
	public void text4(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		//创建一个客户
		Customer customer = new Customer();
		customer.setCust_name("Tim.Cook");
		
		//创建三个联系人
		Linkman linkman1 = new Linkman();
		Linkman linkman2 = new Linkman();
		Linkman linkman3 = new Linkman();
		
		linkman1.setLkm_name("Paul.Wall");
		linkman2.setLkm_name("James.White");
		linkman3.setLkm_name("John.Jason");
		
		//建立关系
		linkman1.setCustomers(customer);
		customer.getLinkmans().add(linkman2);
		customer.getLinkmans().add(linkman3);
		
		session.save(linkman1);//执行4条insert语句     一条insert  linkman1      一条insert customer    一条insert linkman2   一条insert   linkman3
//		session.save(customer);//执行3条insert语句   一条insert  customer    一条insert  linkman2    一条insert   linkman3
//		session.save(linkman2);//执行1条insert语句    insert  linkman2
		
		transaction.commit();
		session.close();
	}
	
	
	/**
	 * 保存linkman级联customer
	 */
	@Test
	public void test3(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		//创建一个客户
		Customer customer = new Customer();
		customer.setCust_name("刘总");
		
		//创建两个联系人
		Linkman linkman1 = new Linkman();
		linkman1.setLkm_name("章秘书");
		
		//建立关系
		customer.getLinkmans().add(linkman1);
		linkman1.setCustomers(customer);
		
		session.save(linkman1);
		
		transaction.commit();
		session.close();
		}
	
	/**
	 * 保存customer级联linkman
	 */
	@Test
	public void test2(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		//创建一个客户
		Customer customer = new Customer();
		customer.setCust_name("马总");
		
		//创建两个联系人
		Linkman linkman1 = new Linkman();
		Linkman linkman2 = new Linkman();
		linkman1.setLkm_name("白秘书");
		linkman2.setLkm_name("柳秘书");
		
		//建立关系
		customer.getLinkmans().add(linkman1);
		customer.getLinkmans().add(linkman2);
		linkman1.setCustomers(customer);
		linkman2.setCustomers(customer);
		
		session.save(customer);
		/*session.save(linkman1);
		session.save(linkman2);*/
		
		transaction.commit();
		session.close();
		}
	
	@Test
	public void test1(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		//创建一个客户
		Customer customer = new Customer();
		customer.setCust_name("赵总");
		
		//创建两个联系人
		Linkman linkman1 = new Linkman();
		Linkman linkman2 = new Linkman();
		linkman1.setLkm_name("小刘");
		linkman2.setLkm_name("小曹");
		
		//建立关系
		customer.getLinkmans().add(linkman1);
		customer.getLinkmans().add(linkman2);
		linkman1.setCustomers(customer);
		linkman2.setCustomers(customer);
		
		session.save(customer);
		session.save(linkman1);
		session.save(linkman2);
		
		transaction.commit();
		session.close();
	}
}
