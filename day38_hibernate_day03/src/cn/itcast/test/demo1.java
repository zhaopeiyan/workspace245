package cn.itcast.test;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import cn.itcast.domain.Customer;
import cn.itcast.domain.Linkman;
import cn.itcast.utils.HibernateUtils;

public class demo1 {
	@Test
	public void test1(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		Customer customer = new Customer();
		customer.setCust_name("赵培焱");
		
		Linkman linkman = new Linkman();
		linkman.setLkm_name("张秘书");
		
		//在customer.hbm.xml中的set标签内设置cascade="save-update"  inverse="true"
		customer.getLinkmans().add(linkman);
		
		session.save(customer);
		
		transaction.commit();
	}
}
