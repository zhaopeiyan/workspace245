package cn.itcast.testByimooc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.management.Query;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import cn.itcast.domain.Student;
import cn.itcast.utils.HibernateUtils;

public class Demo {
	@Test
	public void testSaveStudent() throws Exception{
		
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		Student s = new Student();
		s.setSname("小明");
		s.setGender("男");
		s.setBirthday(new Date());
		s.setAddress("北京");
		
		//先获得一个照片文件
		File f = new File("e:"+File.separator+"50.jpg");
		//获得文件输入流
		InputStream in = new FileInputStream(f);
		//创建一个Blob对象
		Blob image = Hibernate.getLobCreator(session).createBlob(in, in.available());
		//设置照片属性
		s.setPicture(image);

		session.save(s);
		transaction.commit();
	}
	
	@Test
	public void testGetPicture() throws Exception{
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		Student student = session.get(Student.class, 1);
		Blob image = student.getPicture();
		
		//获得照片输入流
		InputStream inputStream = image.getBinaryStream();
		//创建输出流
		File file = new File("e:"+File.separator+"66.jpg");
		FileOutputStream outputStream = new FileOutputStream(file);
		
		//创建缓冲区
		byte[] buff = new byte[inputStream.available()];
		inputStream.read(buff);
		outputStream.write(buff);
		inputStream.close();
		outputStream.close();
		
	}
}
