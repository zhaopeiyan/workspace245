package cn.itcast.domain;

public class User_Role {
	
	 /*`role_id` bigint(32) NOT NULL COMMENT '角色id',
	  `user_id` bigint(32) NOT NULL COMMENT '用户id',*/
	
	private Long role_id;
	private Long user_id;
	public Long getRole_id() {
		return role_id;
	}
	public void setRole_id(Long role_id) {
		this.role_id = role_id;
	}
	public Long getUser_id() {
		return user_id;
	}
	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}
	public User_Role() {
		
	}
}
