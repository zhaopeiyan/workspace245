package cn.itcast.test04;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import cn.itcast.domain.Customer;
import cn.itcast.domain.Linkman;
import cn.itcast.utils.HibernateUtils;

public class HQLDemo {
	
	//=========================HQL链接查询=========================
	@Test
	/**
	 * HQL的多表的连接查询:迫切内连接(使用distinct去除重复数据)
	 */
	public void test9(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		List<Customer> list = session.createQuery("select distinct c from Customer c inner join fetch c.linkmans").list();
		
		for(Customer c : list){
			System.out.println(c);
		}
		
		transaction.commit();
		session.close();
	}
	@Test
	/**
	 * HQL的多表的连接查询:迫切内连接
	 */
	public void test8(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		List<Customer> list = session.createQuery("from Customer c inner join fetch c.linkmans").list();
		
		for(Customer c : list){
			System.out.println(c);
		}
		
		transaction.commit();
		session.close();
	}
	@Test
	/**
	 * HQL的多表的连接查询:内连接
	 */
	public void test7(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		List<Object[]> list = session.createQuery("from Customer c inner join c.linkmans").list();
		
		for(Object[] objects : list){
			System.out.println(Arrays.toString(objects));
		}
		
		transaction.commit();
		session.close();
	}
	
	
	
	
	
	
	
	
	
	
	
	//=========================HQL检索================================
	@Test
	/**
	 * 投影检索
	 */
	public void test6(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		/*List<String> list = session.createQuery("SELECT cust_name from Customer").list();
		for(String s : list){
			System.out.println(s);
		}*/
		
		//投影查询多列
		/*List<Object[]> list = session.createQuery("select cust_id,cust_name from Customer").list();
		for(Object[] objects : list){
			System.out.println(Arrays.toString(objects));
		}*/
		
		//投影的构造的方式查询
		List<Customer> list = session.createQuery("select new Customer(cust_id,cust_name) from Customer").list();
		for(Customer s : list){
			System.out.println(s.toString());
		}
		
		transaction.commit();
		session.close();
	}
	
	@Test
	/**
	 * 统计查询
	 */
	public void test5(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		Query query = session.createQuery("select count(*) from Customer");
		Long num = (Long)query.uniqueResult();
		System.out.println(num);
		
		transaction.commit();
		session.close();
	}
	
	@Test
	/**
	 * 分页检索
	 */
	public void test4(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		Query query = session.createQuery("from Linkman order by lkm_id desc");
		query.setFirstResult(2);
		query.setMaxResults(2);
		List<Linkman> list = query.list();
		
		for(Linkman l : list){
			System.out.println(l.getLkm_name());
		}
		transaction.commit();
		session.close();
	}
	
	@Test
	/**
	 * 条件检索
	 */
	public void test3(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		//按位置绑定参数
		/*Query query = session.createQuery("from Customer where cust_name=?");
		query.setString(0, "赵培焱");*/
		
		//按名称绑定参数
		Query query = session.createQuery("from Customer where cust_name=:name");
		query.setParameter("name", "刘总");
		
		
		List<Customer> list = query.list();
		Customer customer = (Customer)query.uniqueResult();
		Set<Linkman> linkmans = customer.getLinkmans();
		
		for(Linkman l : linkmans){
			System.out.println(customer.getCust_name()+":"+l.getLkm_name());
		}
		
		transaction.commit();
		session.close();
	}
	
	
	
	@Test
	/**
	 * 排序检索
	 */
	public void test2(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		//基本查询
		Query query = session.createQuery("from Customer order by cust_id desc");
		List<Customer> list = query.list();
		
		for(Customer c : list){
			System.out.println(c.toString());
		}
		
		transaction.commit();
		session.close();
	}
	
	
	@Test
	/**
	 * 基本检索
	 */
	public void test1(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		//基本查询
		/*Query query = session.createQuery("from Customer");
		List<Customer> list = query.list();*/
		
		//起别名
		Query query = session.createQuery("select c from Customer c");
		List<Customer> list = query.list();
		
		for(Customer c : list){
			System.out.println(c.getCust_name());
		}
		
		transaction.commit();
		session.close();
	}

}
