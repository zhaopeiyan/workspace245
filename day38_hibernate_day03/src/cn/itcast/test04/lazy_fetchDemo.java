package cn.itcast.test04;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import cn.itcast.domain.Customer;
import cn.itcast.domain.Linkman;
import cn.itcast.utils.HibernateUtils;

public class lazy_fetchDemo {
	
	//=============================批量抓取=======================
	
	@Test
	/**
	 * 查询客户的时候批量抓取联系人
	 * 在Customer.hbm.xml中<Set>标签上配置batch-size="4"
	 */
	public void test2(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		List<Customer> list = session.createQuery("from Customer").list();
		for(Customer c : list){
			System.out.println("客户名称:"+c.getCust_name());
			for(Linkman l : c.getLinkmans()){
				System.out.println("客户对应的联系人的名称:"+l.getLkm_name());
			}
		}
		
		
		transaction.commit();
		session.close();
	}
	
	//============================检索策略（对查询进行优化）=====================
	@Test
	/**
	 * 查询3号客户，同时查询3号客户的联系人的数量（默认情况）
	 */
	public void test1(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		//查询1号客户
		Customer customer = session.get(Customer.class, 9l);
		
		//获得一号客户的联系人的数量
		System.out.println(customer.getLinkmans().size());
		
		transaction.commit();
		session.close();
		
		
	}

}
