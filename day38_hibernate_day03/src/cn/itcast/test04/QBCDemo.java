package cn.itcast.test04;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.junit.Test;

import cn.itcast.domain.Customer;
import cn.itcast.domain.Linkman;
import cn.itcast.utils.HibernateUtils;

public class QBCDemo {
	
	//===================================QBC检索=========================
	
	
	@Test
	/**
	 * 离线条件查询:DetachedCriteria(SSH整合经常使用)
	 */
	public void test6(){
		//获取一个离线条件查询的对象
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Customer.class);
		detachedCriteria.add(Restrictions.eqOrIsNull("cust_name", "赵总"));
		
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		List<Customer> list = detachedCriteria.getExecutableCriteria(session).list();
		
		for(Customer c : list){
			System.out.println(c);
		}
		
		transaction.commit();
		session.close();
	}
	
	@Test
	/**
	 * 统计检索
	 */
	public void test5(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		Criteria criteria = session.createCriteria(Linkman.class);
		criteria.setProjection(Projections.rowCount());
		Long uniqueResult = (Long) criteria.uniqueResult();
		System.out.println(uniqueResult);
		
		transaction.commit();
		session.close();
	}
	
	@Test
	/**
	 * 排序检索
	 */
	public void test4(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		//排序查询
		Criteria criteria = session.createCriteria(Linkman.class);
		criteria.addOrder(Order.desc("lkm_id"));
		
		List<Linkman> list = criteria.list();
		
		for(Linkman l : list){
			System.out.println(l.getLkm_name());
		}
		
		transaction.commit();
		session.close();
	}
	
	@Test
	/**
	 * 分页检索
	 */
	public void test3(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		//条件查询
		Criteria criteria = session.createCriteria(Linkman.class);
		//设置分页查询
		criteria.setFirstResult(2);
		criteria.setMaxResults(2);
		
		List<Linkman> list = criteria.list();
		
		for(Linkman l : list){
			System.out.println(l.getLkm_name());
		}
		
		transaction.commit();
		session.close();
	}
	
	
	@Test
	/**
	 * 条件查询
	 */
	public void test2(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		//条件查询
		Criteria criteria = session.createCriteria(Customer.class);
		//设置条件
		criteria.add(Restrictions.like("cust_name", "%总%"));
		criteria.add(Restrictions.gt("cust_id", 3l));
		
		List<Customer> list = criteria.list();
		
		for(Customer c : list){
			System.out.println(c);
		}
		
		transaction.commit();
		session.close();
	}
	
	@Test
	/**
	 * 基本检索
	 */
	public void test1(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		//简单查询:
		Criteria criteria = session.createCriteria(Customer.class);
		List<Customer> list = criteria.list();
		
		for(Customer c : list){
			System.out.println(c);
		}
		
		transaction.commit();
		session.close();
	} 

}
