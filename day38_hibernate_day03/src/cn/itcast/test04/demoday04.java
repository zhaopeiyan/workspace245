package cn.itcast.test04;

import java.util.Arrays;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import cn.itcast.domain.Customer;
import cn.itcast.domain.Linkman;
import cn.itcast.utils.HibernateUtils;

public class demoday04 {
	
	

	//===================================OID检索方式===============================
	@Test
	public void test4(){
		Session session = HibernateUtils.openSession();
		Customer customer = session.get(Customer.class, 3l);
		Customer customer2 = session.load(Customer.class, 7l);
		
		System.out.println(customer.getCust_name());
		System.out.println(customer2.getCust_name());
	}
	
	
	//===================================对象导航检索=================================
	@Test
	public void test3(){
		Session session = HibernateUtils.openSession();
		
		Customer customer = session.load(Customer.class, 7l);
		Set<Linkman> linkmans = customer.getLinkmans();
		for(Linkman lm : linkmans){
			System.out.println(lm.getLkm_name());
		}
	}
	
	@Test
	public void test2(){
		Session session = HibernateUtils.openSession();
		
		Customer customer = session.get(Customer.class, 7l);
		Set<Linkman> linkmans = customer.getLinkmans();
		for(Linkman lm : linkmans){
			System.out.println(lm.getLkm_name());
		}
	}
	
	@Test
	public void test1(){
		Session session = HibernateUtils.openSession();
		Transaction transaction = session.beginTransaction();
		
		Linkman linkman = session.get(Linkman.class, 10l);
		Customer customer = linkman.getCustomers();
		
		System.out.println(linkman.getLkm_name()+":"+customer.getCust_name());
		
		transaction.commit();
		session.close();
	}

}
