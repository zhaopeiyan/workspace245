package cn.itcast.test;

import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import cn.itcast.domain.Order;
import cn.itcast.domain.User;
import cn.itcast.mapper.OrderMapper;

public class OrderMapperTest {
	
	private SqlSessionFactory sqlSessionFactory;
	
	@Before
	public void init() throws Exception{
		SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
		InputStream inputStream = Resources.getResourceAsStream("SqlMapConfig.xml");
		this.sqlSessionFactory = sqlSessionFactoryBuilder.build(inputStream);
	}
	
	@Test
	public void queryUserOrders(){
		SqlSession openSession = this.sqlSessionFactory.openSession();
		OrderMapper orderMapper = openSession.getMapper(OrderMapper.class);
		List<User> list2 = orderMapper.queryUserOrders();
		for(User u : list2){
			System.err.println(u);
		}
		openSession.close();
	}
	
	@Test
	public void testQueryOrders() {
		SqlSession openSession = sqlSessionFactory.openSession();
		OrderMapper orderMapper = openSession.getMapper(OrderMapper.class);
		List<Order> list = orderMapper.queryOrders();
		for(Order order : list){
			System.err.println(order.toString());
		}
		openSession.close();
		
	}

}
