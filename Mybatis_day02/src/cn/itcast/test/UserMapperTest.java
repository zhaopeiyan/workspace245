package cn.itcast.test;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import cn.itcast.domain.Order;
import cn.itcast.domain.OrderUser;
import cn.itcast.domain.QueryVo;
import cn.itcast.domain.User;
import cn.itcast.mapper.UserMapper;

public class UserMapperTest {
	
private SqlSessionFactory sqlSessionFactory;
	
	@Before
	public void init() throws Exception{
		//创建SQLSessionFactoryBuilder
		SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
		//加载SqlMapConfig配置文件
		InputStream inputStream = Resources.getResourceAsStream("SqlMapConfig.xml");
		//创建SqlSessionFactory
		this.sqlSessionFactory = sqlSessionFactoryBuilder.build(inputStream);
	}
	
	
	
	@Test
	public void queryorderUserResultMap(){
		SqlSession openSession = this.sqlSessionFactory.openSession();
		UserMapper userMapper = openSession.getMapper(UserMapper.class);
		List<Order> list = userMapper.queryorderUserResultMap();
		for(Order u : list){
			System.err.println(u);
		}
		openSession.close();
	}
	
	@Test
	public void queryOrderUser(){
		SqlSession openSession = this.sqlSessionFactory.openSession();
		UserMapper userMapper = openSession.getMapper(UserMapper.class);
		List<OrderUser> list = userMapper.queryOrderUser();
		for(OrderUser u : list){
			System.err.println(u);
		}
		openSession.close();
	}
	
	@Test
	public void queryUserByWhere(){
		SqlSession openSession = this.sqlSessionFactory.openSession();
		UserMapper userMapper = openSession.getMapper(UserMapper.class);
		User user = new User();
		user.setUsername("i");
		//user.setSex("2");
		List<User> list = userMapper.queryUserByWhere(user);
		for(User u : list){
			System.err.println(u);
		}
		openSession.close();
	}
	
	@Test
	public void queryUserCount(){
		SqlSession openSession = this.sqlSessionFactory.openSession();
		UserMapper userMapper = openSession.getMapper(UserMapper.class);
		Integer count = userMapper.queryUserCount();
		System.err.println(count);
		
		openSession.close();
	}
	
	@Test
	public void testqueryUserByQueryVo() {
		//获取SQLSession,和spring整合后由spring管理
		SqlSession sqlSession = this.sqlSessionFactory.openSession();
		//从SQLSession中获取Mapper接口的代理对象
		UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
		
		QueryVo queryVo = new QueryVo();
		User user = new User();
		user.setUsername("张");
		queryVo.setUser(user);
		//执行查询方法
		List<User> list = userMapper.queryUserByQueryVo(queryVo);
		for(User u : list){
			System.err.println(u.toString());
		}
		sqlSession.close();
	}
	
	@Test
	public void testqueryUserById() {
		//获取SQLSession,和spring整合后由spring管理
		SqlSession sqlSession = this.sqlSessionFactory.openSession();
		//从SQLSession中获取Mapper接口的代理对象
		UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
		QueryVo queryVo = new QueryVo();
		List<Integer> ids = new ArrayList<>();
		ids.add(1);
		ids.add(27);
		ids.add(29);
		queryVo.setIds(ids);
		//执行查询方法
		List<User> list = userMapper.queryUserByIds(queryVo);
		for(User user : list){
			System.err.println(user.toString());
		}
		sqlSession.close();
	}

	@Test
	public void testQueryUserByName() {
		SqlSession openSession = this.sqlSessionFactory.openSession();
		UserMapper userMapper = openSession.getMapper(UserMapper.class);
		List<User> list = userMapper.queryUserByName("j");
		for(User user : list){
			System.err.println(user.toString());
		}
		openSession.close();
	}

	@Test
	public void testAddUser() {
		SqlSession openSession = this.sqlSessionFactory.openSession();
		UserMapper userMapper = openSession.getMapper(UserMapper.class);
		User user = new User();
		user.setUsername("Berzil.Rio");
		user.setBirthday(new Date());
		user.setSex("1");
		user.setAddress("巴西里约热内卢燃情海滩");
		userMapper.addUser(user );
		openSession.commit();
		System.err.println(user.toString());
		openSession.close();
	}

}
