package cn.itcast.mapper;

import java.util.List;

import cn.itcast.domain.Order;
import cn.itcast.domain.OrderUser;
import cn.itcast.domain.QueryVo;
import cn.itcast.domain.User;

public interface UserMapper {
	
	
	
	/**
	 * 方式二   resultMap方式   自定义返回类型
	 * @return
	 */
	List<Order> queryorderUserResultMap();
	
	/**
	 * 方式一   resultType方式
	 * 查询order同时查询user
	 * @return
	 */
	List<OrderUser> queryOrderUser();
	
	/**
	 * sql拼接练习
	 * @return
	 */
	List<User> queryUserByWhere(User user);
	
	/**
	 * 查询User所有记录
	 * @return
	 */
	Integer queryUserCount();
	
	/**
	 * 根据包装类查询用户
	 * @param username
	 * @return
	 */
	List<User> queryUserByQueryVo(QueryVo queryVo);
	
	/**
	 * 根据id查询用户信息
	 * @param id
	 * @return
	 */
	User queryUserById(int id);
	
	/**
	 * 
	 * @param queryVo
	 * @return
	 */
	List<User> queryUserByIds(QueryVo queryVo);
	
	/**
	 * 根据用户名模糊查询用户
	 * @param username
	 * @return
	 */
	List<User> queryUserByName(String username);
	
	/**
	 * 保存用户
	 * @param user
	 */
	void addUser(User user);

}
