package cn.itcast.mapper;

import java.util.List;

import cn.itcast.domain.Order;
import cn.itcast.domain.User;

public interface OrderMapper {
	
	List<Order> queryOrders();
	
	/**
	 * 在user中引入order   因为是一对多关系   所以是List<Order>
	 * @return
	 */
	List<User> queryUserOrders();

}
