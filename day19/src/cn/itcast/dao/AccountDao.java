package cn.itcast.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbutils.QueryRunner;

import cn.itcast.utils.C3P0Utils;

public class AccountDao {

	public void outPay(Connection conn, String from, double money) {
		// TODO Auto-generated method stub
		QueryRunner qr = new QueryRunner();
		String sql = "update account set money=money-? where name=?";
		try {
			qr.update(conn, sql, money,from);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("操作数据库异常" + e);
		}
	}

	public void comePay(Connection conn, String to, double money) {
		// TODO Auto-generated method stub
		QueryRunner qr = new QueryRunner();
		String sql = "update account set money=money+? where name=?";
		try {
			qr.update(conn, sql, money,to);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("操作数据库异常" + e);
		}
	}

}
