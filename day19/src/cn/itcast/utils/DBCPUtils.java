package cn.itcast.utils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSourceFactory;

public class DBCPUtils {
	private static DataSource dataSource;
	private static ThreadLocal<Connection> local = new ThreadLocal<>();

	static {
		// 加载配置文件，得到流对象
		InputStream in = DBCPUtils.class.getClassLoader().getResourceAsStream("dbcp.properties");
		// 用Properties处理流对象
		Properties prop = new Properties();
		try {
			prop.load(in);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			dataSource = BasicDataSourceFactory.createDataSource(prop);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 获取数据源（连接池）
	 * 
	 * @return
	 */
	public static DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * 获取链接
	 * 
	 * @return
	 */
	public static Connection getConnection() {
		try {
			Connection conn = local.get();
			if (conn == null) {
				conn = dataSource.getConnection();
				local.set(conn);
			}
			return conn;
		} catch (SQLException e) {
			throw new RuntimeException("获取链接异常" + e);
		}
	}
}
