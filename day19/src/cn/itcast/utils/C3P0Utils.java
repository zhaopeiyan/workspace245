package cn.itcast.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

/**
 * C3P0工具类
 * 
 * @author Never Say Never
 * @date 2016年11月26日
 * @version V1.0
 */
public class C3P0Utils {
	// 1.使用无参的方法，它会默认加载c3p0-config.xml里面的<default-config>配置项
	// 如果指定了参数，那么他会加载c3p0-config.xml里面的<named-config>配置项
	private static DataSource dataSource;
	//获取数据源对象
	static{
		dataSource = new ComboPooledDataSource();
	}
	//创建本地线程对象
	private static ThreadLocal<Connection> local = new ThreadLocal<>();
	/**
	 * 使用当前线程获得连接的方法
	 */
	public static Connection getConnnectionForThreadLocal() {
		try {
			//1\从当前线程中，获得已经绑定的链接
			Connection conn = local.get();
			if(conn == null){
				//2、第一次获得，绑定内容      从连接池获得
				conn = dataSource.getConnection();
				//3\将连接存在ThreadLocal
				local.set(conn);
			}
			return conn;
		} catch (SQLException e) {
			throw new RuntimeException("获取链接失败"+e);
		}
	}
	
	/**
	 * 提供数据源方法
	 */
	public static DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * 普通获取链接方法
	 * @return
	 */
	public static Connection getConnection(){
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			return conn;
		} catch (SQLException e) {
			throw new RuntimeException("获取连接异常"+e);
		}
	}
	

	/**
	 * 释放资源方法
	 */

	public static void release(Connection conn, PreparedStatement pstmt, ResultSet rs) {
		if (rs != null)
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		if (pstmt != null)
			try {
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		if (conn != null)
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}
}
