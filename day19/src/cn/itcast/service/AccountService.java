package cn.itcast.service;

import java.sql.Connection;
import java.sql.SQLException;

import cn.itcast.dao.AccountDao;
import cn.itcast.utils.C3P0Utils;
import cn.itcast.utils.DBCPUtils;

public class AccountService {

	private static Connection conn = null;
	AccountDao ad = new AccountDao();

	/**
	 * 
	 * @param from
	 *            付款人
	 * @param to
	 *            收款人
	 * @param money
	 *            总额
	 */
	public void transfer(String from, String to, double money) {
		// TODO Auto-generated method stub
		try {
			// 获取连接
			conn = DBCPUtils.getConnection();
			//conn = C3P0Utils.getConnnectionForThreadLocal();
//			conn = C3P0Utils.getConnection();
			// 开启事务,取消自动提交
			conn.setAutoCommit(false);
			ad.outPay(conn, from, money);

			/*//断电
			int i = 1/0;*/
			
			
			ad.comePay(conn, to, money);
			// 手动提交事务
			conn.commit();
		} catch (Exception e) {
			try {
				if (conn != null) {
					//执行回滚操作
					conn.rollback();
				}
			} catch (Exception e2) {
				throw new RuntimeException("事务回滚异常"+e);
			}
			throw new RuntimeException("" + e);
		}finally {
			try {
				if(conn != null){
					conn.close();
				}
			} catch (SQLException e) {
				throw new RuntimeException("链接关闭异常"+e);
			}
		}
	}
}
