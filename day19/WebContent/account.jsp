<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>转账案例</title>
</head>
<body>
	<form action="${pageContext.request.contextPath}/AccountServlet" method="post">
		<table border="1" width="50%" style="color:red">
			<tr>
				<td>收款人</td>
				<td><input type="text" name="to"/></td>
			</tr>
			<tr>
				<td>付款人</td>
				<td><input type="text" name="from"/></td>
			</tr>
			<tr>
				<td>付款金额</td>
				<td><input type="text" name="money"/></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="转账"/></td>
			</tr>
		</table>
	</form>
</body>
</html>