package cn.itcast.test;

import java.io.IOException;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;
import org.junit.Test;

/**
 * <p>Title:solrj</p>
 * <p>Description:solr第一天代码</p>
 * <p>Company:</p>
 * @author PeiyanZhao
 * @date2017年5月17日 下午7:46:33
 */
public class solrj {

	/**
	 * 添加索引 
	 * @throws Exception
	 */
	@Test
	public void testCreateIndexBySolrInputDocument() throws Exception{
		//链接solr服务
		String baseURL = "http://localhost:8080/solr/itcast";
		HttpSolrServer httpSolrServer = new HttpSolrServer(baseURL);
		
		//创建SolrInputDocument
		SolrInputDocument doc = new SolrInputDocument();
		doc.addField("id", "3");
		doc.addField("title", "NOKIA");
		doc.addField("content", "NOKIA挡子弹,NOKIA破产,MicroSoft收购NOKIA手机业务");
		
		//添加索引
		httpSolrServer.add(doc);
		httpSolrServer.commit();
	}
	
	/**
	 * 根据id更新索引库,如果有则更新,如果没有则添加
	 * @throws SolrServerException
	 * @throws IOException
	 */
	@Test
	public void updateIndexBySolrId() throws SolrServerException, IOException{
		//链接solr服务
		String baseURL = "http://localhost:8080/solr/itcast";
		HttpSolrServer httpSolrServer = new HttpSolrServer(baseURL);
		
		//创建SolrInputDocument
		SolrInputDocument doc = new SolrInputDocument();
		doc.addField("id", "3");
		doc.addField("title", "Solr");
		doc.addField("content", "Solr主要用于全站检索,底层是Luence,通常使用Ik分词器将数据库进行索引分区,并建立索引库");
		
		//添加索引
		httpSolrServer.add(doc);
		httpSolrServer.commit();
	}
	
	/**
	 * 根据id删除索引
	 * @throws IOException 
	 * @throws SolrServerException 
	 */
	@Test
	public void testDeleteById() throws SolrServerException, IOException{
		//链接solr服务
		String baseURL = "http://localhost:8080/solr/itcast";
		HttpSolrServer httpSolrServer = new HttpSolrServer(baseURL);
		
		httpSolrServer.deleteById("4");
		httpSolrServer.commit();
	}
	
	/**
	 * 先查再删,查到就删,查不到就不删
	 * @throws SolrServerException
	 * @throws IOException
	 */
	@Test
	public void testDeleteByQuery() throws SolrServerException, IOException{
		//链接solr服务
		String baseURL = "http://localhost:8080/solr/itcast";
		HttpSolrServer httpSolrServer = new HttpSolrServer(baseURL);
		
		//先查再删
		httpSolrServer.deleteByQuery("title:HUAWEI");
		httpSolrServer.commit();
	}
	
}
