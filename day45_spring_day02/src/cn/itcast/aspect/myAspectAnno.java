package cn.itcast.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class myAspectAnno {
	
	@Before("myAspectAnno.save()")
	public void before(){
		System.out.println("前置通知...");
	}
	@Pointcut("execute(* cn.itcast.dao.impl.ProductDaoImpl.save(..))")
	private void save(){}
	
	@AfterReturning("myAspectAnno.update()")
	public void afterReturning(){
		System.out.println("后置通知...");
	}
	@Pointcut("execute(* cn.itcast.dao.impl.ProductDaoImpl.update(..))")
	private void update(){}
	
	@AfterThrowing("myAspectAnno.delete()")
	public void afterThrowing(){
		System.out.println("异常抛出...");
	}
	@Pointcut("execute(* cn.itcast.dao.impl.ProductDaoImpl.delete(..))")
	private void delete(){}
	
	@After("myAspectAnno.find()")
	public void after(){
		System.out.println("最终通知...");
	}
	@Pointcut("execute(* cn.itcast.dao.impl.ProductDaoImpl.find(..))")
	private void find(){}
	
	
	
}
