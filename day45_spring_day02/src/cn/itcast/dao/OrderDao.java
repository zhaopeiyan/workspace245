package cn.itcast.dao;

public interface OrderDao {
	public void save();
	public void update();
	public void delete();
	public void find();
	
}
