package cn.itcast.dao.impl;

import org.springframework.stereotype.Component;

import cn.itcast.dao.UserDao;

@Component(value="userDao")
public class UserDaoImpl implements UserDao {

	@Override
	public void sayHello() {
		System.out.println("Hello Spring...");
	}

}
