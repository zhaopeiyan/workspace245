package cn.itcast.service.impl;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import cn.itcast.dao.UserDao;
import cn.itcast.service.UserService;


@Component(value="userService")
@Service(value="userService")
@Scope(value="prototype")
public class UserServiceImpl implements UserService {

	@Resource(name="userDao")
	private UserDao userDao;
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public void sayHello() {
		// TODO Auto-generated method stub
		userDao.sayHello();
	}

	
}
