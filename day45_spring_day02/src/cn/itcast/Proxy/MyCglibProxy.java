package cn.itcast.Proxy;

import java.lang.reflect.Method;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import cn.itcast.dao.UserDao;

public class MyCglibProxy implements MethodInterceptor {

	private UserDao userDao;
	public MyCglibProxy(UserDao userDao) {
		this.userDao = userDao;
	}
	
	//代理方法
	public UserDao createProxy(){
		//创建Cglib的核心类
		Enhancer enhancer = new Enhancer();
		//设置父类 
		enhancer.setSuperclass(UserDao.class);
		//设置回调
		enhancer.setCallback(this);
		//生成代理
		UserDao userDaoProxy = (UserDao) enhancer.create();
		return userDaoProxy;
	}
	
	@Override
	public Object intercept(Object proxy, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
		if("sayHello".equals(method.getName())){
			Object obj = methodProxy.invokeSuper(proxy, args);
			System.out.println("Cglib方式增强方法...");
			return obj;
		}
		return methodProxy.invokeSuper(proxy, args);
	}

}
