package cn.itcast.test;

import javax.annotation.Resource;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.itcast.dao.OrderDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class orderTest {
	@Autowired
	private  OrderDao orderDao;
	
	
	@Test
	public void test1(){
		System.out.println("==========save================");
		orderDao.save();
		
		System.out.println("========update==================");
		orderDao.update();
		
		
		System.out.println("============find==============");
		orderDao.find();
		
		System.out.println("============delete==============");
		orderDao.delete();
	}
	@Test
	public void test2(){
		orderDao.update();
		
	}
	
}














