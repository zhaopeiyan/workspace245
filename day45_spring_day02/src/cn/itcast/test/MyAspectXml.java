package cn.itcast.test;

import org.aopalliance.intercept.Joinpoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.stereotype.Service;

public class MyAspectXml {
	//前置增强
	public void before(){
		System.out.println("前置增强...");
	}
	
	public void after(){
		System.out.println("...后置增强");
	}
	
	public void around(ProceedingJoinPoint proJoinPoint) throws Throwable{
		//切点执行之前执行
		System.out.println("环绕前置增强....");
		//切点执行
		proJoinPoint.proceed();
		//切点执行之后执行
		System.out.println("环绕后置增强....");
		
	}
}
