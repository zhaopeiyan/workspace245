package cn.itcast.test;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.itcast.dao.UserDao;
import cn.itcast.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class SpringDemo3{
	/*@Resource(name="userDao")
	private UserDao userDao;
	
	@Test
	public void test2(){
		userDao.sayHello();
	}*/
	
	
	@Resource(name="userService")
	private UserService userService;
	@Test
	public void test3(){
		userService.sayHello();
	}
}