package cn.itcast.domain;

import java.io.Serializable;

public class User implements Serializable{

	private String uid;
	private String username;
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
