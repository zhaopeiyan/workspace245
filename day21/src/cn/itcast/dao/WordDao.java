package cn.itcast.dao;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.junit.Test;

import cn.itcast.domain.Word;
import cn.itcast.utils.C3p0Utils;

public class WordDao {

	public List<Word> findWord(String word) {
		List<Word> words = null;
		try {
			QueryRunner qr = new QueryRunner(C3p0Utils.getDataSource());
			String sql = "select * from word where word like ? or pinyin like ?";
//			Object[] params = {"%"+word+"%","%"+word+"%"};
			Object[] params = {word,word};
			words = qr.query(sql, new BeanListHandler<>(Word.class), params);
		} catch (SQLException e) {
			throw new RuntimeException("查询数据异常"+e);
		}
		return words;
	}
}
