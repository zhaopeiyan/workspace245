package cn.itcast.dao;

import java.sql.SQLException;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import cn.itcast.domain.User;
import cn.itcast.utils.C3p0Utils;

public class UserDao {

	/**
	 * 按用户名查找User表记录
	 * @param username
	 * @return
	 */
	public User searchUserByName(String username) {
		QueryRunner qr = new QueryRunner(C3p0Utils.getDataSource());
		User user = null;
		String sql = "select * from user where username=?";
		try {
			user = qr.query(sql, new BeanHandler<>(User.class), username);
		} catch (SQLException e) {
			throw new RuntimeException("查询数据异常"+e);
		}
		return user;
	}
}
