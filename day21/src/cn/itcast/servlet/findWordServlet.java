package cn.itcast.servlet;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.domain.Word;
import cn.itcast.service.WordService;
import net.sf.json.JSONArray;

public class findWordServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");
		
		//1、获取页面输入的数据
		String word = request.getParameter("word");
		//2、调用service层处理业务逻辑
		WordService ws = new WordService();
		List<Word> words = ws.findWord(word);
		//3、将取出的数据转换成json数组
		JSONArray json = JSONArray.fromObject(words);
		response.getWriter().write(json.toString());
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
