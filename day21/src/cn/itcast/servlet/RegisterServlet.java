package cn.itcast.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.domain.User;
import cn.itcast.service.UserService;

public class RegisterServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");
		//处理异步请求乱码问题
		response.setCharacterEncoding("utf-8");
		
		//1、获取用户输入的用户名
		String username = request.getParameter("username");
		//2、调用service层处理业务逻辑
		UserService us = new UserService();
		User user = us.searchUserByName(username);
//		System.out.println(user.getUsername());
		//3、判断数据库是否有同名记录
		String data = null;
		if(user == null){
			response.getWriter().write("true");
			//response.getWriter().write("<span class='label label-success'>用户名可用</span>");
		}else{
			response.getWriter().write("false");
//			response.getWriter().write("<span class='label label-danger'>用户名不可用</span>");
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}