package cn.itcast.service;

import cn.itcast.dao.UserDao;
import cn.itcast.domain.User;

public class UserService {

	/**
	 * 按用户名查找User表记录
	 * @param username
	 * @return
	 */
	public User searchUserByName(String username) {
		return new UserDao().searchUserByName(username);
	}

}
