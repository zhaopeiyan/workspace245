package cn.itcast.utils;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbutils.DbUtils;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.impl.NewProxyConnection;

public class C3p0Utils {

	//连接池就定义 并且加载配置完毕
	//命名配置
//	private static DataSource dataSource=new ComboPooledDataSource("itcast");
	
	
	//默认配置
	private static DataSource dataSource=new ComboPooledDataSource();
	
	//
	private static ThreadLocal<Connection> local = new ThreadLocal<Connection>();
	
	//c3p0+DBUtils
	//DBUtils使用时，需要获取dataSource对象
	public static DataSource getDataSource(){
		return dataSource;
	}
	
	
	/**
	 * 
	 * 研发版
	 * 获取链接方法---结合了ThreadLocal，加入了事务自动提交的控制
	 */
	public static Connection getConnectionForThreadLocalForFinal(){
		Connection con = null;
		try {
			//首先从当前的线程中获取Connection对象
			con=local.get();
			if(con==null){
				//当前线程没有绑定Connection对象，从连接池中获取一个新的
				con = dataSource.getConnection();
				local.set(con);
			}
			//这句判断只是为了防止con被关闭及被重复设置自动提交关闭，可以去掉
			if(!con.isClosed()&&con.getAutoCommit())
				con.setAutoCommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		
		return con;
	}
	
	/**
	 * 提交方法
	 */
	public static void commit(){
		Connection con = getConnectionForThreadLocalForFinal();
		if(con!=null)
			//因为这句会导致con对象的关闭，这样con进入池中后，con就被关闭了，而被关闭的con执行任何SQL设置或者语句都会报 java.sql.SQLException: You can't operate on a closed Connection!!! 
//			DbUtils.commitAndCloseQuietly(con);
			try {
				con.commit();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
	}
	
	/**
	 * 回滚方法
	 */
	public static void rollback(){
		Connection con = getConnectionForThreadLocalForFinal();
		if(con!=null)
			//因为这句会导致con对象的关闭，这样con进入池中后，con就被关闭了，而被关闭的con执行任何SQL设置或者语句都会报 java.sql.SQLException: You can't operate on a closed Connection!!! 
//			DbUtils.rollbackAndCloseQuietly(con);
			try {
				con.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
	}
	
	
	
	/**
	 * 
	 * 开发板
	 * 获取链接方法---结合了ThreadLocal
	 */
	public static Connection getConnectionForThreadLocal(){
		Connection con = null;
		try {
			//首先从当前的线程中获取Connection对象
			con=local.get();
			if(con==null){
				//当前线程没有绑定Connection对象，从连接池中获取一个新的
				con = dataSource.getConnection();
				local.set(con);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return con;
	}
	
	/**
	 * 获取链接方法
	 */
	public static Connection getConnection(){
		Connection con = null;
		try {
			con = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return con;
	}
	
	
	public static void main(String[] args) throws SQLException {
		for(int i =0;i<40;i++){
			Connection con=C3p0Utils.getConnection();
			System.out.println("获取到的"+con);
			//不会关闭con对象，C3P0已经帮我去增强了Connection close方法
			con.close();
			
		}
	}
}
