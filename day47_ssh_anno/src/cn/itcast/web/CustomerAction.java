package cn.itcast.web;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.domain.Customer;
import cn.itcast.service.CustomerService;

@Controller
@Scope(value="prototype")
@Namespace("/")
@ParentPackage("struts-default")
@Results(value={@Result(name="success",location="/success.jsp")})
public class CustomerAction extends ActionSupport implements ModelDriven<Customer> {

	@Autowired
	private CustomerService customerService;
	
	@Action(value="customer_save")
	public String save(){
		customerService.save(customer);
		return SUCCESS;
	}
	
	private Customer customer = new Customer();
	@Override
	public Customer getModel() {
		// TODO Auto-generated method stub
		return customer;
	}

}
