package cn.itcast.dao;

import cn.itcast.domain.Customer;

public interface CustomerDao {

	void save(Customer customer);

}
