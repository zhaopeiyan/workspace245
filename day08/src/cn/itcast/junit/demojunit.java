package cn.itcast.junit;

import org.junit.Test;

public class demojunit {
	
	
	/*
	 * junit:
	 * 		作用：用来修饰方法
	 * 要求：
	 * 		1、修饰的方法不能使静态的
	 * 		2、方法不能有参数
	 */
	@Test
	public void eat(){
		System.out.println("132");
	}

}
