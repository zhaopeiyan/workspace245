package cn.itcast.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DemoJDBC {
	public static void main(String[] args) throws Exception {
		
		try {
			//注册驱动
			Class.forName("com.mysql.jdbc.Driver");
			//获取连接
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/ee_245_day08","root","root");
			//获取语句执行者
			String sql = "select * from product";
			PreparedStatement ps = conn.prepareStatement(sql);
			//执行语句
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				int id = rs.getInt(1);
				String name = rs.getString(2);
				int phone = rs.getInt(3);
				String grade = rs.getString(4);
				System.out.println(id+" - "+name+" - "+phone+" - "+grade);
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
