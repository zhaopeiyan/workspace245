package cn.itcast.web.listener;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;

/**
 * Application Lifecycle Listener implementation class MyHttpServletRequest
 *
 */
public class MyHttpServletRequest implements ServletRequestListener {

    /**
     * Default constructor. 
     */
    public MyHttpServletRequest() {
        // TODO Auto-generated constructor stub
    	System.out.println("MyHttpServletRequest在空参构造里");
    }

	/**
     * @see ServletRequestListener#requestDestroyed(ServletRequestEvent)
     */
    public void requestDestroyed(ServletRequestEvent sre)  { 
         // TODO Auto-generated method stub
    	System.out.println("MyHttpServletRequest在Destory里");
    }
	/**
     * @see ServletRequestListener#requestInitialized(ServletRequestEvent)
     */
    public void requestInitialized(ServletRequestEvent sre)  { 
         // TODO Auto-generated method stub
    	System.out.println("MyHttpServletRequest在Initialized里");
    }
	
}
