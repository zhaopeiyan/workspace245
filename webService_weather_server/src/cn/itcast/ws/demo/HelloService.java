package cn.itcast.ws.demo;

import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@WebService
public class HelloService {
	
	public String sayHi(String name){
		System.out.println("sayHi方法被调用了...");
		return "hi"+name;
	}
	
	/*public static void main(String args[]){
		//参数1:指定服务的发布地址
		//参数2:服务器提供服务的对象实例
		Endpoint.publish("http://192.168.71.38:8080/day67_webService_demo1", new HelloService());
		System.out.println("webservice发布好了!");
	}
*/
}
