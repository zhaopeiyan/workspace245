package cn.itcast.ws.demo;

import javax.xml.ws.Endpoint;

import cn.itcast.ws.weather.impl.WeatherInterfaceImpl;

public class WeatherServer {
	
	public static void main(String[] args) {
		//Endpoint类的publish发布服务
		//参数1:address - 服务地址:http:ip:port/server
		//参数2:implmentor - 实现类的实例,需要new
		Endpoint.publish("http://localhost:12345/weather", new WeatherInterfaceImpl());
	}

}
