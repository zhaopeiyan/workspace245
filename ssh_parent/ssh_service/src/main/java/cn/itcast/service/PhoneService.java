package cn.itcast.service;

import java.util.List;

import cn.itcast.domain.Phone;

public interface PhoneService {

	public List<Phone> findAll();
}
