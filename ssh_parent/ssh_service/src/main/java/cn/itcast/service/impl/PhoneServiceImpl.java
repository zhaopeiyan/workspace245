package cn.itcast.service.impl;

import java.util.List;

import cn.itcast.dao.PhoneDao;
import cn.itcast.domain.Phone;
import cn.itcast.service.PhoneService;

public class PhoneServiceImpl implements PhoneService {

	private PhoneDao phoneDao;
	public void setPhoneDao(PhoneDao phoneDao) {
		this.phoneDao = phoneDao;
	}

	
	@Override
	public List<Phone> findAll() {
		// TODO Auto-generated method stub
		return phoneDao.findAll();
	}

}
