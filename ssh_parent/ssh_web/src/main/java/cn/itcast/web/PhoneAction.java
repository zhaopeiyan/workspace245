package cn.itcast.web;

import java.io.IOException;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.domain.Phone;
import cn.itcast.service.PhoneService;

public class PhoneAction extends ActionSupport implements ModelDriven<Phone> {

	private PhoneService phoneService;
	public void setPhoneService(PhoneService phoneService) {
		this.phoneService = phoneService;
	}
	
	
	public String findAll() throws IOException{
		List<Phone> list = phoneService.findAll();
		ServletActionContext.getContext().getValueStack().set("list", list);
		
		return "findAll";
	}
	
	private Phone phone = new Phone();
	@Override
	public Phone getModel() {
		// TODO Auto-generated method stub
		return null;
	}

}
