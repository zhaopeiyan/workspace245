package cn.itcast.dao.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;

import cn.itcast.dao.PhoneDao;
import cn.itcast.domain.Phone;

public class PhoneDaoImpl extends HibernateDaoSupport implements PhoneDao {

	@Override
	public List<Phone> findAll() {
		// TODO Auto-generated method stub
		System.out.println("===========================");
		return (List<Phone>) this.getHibernateTemplate().find("from Phone");
	}


}
