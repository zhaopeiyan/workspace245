package cn.itcast.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import cn.itcast.dao.ProductDao;
import cn.itcast.domain.Category;
import cn.itcast.domain.Product;
import cn.itcast.domain.pageBean;
import cn.itcast.utils.C3p0Utils;

public class ProductService {

	/**
	 * 查询所有商品信息
	 * @return
	 */
	public List<Product> findAllProduct() {
		// TODO Auto-generated method stub
		return new ProductDao().findAllProduct();
	}

	/**
	 * 查询所有分类信息
	 * @return
	 */
	public List<Category> findAllCategory() {
		// TODO Auto-generated method stub
		return new ProductDao().findAllCategory();
	}

	/**
	 * 添加商品信息
	 * @param product
	 */
	public void addProduct(Product product) {
		// TODO Auto-generated method stub
		new ProductDao().addProduct(product);
	}

	/**
	 * 按pid删除商品信息
	 * @param pid
	 */
	public void deleteProductById(String pid) {
		// TODO Auto-generated method stub
		new ProductDao().deleteProductById(pid);
	}

	/**
	 * 按pid查找商品信息
	 * @param pid
	 * @return
	 */
	public Product findProductById(String pid) {
		// TODO Auto-generated method stub
		return new ProductDao().findProductById(pid);
	}

	/**
	 * 修改商品信息
	 * @param product
	 */
	public void modifyProduct(Product product) {
		// TODO Auto-generated method stub
		new ProductDao().modifyProduct(product);
	}

	/**
	 * 删除选中的商品信息
	 * @param pids
	 */
	public void deleteProductById(String[] pids) {
		// TODO Auto-generated method stub
		ProductDao pd = new ProductDao();
		Connection conn = C3p0Utils.getConnection();
		try {
			conn.setAutoCommit(false);
			for (String pid : pids) {
				pd.deleteProductsById(conn,pid);
			}
			conn.commit();
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				throw new RuntimeException("回滚异常"+e1);
			}
			throw new RuntimeException("设置自动提交异常"+e);
		}
	}

	/**
	 * 条件查询商品信息
	 * @param cid
	 * @param pname
	 * @return
	 */
	public List<Product> searchProductByCondition(String cid, String pname) {
		// TODO Auto-generated method stub
		return new ProductDao().searchProductByCondition(cid,pname);
	}

	/**
	 * 分页查询商品信息
	 * @param pb
	 * @return
	 */
	public pageBean searchProductByLimitCount(pageBean pb) {
		ProductDao pd = new ProductDao();
		//一个页面显示的记录数
		pb.setPageSize(4);
		//开始的索引值
		pb.setStartIndex((pb.getPageNumber() - 1) * pb.getPageSize());
		//全部的记录数
		pb.setTotalRecord(pd.findAllRecord());
		System.out.println(pb.getTotalRecord());
		//全部的页数
		pb.setTotalPage(((pb.getTotalRecord() % pb.getPageSize() == 0)) ? (pb.getTotalRecord()/pb.getPageSize()) : ((pb.getTotalRecord()/pb.getPageSize()) + 1));
		System.out.println(pb.getTotalPage());
		return pd.searchProductByLimitCount(pb);
	}

	/**
	 * 模糊查询所有与用户输入字符匹配的商品
	 * @param pname
	 * @return
	 */
	public List<Product> findAllProductName(String pname) {
		return new ProductDao().findAllProductName(pname.replace("", "%"));
	}

	/**
	 * 根据条件查询对应的商品信息，并进行分页查询
	 * @param cid
	 * @param pname
	 * @param pb
	 * @return
	 */
	public pageBean searchProductByConditionOfLimit(String cid, String pname,pageBean pb) {
		ProductDao productDao = new ProductDao();
		//一个页面显示的记录数
		pb.setPageSize(4);
		//开始的索引值
		pb.setStartIndex((pb.getPageNumber() - 1) * pb.getPageSize());
		//全部的记录数
		pb.setTotalRecord(productDao.findAllRecordByCondition(cid,pname));
		System.out.println(pb.getTotalRecord());
		//全部的页数
		pb.setTotalPage(((pb.getTotalRecord() % pb.getPageSize() == 0)) ? (pb.getTotalRecord()/pb.getPageSize()) : ((pb.getTotalRecord()/pb.getPageSize()) + 1));
		System.out.println(pb.getTotalPage());
		return productDao.searchProductByConditionOfLimit(cid, pname,pb);
	}
}
