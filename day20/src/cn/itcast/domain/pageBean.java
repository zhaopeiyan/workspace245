package cn.itcast.domain;

import java.io.Serializable;
import java.util.List;

public class pageBean<T> implements Serializable{
	private int startIndex;
	private int totalRecord;
	private int totalPage;
	private int pageNumber;
	private int pageSize;
	private List<T> result;
	
	
	public pageBean(int startIndex, int totalRecord, int totalPage, int pageNumber, int pageSize, List<T> result) {
		super();
		this.startIndex = startIndex;
		this.totalRecord = totalRecord;
		this.totalPage = totalPage;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.result = result;
	}
	public List<T> getResult() {
		return result;
	}
	public void setResult(List<T> result) {
		this.result = result;
	}
	public pageBean() {
		super();
	}
	public int getStartIndex() {
		return startIndex;
	}
	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}
	public int getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(int totalRecord) {
		this.totalRecord = totalRecord;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
}
