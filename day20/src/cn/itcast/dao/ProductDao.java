package cn.itcast.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ColumnListHandler;
import org.apache.commons.dbutils.handlers.KeyedHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import cn.itcast.domain.Category;
import cn.itcast.domain.Product;
import cn.itcast.domain.pageBean;
import cn.itcast.utils.C3p0Utils;

public class ProductDao {

	/**
	 * 查询所有商品信息
	 * @return
	 */
	public List<Product> findAllProduct() {
		List<Product> products = null;
		//1、获取QueryRunner对象
		QueryRunner qr = new QueryRunner(C3p0Utils.getDataSource());
		//2、创建sql语句
		String sql = "select * from product";
		try {
			products = qr.query(sql, new BeanListHandler<Product>(Product.class));
		} catch (SQLException e) {
			throw new RuntimeException("查询数据库异常"+e);
		}
		return products;
	}

	/**
	 * 查询所有分类信息
	 * @return
	 */
	public List<Category> findAllCategory() {
		List<Category> categorys =null;
		QueryRunner qr = new QueryRunner(C3p0Utils.getDataSource());
		String sql = "select * from category";
		try {
			categorys = qr.query(sql, new BeanListHandler<Category>(Category.class));
		} catch (SQLException e) {
			throw new RuntimeException("查询分类信息异常"+e);
		}
		return categorys;
	}

	/**
	 * 添加商品信息
	 * @param product 
	 */
	public void addProduct(Product product) {
		// TODO Auto-generated method stub
		QueryRunner qr = new QueryRunner(C3p0Utils.getDataSource());
		String sql = "insert into product values(?,?,?,?,?,?,?,?,?,?)";
		Object[] params = {product.getPid(),product.getPname(),product.getMarket_price(),product.getShop_price(),
				product.getPimage(),product.getPdate(),product.getIs_hot(),product.getPdesc(),product.getPflag(),
				product.getCategory().getCid()};
		try {
			int update = qr.update(sql, params);
		} catch (Exception e) {
			throw new RuntimeException("添加数据异常"+e);
		}
	}

	/**
	 * 按pid删除商品信息
	 * @param pid
	 */
	public void deleteProductById(String pid) {
		QueryRunner qr = new QueryRunner(C3p0Utils.getDataSource());
		String sql = "delete from product where pid=?";
		try {
			qr.update(sql,pid);
		} catch (SQLException e) {
			throw new RuntimeException("删除产品信息异常"+e);
		}
	}

	public Product findProductById(String pid) {
		Product product = null;
		QueryRunner qr = new QueryRunner(C3p0Utils.getDataSource());
		String sql = "select * from product where pid=?";
		try {
			product = qr.query(sql, new BeanHandler<Product>(Product.class), pid);
		} catch (SQLException e) {
			throw new RuntimeException("查询产品信息异常"+e);
		}
		return product;
	}

	/**
	 * 修改product信息
	 * @param product
	 */
	public void modifyProduct(Product product) {
		QueryRunner qr = new QueryRunner(C3p0Utils.getDataSource());
		String sql = "update product set pname=?,shop_price=?,pimage=?,is_hot=?,pdesc=?,cid=? where pid=?";
		Object[] params = {product.getPname(),product.getShop_price(),product.getPimage(),product.getIs_hot(),product.getPdesc(),product.getCid(),product.getPid()};
		try {
			qr.update(sql, params);
		} catch (SQLException e) {
			throw new RuntimeException("修改数据异常"+e);
		}
	}

	/**
	 * 删除选中商品信息，如果有一条删除失败，则回滚操作不提交修改
	 * @param conn
	 * @param pid
	 */
	public void deleteProductsById(Connection conn, String pid) {
		try {
			QueryRunner qr = new QueryRunner(C3p0Utils.getDataSource());
			String sql = "delete from product where pid=?";
			qr.update(conn, sql, pid);
		} catch (SQLException e) {
			throw new RuntimeException("批量删除异常"+e);
		}
	}

	/**
	 * 条件查询商品信息
	 * @param cid
	 * @param pname
	 * @return
	 */
	public List<Product> searchProductByCondition(String cid, String pname) {
		QueryRunner qr = new QueryRunner(C3p0Utils.getDataSource());
		
		List<Product> products = null;
		//条件查询一般写法   创建StringBuilder对象
		StringBuilder sb = new StringBuilder("select * from product where 1=1");
		List<Object> params = new LinkedList<>();
		if(!"".equals(cid) && cid != null){
			sb = sb.append(" and cid=? ");
			params.add(cid);
		}
		if(!"".equals(pname) && pname != null){
			sb = sb.append(" and pname like ?");
			params.add("%"+pname+"%");
		}
		String sql = sb.toString();
		Object[] param = params.toArray();
		try {
			products = qr.query(sql, new BeanListHandler<>(Product.class), param);
		} catch (SQLException e) {
			throw new RuntimeException("条件查询异常"+e);
		}
		return products;
	}

	/**
	 * 分页查询
	 * @param pb
	 * @return
	 */
	public pageBean searchProductByLimitCount(pageBean pb) {
		QueryRunner qr = new QueryRunner(C3p0Utils.getDataSource());
		List<Product> products = null;
		String sql = "select * from product limit ?,?";
		try {
			products = qr.query(sql, new BeanListHandler<>(Product.class), pb.getStartIndex(),pb.getPageSize());
			pb.setResult(products);
		} catch (SQLException e) {
			throw new RuntimeException("分页查询异常"+e);
		}
		return pb;
	}

	public int findAllRecord() {
		QueryRunner qr = new QueryRunner(C3p0Utils.getDataSource());
		Long total = null;
		String sql = "select count(*) from product";
		try {
			total = (Long)qr.query(sql, new ScalarHandler());
		} catch (SQLException e) {
			throw new RuntimeException("查询所有记录异常"+e);
		}
		return total.intValue();
	}

	/**
	 * 模糊查询与传入字符相匹配的记录
	 * @param pname
	 * @return
	 */
	public List<Product> findAllProductName(String pname) {
		List<Product> products = null;
		try {
			QueryRunner qr = new QueryRunner(C3p0Utils.getDataSource());
			String sql = "select * from product where pname like ?";
			Object[] params = {pname};
			products = qr.query(sql, new BeanListHandler<>(Product.class), params);
		} catch (SQLException e) {
			throw new RuntimeException("查询数据库异常"+e);
		}
		return products;
	}

	public pageBean searchProductByConditionOfLimit(String cid, String pname,pageBean pb) {
		List<Product> products = null;
		try {
			QueryRunner qr = new QueryRunner(C3p0Utils.getDataSource());
			String sql = "select * from product where cid=? and pname=? limit ?,?";
			Object[] params = {cid,pname,pb.getStartIndex(),pb.getPageSize()};
			products = qr.query(sql, new BeanListHandler<>(Product.class), params);
			pb.setResult(products);
		} catch (SQLException e) {
			throw new RuntimeException("查询数据库异常"+e);
		}
		return pb;
	}

	public int findAllRecordByCondition(String cid, String pname) {
		QueryRunner qr = new QueryRunner(C3p0Utils.getDataSource());
		Long total = null;
		String sql = "select count(*) from product where cid=? or pname=?";
		try {
			total = (Long)qr.query(sql, new ScalarHandler(),cid,pname);
		} catch (SQLException e) {
			throw new RuntimeException("查询所有记录异常"+e);
		}
		return total.intValue();
	}
}
