package cn.itcast.web.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import cn.itcast.domain.Product;
import cn.itcast.service.ProductService;

public class modifyProductServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		Product product = new Product();
		//1、获取修改后的数据
		Map<String, String[]> map = request.getParameterMap();
		//2、封装到javabean
		try {
			BeanUtils.populate(product, map);
			//3、需要单独封装的double型数据
			double shop_price = Double.parseDouble(request.getParameter("shop_price"));
			product.setShop_price(shop_price);
			//4、调用service层执行
			ProductService ps = new ProductService();
			ps.modifyProduct(product);
			//5、执行后重定向查询所有商品信息
			request.getRequestDispatcher("FindAllProductForNoPageServlet").forward(request, response);
		} catch (Exception e) {
			throw new RuntimeException("封装到javabean异常"+e);
		}
		
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}