package cn.itcast.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.domain.Category;
import cn.itcast.domain.Product;
import cn.itcast.domain.pageBean;
import cn.itcast.service.ProductService;
import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;

public class searchProductByLimitServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		pageBean pb = new pageBean();
//		1、获取到起始页
		int pageNumber = 1;
		
		pb.setPageNumber(pageNumber);
		System.out.println(pageNumber);
//		2、调用service层
		ProductService ps = new ProductService();
		List<Category> categorys = ps.findAllCategory();
		pageBean pageBean = ps.searchProductByLimitCount(pb);
		//将得到的分类数据进行转换，变成json类型数据
//		String json = JSONArray.fromObject(categorys).toString();
		//所有的异步操作都是借助响应体完成的,不能用域对象操作
//		response.getWriter().write(json);
		//再将所有商品信息通过同步方式传到jsp内，用jstl和EL来展示数据
		request.setAttribute("pageBean", pageBean);
		System.out.println(pageBean);
		request.getRequestDispatcher("plist.jsp").forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}