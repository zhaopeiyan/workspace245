package cn.itcast.web.servlet;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import cn.itcast.domain.Category;
import cn.itcast.domain.Product;
import cn.itcast.service.ProductService;
import cn.itcast.utils.UUIDUtils;

public class AddProductServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");
		
		Product product = new Product();
		//1、获取页面数据
		Map<String, String[]> map = request.getParameterMap();
		//2、封装数据进javabean
		try {
			BeanUtils.populate(product, map);
			//单独封装类对象和double类型的数据,单独封装pid
			String pid = UUIDUtils.getUUID();
			product.setPid(pid);
			
			String cid = request.getParameter("cid");
			Category category = new Category();
			category.setCid(cid);
			product.setCategory(category);
			
			double shop_price = Double.parseDouble(request.getParameter("shop_price"));
			product.setShop_price(shop_price);
			
			product.setPdate("");
			
			//调用service层方法
			ProductService ps = new ProductService();
			ps.addProduct(product);
			
			//执行过后再查询所有记录
			request.getRequestDispatcher("FindAllProductForNoPageServlet").forward(request, response);
		} catch (Exception e) {
			throw new RuntimeException("封装数据异常"+e);
		} 
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}