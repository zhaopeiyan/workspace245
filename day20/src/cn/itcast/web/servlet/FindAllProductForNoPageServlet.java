package cn.itcast.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.domain.Category;
import cn.itcast.domain.Product;
import cn.itcast.service.ProductService;

public class FindAllProductForNoPageServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		//1、调用service层
		ProductService ps = new ProductService();
		try {
			List<Product> products = ps.findAllProduct();
			List<Category> categorys = ps.findAllCategory();
			//2、判断
			if(products != null){
				//3、将查询结果保存到request域对象中
				request.setAttribute("products", products);
				request.setAttribute("categorys", categorys);
				//4、重定向到jsp页面，显示信息
				request.getRequestDispatcher("/plistNoPage.jsp").forward(request, response);
			}
		} catch (Exception e) {
			// TODO: handle exception
			throw new RuntimeException("" + e);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}