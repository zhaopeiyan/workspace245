package cn.itcast.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.domain.Category;
import cn.itcast.domain.pageBean;
import cn.itcast.service.ProductService;

public class searchProductByLimitCountServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		//int pageNumber = 1;
		pageBean pb = new pageBean();
		//1、获取到起始页
		int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
		System.out.println(pageNumber);
		pb.setPageNumber(pageNumber);
		//2、调用service层
		ProductService ps = new ProductService();
		List<Category> categorys = ps.findAllCategory();
		pageBean pageBean = ps.searchProductByLimitCount(pb);
		//3、将查询到的数据存放到域对象
		request.setAttribute("categorys", categorys);
		request.setAttribute("pageBean", pageBean);
		//4、重定向到显示页面
		request.getRequestDispatcher("plist.jsp").forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}