package cn.itcast.web.servlet;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.service.ProductService;

public class deleteSelectedProductServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		//1、获取所有的选中商品的pid
		String[] pids = request.getParameterValues("pid");
//		System.out.println(Arrays.toString(pids));
		//2、调用service层处理业务逻辑
		ProductService ps = new ProductService();
		ps.deleteProductById(pids);
		//3、重定向查询所有商品
		response.getWriter().print("恭喜你，删除成功！");
		request.getRequestDispatcher("FindAllProductForNoPageServlet").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}