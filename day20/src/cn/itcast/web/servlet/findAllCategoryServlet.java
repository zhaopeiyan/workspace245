package cn.itcast.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.domain.Category;
import cn.itcast.service.ProductService;
import net.sf.json.JSONArray;

public class findAllCategoryServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		// 解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		// 先要获取所有分类信息，并在页面上显示
		ProductService ps = new ProductService();
		List<Category> categorys = ps.findAllCategory();
		//将list集合转换为json数据格式
		JSONArray json = JSONArray.fromObject(categorys);
		//通过响应体传递给jsp页面
		response.getWriter().write(json.toString());
		/*// 将分类信息存放到域对象内
		request.setAttribute("categorys", categorys);
		// 重定向到添加页面
		request.getRequestDispatcher("add.jsp").forward(request, response);*/
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}