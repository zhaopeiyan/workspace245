package cn.itcast.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.domain.Product;
import cn.itcast.service.ProductService;
import net.sf.json.JSONArray;

public class findAllProducNameServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		//1、获取搜索文本框的值
		String pname = request.getParameter("pname");
		System.out.println("++++++++++++++++++++++++++");
		System.out.println(pname);
		System.out.println("**************************");
		//2、调用service方法处理业务逻辑
		ProductService ps = new ProductService();
		List<Product> products = ps.findAllProductName(pname);
		
		//3、将得到的list集合转换成json数据结构
		String data = JSONArray.fromObject(products).toString();
		//4、将数据通过响应体的方式传回到jsp页面
		response.getWriter().write(data);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}