package cn.itcast.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.domain.Category;
import cn.itcast.domain.Product;
import cn.itcast.service.ProductService;

public class findProductByIdServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		// 解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		// 1、先查询pid对应的表信息
		String pid = request.getParameter("pid");
//		System.out.println(pid);
		ProductService ps = new ProductService();
		Product product = ps.findProductById(pid);
		List<Category> categorys = ps.findAllCategory();
		//System.out.println(categories);
		// 2、将查询到的产品信息，存到域对象中
		request.setAttribute("product", product);
		
		request.setAttribute("categorys", categorys);
		// 3、重定向到修改页面
		request.getRequestDispatcher("modify.jsp").forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}