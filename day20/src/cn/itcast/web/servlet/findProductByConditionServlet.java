package cn.itcast.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.domain.Category;
import cn.itcast.domain.Product;
import cn.itcast.domain.pageBean;
import cn.itcast.service.ProductService;

public class findProductByConditionServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		//用于数据回显
		Product product =  new Product();
		pageBean pb = new pageBean<>();
		int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
		//1、获取模糊查询的条件数据
		String cid = request.getParameter("cid");
		product.setCid(Integer.parseInt(cid));
		String pname = request.getParameter("pname");
		product.setPname(pname);
		pb.setPageNumber(pageNumber);
		//2、调用service层处理业务逻辑关系
		ProductService ps = new ProductService();
		pageBean pageBean = ps.searchProductByConditionOfLimit(cid,pname,pb);
		List<Category> categorys = ps.findAllCategory();
		//存放回显数据
		request.setAttribute("product", product);
		request.setAttribute("categorys", categorys);
		request.setAttribute("pageBean", pageBean);
		request.getRequestDispatcher("plist.jsp").forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}