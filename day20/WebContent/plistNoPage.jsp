<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/imagetable.css">
<script type="text/javascript" src="js/jquery-1.11.3.js"></script>
<script type="text/javascript">
	$(function (){
		$("#add").click(function (){
			location.href="${pageContext.request.contextPath}/findAllCategoryServlet";
		});
		/*全选jQuery  */
		$("#selectAll").click(function (){
			/* alert(this.checked); */
			$(".checkBox").prop("checked",this.checked);
		});
		
		/*反选  */
		$("#resselectAll").click(function (){
			$(".checkBox").each(function (){
				$(this).prop("checked",!(this.checked));
			})
		});
		/*全部选中则选中全选按钮  */
		$(".checkBox").click(function (){
			var flag = true;
			$(".checkBox").each(function (){
				if(this.checked == false){
					flag = false;
				}
			});
			$("#selectAll").prop("checked",flag);
		});
		
		/*删除选中商品信息*/
		$(".deleteSelected").click(function (){
			var all = $("input[name='pid']:checked");
			//alert(all.length);
			if(all.length == 0){
				alert("请选择要删除的商品");
			}else if(all.length != 0){
				if(confirm("确定要删除所选的"+all.length+"条记录吗？")){
					//获得所有的id串
					var ids = all.serialize();
					//alert(ids);
					location.href="${pageContext.request.contextPath}/deleteSelectedProductServlet?"+ids;
				}else{
					all.removeProp("checked");
				}
			}
		});
		/*模糊查询商品信息  */
		/* $(".search").click(function (){
			location.href="${pageContext.request.contextPath}/findProductByConditionServlet";
		}); */
	});
	
	 /*删除商品*/
	 function deleteById(pid){
		if(confirm("确定删除？")){
			location.href="${pageContext.request.contextPath}/deleteProductServlet?pid="+pid;
		}
	}
	
	/*删除选中商品  */
	/*
	function deleteSelected() {
		alert("before");
		var ids = document.getElementsByName("pid").checked;
		alert("after"+ids);
	} */
</script>
</head>
<body>
	<table border="1" width="40%" class="imagetable" align="center">
		<tr>
			<th>商品列表</th>
		</tr>
	</table>
	<hr />
	<table border="1" width="100%" class="imagetable">
		<tr>
		<th>
			<input type="button" class="deleteSelected" value="批量删除" onclick="deleteSelected()">
		</th>
		<th colspan="5">
		<form action="${pageContext.request.contextPath}/findProductByConditionServlet" method="post">
			商品查询功能：
			<select name="cid">
				<c:if test="${not empty categorys }">
					<c:forEach items="${categorys }" var="category" >
						<option value="${category.cid}" ${product.cid==category.cid ? "selected='selected'" : "" }>
							${category.cname }
						</option>
					</c:forEach>
				</c:if>
			</select>
			<input type="text" name="pname" placeholder="请输入商品名称" value="${product.pname }">
			<input type="submit" class="search" value="search" >
		</form>
		</th>
		<th  align="right">
			<input id="add" type="button" value="添加商品" onclick="addProduct" />
		</th>
		</tr>
		<tr>
			<th>
				<input id="selectAll" type="checkbox" >全选
				<input id="resselectAll" type="checkbox">反选
			</th>
			<th>商品序号</th>
			<th>商品名称</th>
			<th>商品图片</th>
			<th>商品价格</th>
			<th>商品描述</th>
			<th>操作</th>
		</tr>
		<c:if test="${not empty products }">
			<c:forEach items="${products }" var="product" varStatus="varStatus">
				<tr align="center">
					<td>
						<input type="checkbox" class="checkBox" name="pid" value="${product.pid }"/>
					</td>
					<td>${varStatus.count }</td>
					<td>${product.pname }</td>
					<td >
						<img width="90" height="90" src="${product.pimage }"/>
					</td>
					<td>${product.shop_price }</td>
					<td>${product.pdesc }</td>
					<td>
						<a href="findProductByIdServlet?pid=${product.pid }">修改</a> 
						<input class="delete" type="button" value="delete" onclick="deleteById('${product.pid}')" />
					</td>
					</td>
				</tr>
			</c:forEach>
			<tr>
				<td colspan="7">
				<c:if test="${not empty pageBean && not empty pageBean.result}">
					<c:forEach items="${pageBean.page }" var="product">
						
					</c:forEach>
				</c:if>
					<a href="${pageContext.request.contextPath}/searchProductByLimitCountServlet?pageNumber=${pageBean.getPageNumber+1}">下一页</a>
					<a href="${pageContext.request.contextPath}/searchProductByLimitCountServlet?pageNumber=${pageBean.getPageNumber-1}">上一页</a>
				</td>
			</tr>
		</c:if>
	</table>
</body>
</html>