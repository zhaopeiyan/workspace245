package cn.itcast.ws.cxf.service;

import javax.jws.WebService;

@WebService
public interface WeatherInterface {
	public String queryWeather(String cityName);
}
