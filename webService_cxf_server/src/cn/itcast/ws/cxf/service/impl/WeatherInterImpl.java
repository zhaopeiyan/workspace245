package cn.itcast.ws.cxf.service.impl;

import cn.itcast.ws.cxf.service.WeatherInterface;

public class WeatherInterImpl implements WeatherInterface {

	@Override
	public String queryWeather(String cityName) {
		System.out.println("from client ... "+cityName);;
		if("Miami".equals(cityName)){
			return "yh!  Miami";
		}else{
			return "oh!  NewYork";
		}
	}
}
