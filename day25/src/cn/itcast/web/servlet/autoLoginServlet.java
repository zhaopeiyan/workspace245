package cn.itcast.web.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import cn.itcast.domain.User;
import cn.itcast.web.service.UserService;

public class autoLoginServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		User user = new User();
		//1、从页面中获取数据
		Map<String, String[]> map = request.getParameterMap();
		try {
			BeanUtils.populate(user, map);
		} catch (Exception e) {
			throw new RuntimeException("封装javaBean异常");
		} 
		UserService us = new UserService();
		User existUser = us.login(user);
		if(existUser != null){
			//2、将查询到的user添加到session对象中
			request.getSession().setAttribute("existUser", existUser);
			//3、创建cookie保存数据
			Cookie cookie = new Cookie("autoLogin", user.getUsername()+":"+user.getPassword());
			cookie.setMaxAge(60);
			cookie.setPath("/");
			response.addCookie(cookie);
			request.setAttribute("msg", "登陆成功");
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}else{
			request.setAttribute("msg", "登陆失败");
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}
		
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}