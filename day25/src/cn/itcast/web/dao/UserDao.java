package cn.itcast.web.dao;

import java.sql.SQLException;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import cn.itcast.domain.User;
import cn.itcast.utils.C3P0Utils;

public class UserDao {

	public User login(User user) {
		User existUser = null;
		try {
			QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
			String sql = "select * from tbl_user where username=? and password=?";
			existUser = qr.query(sql, new BeanHandler<>(User.class),user.getUsername(),user.getPassword());
		} catch (SQLException e) {
			throw new RuntimeException("查询数据库异常"+e);
		}
		return existUser;
	}

}
