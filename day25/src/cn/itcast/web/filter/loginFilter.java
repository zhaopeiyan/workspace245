package cn.itcast.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.domain.User;
import cn.itcast.utils.CookieUtils;
import cn.itcast.web.service.UserService;

/**
 * Servlet Filter implementation class loginFilter
 */
public class loginFilter implements Filter {

    /**
     * Default constructor. 
     */
    public loginFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		//强转
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse) response;
		
		//判断session里是否有数据
		User existUser = (User)req.getSession().getAttribute("existUser");
		if(existUser == null){
			Cookie[] cookies = req.getCookies();
			Cookie cookie = CookieUtils.getCookieByName(cookies, "autoLogin");
			if(cookie != null){
				String[] value = cookie.getValue().split(":");
				User user = new User();
				user.setUsername(value[0]);
				user.setPassword(value[1]);
				User us = new UserService().login(user);
				req.getSession().setAttribute("existUser", us);
			}
		}
		
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
