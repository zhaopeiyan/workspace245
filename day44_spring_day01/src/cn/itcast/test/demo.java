package cn.itcast.test;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.itcast.dao.UserDao;

public class demo {
	
	@Test
	public void test1(){
		//创建Spring的工厂类
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
		//通过工厂类解析xml获取Bean的实例
		UserDao userDao = (UserDao) applicationContext.getBean("UserDao");
		userDao.sayHello();
	}

}
