package cn.itcast.web.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.domain.User;
import cn.itcast.web.service.UserService;

public class registerServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		User user = new User();
		UserService us = new UserService();
		//1、从页面获取数据,并封装到javabean
		String username = request.getParameter("username");
		user.setUsername(username);
		String password = request.getParameter("password");
		user.setPassword(password);
		String email = request.getParameter("email");
		user.setEmail(email);
		String sex = request.getParameter("sex");
		user.setSex(sex);
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
//		String birthday = sdf.format(request.getParameter("birthday"));
		String birthday = request.getParameter("birthday");
//		System.out.println(birthday);
		user.setBirthday(birthday);
		//2、调用service层处理业务逻辑
		int i = us.register(user);
		if(i > 0){
			response.sendRedirect("success.jsp");
		}else{
			response.sendRedirect("success.jsp");
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
