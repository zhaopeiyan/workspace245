package cn.itcast.web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.domain.User;
import cn.itcast.web.service.UserService;

public class searchUserByNameServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");
		
		User user = new User();
		UserService us = new UserService();
		//1、从页面获取数据,并封装到javabean
		String username = request.getParameter("username");
		user.setUsername(username);
		
		//得到name后先查询数据库，看是否有重名的用户
		User existUser = us.searchUserByName(username);
		if(existUser != null){
			response.getWriter().write("false");
		}else{
			response.getWriter().write("true");
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
