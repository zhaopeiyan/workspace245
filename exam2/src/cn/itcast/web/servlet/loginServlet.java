package cn.itcast.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.domain.User;
import cn.itcast.web.service.UserService;

public class loginServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		User user = new User();
		//获取网页文本框数据,并封装到javaBean
		String username = request.getParameter("username");
		System.out.println(username);
		String password = request.getParameter("password");
		System.out.println(password);
		user.setUsername(username);
		user.setPassword(password);
		//访问service层处理业务逻辑
		UserService us = new UserService();
		User existUser = us.searchUser(user);
		//System.out.println(existUser);
		if(existUser == null){
//			response.getWriter().write("false");
			request.setAttribute("fail", "<h4 style='color:red'>用户名或密码不正确....</h4>");
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}else{
			//response.getWriter().write("true");
			request.setAttribute("name", existUser.getUsername());
			request.getRequestDispatcher("index.jsp").forward(request, response);;
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
