package cn.itcast.web.service;

import cn.itcast.domain.User;
import cn.itcast.web.dao.UserDao;

public class UserService {

	/**
	 * 用户注册
	 * @param user
	 * @return
	 */
	public int register(User user) {
		// TODO Auto-generated method stub
		return new UserDao().register(user);
	}

	public User searchUserByName(String username) {
		// TODO Auto-generated method stub
		return new UserDao().searchUserByName(username);
	}

	public User searchUser(User user) {
		// TODO Auto-generated method stub
		return new UserDao().searchUser(user);
	}

}
