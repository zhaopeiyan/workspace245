package cn.itcast.web.dao;

import java.sql.SQLException;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import cn.itcast.domain.User;
import cn.itcast.utils.DataSourceUtils;

public class UserDao {

	public int register(User user) {
		// TODO Auto-generated method stub
		int i = 0;
		try {
			QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
			String sql = "insert into user values(null,?,?,?,?,?)";
			Object[] params = {user.getUsername(),user.getPassword(),user.getEmail(),user.getSex(),user.getPassword()};
			i = qr.update(sql, params);
		} catch (SQLException e) {
			throw new RuntimeException("查询数据库异常"+e);
		}
		return i;
	}

	public User searchUserByName(String username) {
		User user = null;
		try {
			QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
			String sql = "select * from user where username=?";
			user = qr.query(sql, new BeanHandler<>(User.class), username);
		} catch (SQLException e) {
			throw new RuntimeException("查询数据库异常"+e);
		}
		return user;
	}

	public User searchUser(User user) {
		User existUser = null;
		QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
		String sql = "select * from user where username=? and password=?";
		try {
		 existUser = qr.query(sql, new BeanHandler<>(User.class), user.getUsername(),user.getPassword());
		} catch (SQLException e) {
			throw new RuntimeException("查询数据库异常"+e);
		}
		return existUser;
	}

}
