package cn.itcast.utils;

import javax.servlet.http.Cookie;

public class CookUtils {

	public static Cookie findCookieByName(Cookie[] cookies, String name) {

		if (cookies != null) {

			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(name)) {
					return cookie;
				}
			}
		}
		return null;
	}
}
