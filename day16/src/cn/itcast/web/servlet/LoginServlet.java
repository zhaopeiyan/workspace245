package cn.itcast.web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		//获取网页数据
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String vc = request.getParameter("verifycode");
		String attribute = (String) request.getSession().getAttribute("vCode");
		
		if(attribute.equalsIgnoreCase(vc)){
			//request.getRequestDispatcher("").forward(request, response);
			response.getWriter().print("<h3 style='color:blue'>验证成功</h3>");
		}else{
			response.getWriter().print("<h3 style='color:red'>验证失败</h3>");
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}