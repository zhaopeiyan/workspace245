package cn.itcast.web.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.utils.CookUtils;

public class TheLastLoginServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		/*
		 * 获取指定名称的cookie对象
		 * 其中name = TheLastLogin
		 */
		//1、获取所有cookie
		Cookie[] cookies = request.getCookies();
		Cookie cookie = CookUtils.findCookieByName(cookies,"TheLastLogin");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");
		if(cookie != null){
			response.getWriter().print("<h3 style='color:blue'>上次登录时间："+cookie.getValue()+"</h3>");
			Cookie cookie1 = new Cookie("TheLastLogin", format.format(new Date())); 
			cookie1.setMaxAge(60*60);
			cookie1.setPath("/day16/");
			response.addCookie(cookie1);
		}else{
			response.getWriter().println("<h3 style='color:red'>欢迎你的登陆！</h3>");
			Cookie cookie2 = new Cookie("TheLastLogin",format.format(new Date()));
			cookie2.setPath("/day16/");
			cookie2.setMaxAge(60*60);
			response.addCookie(cookie2);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
