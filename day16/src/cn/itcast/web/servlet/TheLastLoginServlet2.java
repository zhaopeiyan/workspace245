package cn.itcast.web.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.utils.CookUtils;

public class TheLastLoginServlet2 extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");
		Cookie cookie = new Cookie("TheLastLogin", format.format(new Date().getTime()));
		
		Cookie[] cookies = request.getCookies();
		Cookie findCookieByName = CookUtils.findCookieByName(cookies, "TheLastLogin");
		if(findCookieByName == null){
			response.getWriter().print("<h3 style='color:red'>欢迎第一次登陆</h3>");
			cookie.setValue(format.format(new Date().getTime()));
			response.addCookie(cookie);
		}else{
			response.getWriter().print("<h3 style='color:red'>您上一次登陆时间为："+findCookieByName.getValue()+"</h3>");
			
			findCookieByName.setValue(format.format(new Date().getTime()));
			response.addCookie(findCookieByName);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}