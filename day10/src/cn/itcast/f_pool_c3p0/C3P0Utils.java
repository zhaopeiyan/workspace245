package cn.itcast.f_pool_c3p0;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class C3P0Utils {
	//使用默认配置
//	private static ComboPooledDataSource dataSource = new ComboPooledDataSource();
	//使用命名配置
	private static ComboPooledDataSource dataSource = new ComboPooledDataSource("itcast");
	
	/*
	 * 获得数据源
	 */
	public static DataSource getDataSource(){
		return dataSource;
	}
	
	/*
	 * 获得链接
	 */
	public static Connection getConn(){
		try {
			return dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("获取连接失败哦"+e);
		}
	}
}
