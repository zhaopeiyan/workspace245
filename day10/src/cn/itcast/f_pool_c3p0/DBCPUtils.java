package cn.itcast.f_pool_c3p0;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.management.RuntimeErrorException;
import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSourceFactory;

public class DBCPUtils {
	private static DataSource dataSource;
	static{
		try {
		//1.加载配置文件，获得文件流
		InputStream is = DBCPUtils.class.getClassLoader().getResourceAsStream("reource/dbcp.properties");
		//2. 使用Properties处理配置文件
		Properties prop = new Properties();
		prop.load(is);
		//3.使用工具类创建链接池（数据源）
		dataSource = BasicDataSourceFactory.createDataSource(prop);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("创建连接池失败哦"+e);
		}
	}
	//获取连接池
	public static DataSource getDataSource(){
		return dataSource;
	}
	//获取链接
	public static Connection getConn(){
		try {
			return dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("获取链接异常"+e);
		}
	}
}
