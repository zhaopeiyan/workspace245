package cn.itcast.d_dbcp;

import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSourceFactory;

public class dbcputils2 {
	private static DataSource dataSource;
	static {
		try {
			// 1.加载配置文件,获得文件流
			InputStream is = dbcputils2.class.getClassLoader().getResourceAsStream("dbcp.properties");
			// 2.使用properties处理配置文件
			Properties prop = new Properties();
			prop.load(is);
			// 3.使用工具类创建连接池
			dataSource = BasicDataSourceFactory.createDataSource(prop);
		} catch (Exception e) {
			throw new RuntimeException("获取连接池对象失败" + e);
		}
	}
	
	public static DataSource getDateSource(){
		return dataSource;
	}
	
	public static Connection getConn(){
		try {
			return dataSource.getConnection();
		} catch (Exception e) {
			throw new RuntimeException("获取链接失败"+e);
		}
	}
}
