package cn.itcast.d_dbcp;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSourceFactory;

public class dbcpUtils {
	private static DataSource dataSource;

	static {
		try {
			// 1.加载配置文件，获得文件流
			InputStream is = dbcpUtils.class.getClassLoader().getResourceAsStream("dbcp.properties");
			// 2.使用properties处理配置文件
			Properties prop = new Properties();
			prop.load(is);
			//3.使用工具类创建连接池
			dataSource = BasicDataSourceFactory.createDataSource(prop);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

		public static DataSource getDataSource(){
			return dataSource;
		}

		public static Connection getConn(){
			try {
				return dataSource.getConnection();
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("---" + e);
			}
		}
}
