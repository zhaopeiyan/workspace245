package cn.itcast.e_pool_custom_wrapper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;

public class jdbcUtils {

	// 1.创建容器，用于存放链接Connection
	private static LinkedList<Connection> pool = new LinkedList<>();

	// 1.1初始化连接池中的链接
	static {

		try {
			// 1.注册驱动
			Class.forName("com.mysql.jdbc.Driver");

			for (int i = 0; i < 3; i++) {
				try {
					// 2.获得链接
					Connection conn;
					conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/day09_db", "root", "root");
					// 3.将链接放到连接池中
					pool.add(conn);
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("获取连接失败" + e);
				}

			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("注册驱动失败" + e);
		}
	}
	/*
	 * 2.获得链接，从连接池中获得链接
	 */
	public static Connection getConn(){
		return pool.removeFirst();
	}
	/*
	 * 3.归还链接  释放资源，当链接connection close时，归还给连接池
	 */
	public static void release(Connection conn){
		if(conn != null){
			pool.add(conn);
		}
	}
	

}
