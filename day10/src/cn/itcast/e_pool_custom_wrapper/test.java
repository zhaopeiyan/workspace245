package cn.itcast.e_pool_custom_wrapper;

import java.sql.Connection;

public class test {
	
	public static void main (String[] args){
		//获得链接
		Connection conn = jdbcUtils.getConn();
		System.out.println("使用："+conn+","+Thread.currentThread());
		
		//释放资源
		jdbcUtils.release(conn);
	}

}
