package cn.itcast.test;

import java.util.List;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import cn.itcast.c_c3p0.C3P0Utils;
import cn.itcast.javabean.category;

public class demo2 {
	
	public static void main(String[] args) throws Exception {
		//获取连接池对象
		QueryRunner qr = new QueryRunner(C3P0Utils.getDateSource());
		String sql = "SELECT * FROM category";
		List<category> list = qr.query(sql, new BeanListHandler<category>(category.class));
		for (category c : list) {
			System.out.println(c.getCid()+c.getCname());
		}
	}

}
