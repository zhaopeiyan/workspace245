package cn.itcast.test;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.junit.Test;

import cn.itcast.c_c3p0.C3P0Utils;
import cn.itcast.javabean.category;


public class demo {
	//1.核心类
	static QueryRunner qr = new QueryRunner(C3P0Utils.getDateSource());
	@Test
	public void insert(){
		try {
			//2.sql
			String sql = "insert into category(cid,cname) values (?,?)";
			//3.执行
			Object[] params = {"c003","飞机"};
			int i = qr.update(sql,params);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("添加数据失败！"+e);
		}
	}
	@Test
	public void update(){
		try {
		//sql
		String sql = "update category set cname = ? where cid = ?";
		//使用可变参数
			int update = qr.update(sql, "大炮","c001");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("修改数据失败!"+e);
		}
	}
	@Test
	public void delete(){
		try {
			String sql = "delete from category where cid = ?";
			int i = qr.update(sql, "c001");
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("删除失败"+e);
		}
	}
	@Test
	public void select(){
		try {
			String sql = "select * from category";
			List<category> list = qr.query(sql, new BeanListHandler<category>(category.class));
			for (category c : list) {
				System.out.println(c.getCid()+"---"+c.getCname());
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("查询失败"+e);
		}
	}
	@Test
	public void count(){
		try {
			String sql = "select count(*) from category";
			Long query = (Long) qr.query(sql, new ScalarHandler());
			System.out.println("有"+query+"条记录！");
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("查询异常"+e);
		}
	}
}
