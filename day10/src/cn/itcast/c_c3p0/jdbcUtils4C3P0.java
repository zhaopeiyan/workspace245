package cn.itcast.c_c3p0;

import java.sql.Connection;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class jdbcUtils4C3P0 {
	public static void main(String[] args) throws Exception {
		//核心类.c3p0连接池
		//获取c3p0连接池
		ComboPooledDataSource dataSource = new ComboPooledDataSource("itcast");
		Connection conn = dataSource.getConnection();
		System.out.println("+++++++++++++++++==");
		conn.close();
		System.out.println(conn);
	}
}
