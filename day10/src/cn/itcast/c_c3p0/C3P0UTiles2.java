package cn.itcast.c_c3p0;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class C3P0UTiles2 {
	//使用默认的配置读取.xml配置文件
	static ComboPooledDataSource dataSource = new ComboPooledDataSource();
	
	/**
	 * 获得数据源（连接池）
	 */
	public static DataSource getDataSource(){
		return dataSource;
	}
	
	/**
	 * 获取链接
	 */
	public static Connection getConn(){
		try {
			return dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("获取连接失败"+e);
		}
				
	}
	
}
