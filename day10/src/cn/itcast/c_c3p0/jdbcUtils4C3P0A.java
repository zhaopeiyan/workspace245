package cn.itcast.c_c3p0;

import java.sql.Connection;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class jdbcUtils4C3P0A {
	public static void main(String[] args) throws Exception {
		//核心类.c3p0连接池
		//获取c3p0连接池
		Connection conn = C3P0Utils.getConn();
		conn.close();
		System.out.println(conn);
	}
}
