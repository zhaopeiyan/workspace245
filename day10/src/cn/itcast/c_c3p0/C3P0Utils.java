package cn.itcast.c_c3p0;

import java.sql.Connection;

import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class C3P0Utils {
	//初始化c3p0连接池
	static ComboPooledDataSource dataSource = new ComboPooledDataSource();
	//获取连接
	public static Connection getConn () throws Exception{
		return dataSource.getConnection();
	}
	//获取c3p0连接池
	public static DataSource getDateSource(){
		return dataSource;
	}
}
