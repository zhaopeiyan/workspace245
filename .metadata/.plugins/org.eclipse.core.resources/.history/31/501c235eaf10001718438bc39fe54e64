<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:context="http://www.springframework.org/schema/context"
	xmlns:aop="http://www.springframework.org/schema/aop"
	xmlns:tx="http://www.springframework.org/schema/tx"
	xsi:schemaLocation="
        http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop.xsd
        http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd">
	
	<!-- 引入jdbc.properties -->
	<context:property-placeholder location="classpath:jdbc.properties"/>
	
	<!-- 创建连接池 -->
	<bean class="com.mchange.v2.c3p0.ComboPooledDataSource" id="dataSource">
		<property name="driverClass" value="${jdbc.driver}"></property>
		<property name="jdbcUrl" value="${jdbc.url}"></property>
		<property name="user" value="${jdbc.username}"></property>
		<property name="password" value="${jdbc.password}"></property>
	</bean>
	
	<!-- 创建sessionFactory -->
	<bean class="org.springframework.orm.hibernat5.LocalSessionFactoryBean" id="sessionFactory">
		<!-- 不再加载hibernate的核心配置文件 将那些配置转到该sessionFactory中 -->
		<!-- 1\连接池 -->
		<property name="dataSource" ref="dataSource"></property>
		<!-- 2\hibernate的其他配置 -->
		<property name="hibernateProperties">
			<props>
				<prop key="hibernate.dialect">org.hibernate.dialect.MySQLDialect</prop>
				<prop key="hibernate.show">true</prop>
				<prop key="hibernate.format_sql">true</prop>
				<prop key="hibernate.hbm2ddl.auto">update</prop>
			</props>
		</property>
		<!-- 3\加载实体的映射文件 -->
		<property name="mappingResources">
			<list>
				<value>cn/itcast/domain/Customer.hbm.xml</value>
			</list>
		</property>
	</bean>
	
	<!-- 开启声明式事务 -->
	<!-- 平台事务管理器  不同的底层实现用到事务管理器是不同的 -->
	<bean class="org.springframework.orm.hibernate5.HibernateTransactionManager" id="transactionManager">
		<property name="sessionFactory" ref="sessionFactory"></property>
	</bean>
	
	<!-- 配置事务的增强切面 -->
	<tx:advice id="txAdvice" transaction-manager="transactionManager">
		<tx:attributes>
			<tx:method name="*"/>
		</tx:attributes>
	</tx:advice>
	<!-- 事务增强的aop -->
	<aop:config>
		<aop:pointcut expression="execution(* cn.itcast.service.impl.*.*(..))" id="txPointcut"/>
		<aop:advisor advice-ref="txAdvice" pointcut-ref="txPointcut"/>
	</aop:config>
	
	<!-- 配置hibernateTemplate模板 -->
	<bean class="org.springframework.orm.hibernate5.HibernateTemplate" id="hibernateTemplate">
		<property name="sessionFactory" ref="sessionFactory"></property>
	</bean>
	
	<!-- 配置dao层 -->
	<bean id="customerDao" class="cn.itcast.dao.impl.CustomerDaoImpl">
		<property name="hibernateTemplate" ref="hibernateTemplate"></property>
	</bean>
	
	<!-- 配置service层 -->
	<bean id="customerService" class="cn.itcast.service.impl.CustomerServiceImpl">
		<property name="customerDao" ref="customerDao"></property>
	</bean>
	
	
</beans>