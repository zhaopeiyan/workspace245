/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.52
 * Generated at: 2017-06-04 09:29:44 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.WEB_002dINF.console.frame;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;

public final class ad_005fleft_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.HashMap<java.lang.String,java.lang.Long>(1);
    _jspx_dependants.put("/WEB-INF/console/frame/../date.jsp", Long.valueOf(1495616715957L));
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("<title>Insert title here</title>\r\n");
      out.write("<link href=\"/css/admin.css\" rel=\"stylesheet\" type=\"text/css\" />\r\n");
      out.write("<link href=\"/css/theme.css\" rel=\"stylesheet\" type=\"text/css\" />\r\n");
      out.write("<link href=\"/css/jquery.validate.css\" rel=\"stylesheet\" type=\"text/css\" />\r\n");
      out.write("<link href=\"/css/jquery.treeview.css\" rel=\"stylesheet\" type=\"text/css\" />\r\n");
      out.write("<link href=\"/css/jquery.ui.css\" rel=\"stylesheet\" type=\"text/css\" />\r\n");
      out.write("\r\n");
      out.write("<script src=\"/js/jquery.js\" type=\"text/javascript\"></script>\r\n");
      out.write("<script src=\"/js/jquery.ext.js\" type=\"text/javascript\"></script>\r\n");
      out.write("<script src=\"/js/jquery.form.js\" type=\"text/javascript\"></script>\r\n");
      out.write("<script src=\"/js/itcast.js\" type=\"text/javascript\"></script>\r\n");
      out.write("<script src=\"/js/admin.js\" type=\"text/javascript\"></script>\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("\t$(function() {\r\n");
      out.write("\t\t$(\"#browser\").treeview({\r\n");
      out.write("\t\t\turl : \"/position/tree.do\"\r\n");
      out.write("\t\t});\r\n");
      out.write("\t})\r\n");
      out.write("</script>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\t<div class=\"left\">\r\n");
      out.write("\t\t");
      out.write("\r\n");
      out.write("<div class=\"date\">\r\n");
      out.write(" <span>现在时间： \r\n");
      out.write(" \t<script type=\"text/javascript\">\r\n");
      out.write("       var day=\"\";\r\n");
      out.write("       var month=\"\";\r\n");
      out.write("       var ampm=\"\";\r\n");
      out.write("       var ampmhour=\"\";\r\n");
      out.write("       var myweekday=\"\";\r\n");
      out.write("       var year=\"\";\r\n");
      out.write("       mydate=new Date();\r\n");
      out.write("       myweekday=mydate.getDay();\r\n");
      out.write("       mymonth=mydate.getMonth()+1;\r\n");
      out.write("       myday= mydate.getDate();\r\n");
      out.write("       year= mydate.getFullYear();\r\n");
      out.write("       if(myweekday == 0)\r\n");
      out.write("       weekday=\" 星期日 \";\r\n");
      out.write("       else if(myweekday == 1)\r\n");
      out.write("       weekday=\" 星期一 \";\r\n");
      out.write("       else if(myweekday == 2)\r\n");
      out.write("       weekday=\" 星期二 \";\r\n");
      out.write("       else if(myweekday == 3)\r\n");
      out.write("       weekday=\" 星期三 \";\r\n");
      out.write("       else if(myweekday == 4)\r\n");
      out.write("       weekday=\" 星期四 \";\r\n");
      out.write("       else if(myweekday == 5)\r\n");
      out.write("       weekday=\" 星期五 \";\r\n");
      out.write("       else if(myweekday == 6)\r\n");
      out.write("       weekday=\" 星期六 \";\r\n");
      out.write("       document.write(year+\"年\"+mymonth+\"月\"+myday+\"日 \"+weekday);\r\n");
      out.write("      </script>\r\n");
      out.write(" </span>\r\n");
      out.write("</div> ");
      out.write("\r\n");
      out.write("\t\t<div class=\"fresh\">\r\n");
      out.write("\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n");
      out.write("\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t<td height=\"35\" align=\"center\"><img\r\n");
      out.write("\t\t\t\t\t\tsrc=\"/images/admin/refresh-icon.png\" />&nbsp;&nbsp;<a\r\n");
      out.write("\t\t\t\t\t\thref=\"javascript:location.href=location.href\">刷新</a></td>\r\n");
      out.write("\t\t\t\t</tr>\r\n");
      out.write("\t\t\t</table>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t<!-- </div> -->\r\n");
      out.write("\t\t<ul class=\"filetree treeview\" id=\"browser\"></ul>\r\n");
      out.write("</div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
