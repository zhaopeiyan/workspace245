/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.52
 * Generated at: 2017-05-27 14:04:28 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.WEB_002dINF.console.brand;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;

public final class add_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.HashMap<java.lang.String,java.lang.Long>(1);
    _jspx_dependants.put("/WEB-INF/console/brand/../head.jsp", Long.valueOf(1495616715982L));
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write('\r');
      out.write('\n');
      out.write("<link href=\"/css/admin.css\" rel=\"stylesheet\" type=\"text/css\"/>\r\n");
      out.write("<link href=\"/css/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>\r\n");
      out.write("<link href=\"/css/jquery.validate.css\" rel=\"stylesheet\" type=\"text/css\"/>\r\n");
      out.write("<link href=\"/css/jquery.treeview.css\" rel=\"stylesheet\" type=\"text/css\"/>\r\n");
      out.write("<link href=\"/css/jquery.ui.css\" rel=\"stylesheet\" type=\"text/css\"/>\r\n");
      out.write("\r\n");
      out.write("<script type=\"text/javascript\" src=\"/js/kindeditor-4.1.10/kindeditor-all-min.js\"></script>\r\n");
      out.write("<script type=\"text/javascript\" src=\"/js/kindeditor-4.1.10/lang/zh_CN.js\"></script>\r\n");
      out.write("<script src=\"/js/jquery.js\" type=\"text/javascript\"></script>\r\n");
      out.write("<script src=\"/js/jquery.ext.js\" type=\"text/javascript\"></script>\r\n");
      out.write("<script src=\"/js/jquery.form.js\" type=\"text/javascript\"></script>\r\n");
      out.write("<script src=\"/js/itcast.js\" type=\"text/javascript\"></script>\r\n");
      out.write("<script src=\"/js/admin.js\" type=\"text/javascript\"></script>\r\n");
      out.write("\r\n");
      out.write("<link rel=\"stylesheet\" href=\"/css/style.css\" />\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n");
      out.write("<head>\r\n");
      out.write("<title>babasport-add</title>\r\n");
      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("\tfunction uploadPic(){\r\n");
      out.write("\t\tvar options = {\r\n");
      out.write("\t\t\t\turl : \"/upload/uploadFck.do\",\r\n");
      out.write("\t\t\t\ttype : \"post\",\r\n");
      out.write("\t\t\t\tdataType : \"json\",\r\n");
      out.write("\t\t\t\tsuccess : function (data) {\r\n");
      out.write("\t\t\t\t\t//alert(123);\r\n");
      out.write("\t\t\t\t\t$(\"#allUrl\").attr(\"src\",data.url);\r\n");
      out.write("\t\t\t\t\t$(\"#imgUrl\").attr(\"value\",data.url);\r\n");
      out.write("\t\t\t\t}\r\n");
      out.write("\t\t}\r\n");
      out.write("\t\t//jquery.form.js   插件   异步ajax上传图片\r\n");
      out.write("\t\t$(\"#jvForm\").ajaxSubmit(options);\r\n");
      out.write("\t}\r\n");
      out.write("\r\n");
      out.write("</script>\r\n");
      out.write("\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("<div class=\"box-positon\">\r\n");
      out.write("\t<div class=\"rpos\">当前位置: 品牌管理 - 添加</div>\r\n");
      out.write("\t<form class=\"ropt\">\r\n");
      out.write("\t\t<input type=\"submit\" onclick=\"this.form.action='v_list.shtml';\" value=\"返回列表\" class=\"return-button\"/>\r\n");
      out.write("\t</form>\r\n");
      out.write("\t<div class=\"clear\"></div>\r\n");
      out.write("</div>\r\n");
      out.write("<div class=\"body-box\" style=\"float:right\">\r\n");
      out.write("\t<form id=\"jvForm\" action=\"/brand/add.do\" method=\"post\" enctype=\"multipart/form-data\">\r\n");
      out.write("\t\t<table cellspacing=\"1\" cellpadding=\"2\" width=\"100%\" border=\"0\" class=\"pn-ftable\">\r\n");
      out.write("\t\t\t<tbody>\r\n");
      out.write("\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t<td width=\"20%\" class=\"pn-flabel pn-flabel-h\">\r\n");
      out.write("\t\t\t\t\t\t<span class=\"pn-frequired\">*</span>\r\n");
      out.write("\t\t\t\t\t\t品牌名称:</td><td width=\"80%\" class=\"pn-fcontent\">\r\n");
      out.write("\t\t\t\t\t\t<input type=\"text\" class=\"required\" name=\"name\" maxlength=\"100\"/>\r\n");
      out.write("\t\t\t\t\t</td>\r\n");
      out.write("\t\t\t\t</tr>\r\n");
      out.write("\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t<td width=\"20%\" class=\"pn-flabel pn-flabel-h\">\r\n");
      out.write("\t\t\t\t\t\t<span class=\"pn-frequired\">*</span>\r\n");
      out.write("\t\t\t\t\t\t上传商品图片(90x150尺寸):</td>\r\n");
      out.write("\t\t\t\t\t\t<td width=\"80%\" class=\"pn-fcontent\">\r\n");
      out.write("\t\t\t\t\t\t注:该尺寸图片必须为90x150。\r\n");
      out.write("\t\t\t\t\t</td>\r\n");
      out.write("\t\t\t\t</tr>\r\n");
      out.write("\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t<td width=\"20%\" class=\"pn-flabel pn-flabel-h\"></td>\r\n");
      out.write("\t\t\t\t\t\t<td width=\"80%\" class=\"pn-fcontent\">\r\n");
      out.write("\t\t\t\t\t\t<img width=\"100\" height=\"100\" id=\"allUrl\"/>\r\n");
      out.write("\t\t\t\t\t\t<input type=\"hidden\" name=\"imgUrl\" id=\"imgUrl\" />\r\n");
      out.write("\t\t\t\t\t\t<input type=\"file\" name=\"pic\" onchange=\"uploadPic()\" />\r\n");
      out.write("\t\t\t\t\t</td>\r\n");
      out.write("\t\t\t\t</tr>\r\n");
      out.write("\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t<td width=\"20%\" class=\"pn-flabel pn-flabel-h\">\r\n");
      out.write("\t\t\t\t\t\t品牌描述:</td><td width=\"80%\" class=\"pn-fcontent\">\r\n");
      out.write("\t\t\t\t\t\t<input type=\"text\" class=\"required\" name=\"description\" maxlength=\"80\"  size=\"60\"/>\r\n");
      out.write("\t\t\t\t\t</td>\r\n");
      out.write("\t\t\t\t</tr>\r\n");
      out.write("\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t<td width=\"20%\" class=\"pn-flabel pn-flabel-h\">\r\n");
      out.write("\t\t\t\t\t\t排序:</td><td width=\"80%\" class=\"pn-fcontent\">\r\n");
      out.write("\t\t\t\t\t\t<input type=\"text\" class=\"required\" name=\"sort\" maxlength=\"80\"/>\r\n");
      out.write("\t\t\t\t\t</td>\r\n");
      out.write("\t\t\t\t</tr>\r\n");
      out.write("\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t<td width=\"20%\" class=\"pn-flabel pn-flabel-h\">\r\n");
      out.write("\t\t\t\t\t\t是否可用:</td><td width=\"80%\" class=\"pn-fcontent\">\r\n");
      out.write("\t\t\t\t\t\t<input type=\"radio\" name=\"isDisplay\" value=\"1\" checked=\"checked\"/>可用\r\n");
      out.write("\t\t\t\t\t\t<input type=\"radio\" name=\"isDisplay\" value=\"0\"/>不可用\r\n");
      out.write("\t\t\t\t\t</td>\r\n");
      out.write("\t\t\t\t</tr>\r\n");
      out.write("\t\t\t</tbody>\r\n");
      out.write("\t\t\t<tbody>\r\n");
      out.write("\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t<td class=\"pn-fbutton\" colspan=\"2\">\r\n");
      out.write("\t\t\t\t\t\t<input type=\"submit\" class=\"submit\" value=\"提交\"/> &nbsp; <input type=\"reset\" class=\"reset\" value=\"重置\"/>\r\n");
      out.write("\t\t\t\t\t</td>\r\n");
      out.write("\t\t\t\t</tr>\r\n");
      out.write("\t\t\t</tbody>\r\n");
      out.write("\t\t</table>\r\n");
      out.write("\t</form>\r\n");
      out.write("</div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
