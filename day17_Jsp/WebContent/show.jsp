<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="cn.itcast.domain.Product"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//Dth HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dth">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<table border="1px"  align="center" cellpadding="0" cellspacing="0" width="50%">
		<tr style="color:red">
			<th>商品id</th>
			<th>商品名称</th>
			<th>商品价格</th>
			<th>商品描述</th>
		</tr>
		<%
			List<Product> products = (List)request.getAttribute("products");
			if(products != null || products.size() > 0){
				for(Product p : products){
					out.print("<td>"+p.getPid()+"</td>");
					out.print("<td>"+p.getPname()+"</td>");
					out.print("<td>"+p.getPrice()+"</td>");
					out.print("<td>"+p.getPdesc()+"</td>");
					out.print("<tr>");
				}
			}else{
				out.print("<tr><td colspan='4'>暂无商品信息</td></tr>");
			}
		%>
	</table>

</body>
</html>;