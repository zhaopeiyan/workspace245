package cn.itcast.test;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.itcast.dao.ProductDao;

public class Demo1 {
	
	@Test
	public void test2(){
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext2.xml");
		ProductDao productDao = (ProductDao) applicationContext.getBean("productDao");
		productDao.update();
	}
	
	
	@Test
	public void test1(){
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
		ProductDao productDao = (ProductDao) applicationContext.getBean("productDao");
		productDao.save();
	}

}
