package cn.itcast.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class myAspectAnno {
	
	@Before(value="execution(* cn.itcast.dao.ProductDao.*(..))")
	public void before(){
		System.out.println("前置通知...");
	}
	
	@After(value="execution(* cn.itcast.dao.ProductDao.*(..))")
	public void after(){
		System.out.println("最终通知...");
	}
	
	@Around(value="execution(* cn.itcast.dao..*(..))")
	public void around(ProceedingJoinPoint pjp) throws Throwable{
		System.out.println("环绕前 ....");
		pjp.proceed();
		System.out.println("环绕后 ....");
	}
}
