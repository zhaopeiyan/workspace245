package cn.itcast.acount.service.impl;

import org.springframework.transaction.annotation.Transactional;

import cn.itcast.acount.dao.AccountDao;
import cn.itcast.acount.service.AccountService;

//@Transactional
public class AccountServiceImpl implements AccountService {

	// 业务层注入Dao
	private AccountDao accountDao;
	public void setAccountDao(AccountDao accountDao) {
		this.accountDao = accountDao;
	}

	@Override
	public void transfer(String outer, String inner, Double money) {
		accountDao.outMoney(outer,money);
//		int i = 1/0;
		accountDao.inMoney(inner,money);
	}

}
