package cn.itcast.acount.demo;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.itcast.acount.service.AccountService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext4accountByAspectJ.xml")
//@ContextConfiguration("classpath:applicationContext4accountBy.xml")
public class accountDemo {
	
	
	/**
	 * 注入代理类
	 */
	//@Resource(name="accountServiceProxy")
	
	@Autowired
	private AccountService accountService;
	
	@Test
	public void demo1(){
		accountService.transfer("roy", "王守仁", 500d);
	}

}
