package cn.itcast.acount.demo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import cn.itcast.acount.domain.Account;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext4account.xml")
public class demo3 {

	@Resource(name = "jdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	@Test
	// 插入操作
	public void insert() {
		jdbcTemplate.update("insert into account values(null,?,?)", "kobe.byant", 3500d);
		jdbcTemplate.execute("insert into account values(null,'roy',888)");
	}

	@Test
	// 更新操作
	public void update() {
		jdbcTemplate.update("update account set money=? where name=?", 6666d, "小明");
	}

	@Test
	// 删除操作
	public void delete() {
		jdbcTemplate.update("delete from account where name=?", "kobe.byant");

	}

	@Test
	// 查询一条记录
	public void query() {
		Account account = jdbcTemplate.queryForObject("select * from account where name=?", new RowMapper<Account>() {

			@Override
			public Account mapRow(ResultSet rs, int rowNum) throws SQLException {
				Account account = new Account();
				account.setName(rs.getString("name"));
				account.setMoney(rs.getDouble("money"));
				return account;
			}
		}, "roy");
		System.out.println(account.toString());
	}

	@Test
	// 查询所有记录
	public void queryList() {
		List<Account> list = jdbcTemplate.query("select * from account", new RowMapper<Account>() {
			@Override
			public Account mapRow(ResultSet rs, int rowNum) throws SQLException {
				// TODO Auto-generated method stub
				Account account = new Account();
				account.setName(rs.getString("name"));
				account.setMoney(rs.getDouble("money"));
				return account;
			}
		});
		for(Account account : list){
			System.out.println(account.toString());
		}
	}
}
