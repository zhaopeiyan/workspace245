package cn.itcast.acount.dao.impl;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import cn.itcast.acount.dao.AccountDao;

public class AccountDaoImpl extends JdbcDaoSupport implements AccountDao {

	
	@Override
	public void outMoney(String outer, Double money) {
		// TODO Auto-generated method stub
		//jdbcTemplate.update("update account set money=money-? where name=?",money,outer);
		this.getJdbcTemplate().update("update account set money=money-? where name=?",money,outer);
		
	}

	@Override
	public void inMoney(String inner, Double money) {
		// TODO Auto-generated method stub
//		jdbcTemplate.update("update account set money=money+? where name=?",money,inner);
		this.getJdbcTemplate().update("update account set money=money+? where name=?",money,inner);
	}

}
