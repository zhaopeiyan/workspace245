package cn.itcast.acount.dao;

public interface AccountDao {

	void outMoney(String outer, Double money);

	void inMoney(String inner, Double money);

}
