package cn.itcast.request;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.domain.User;
import cn.itcast.service.UserService;

public class LoginServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");
		
		//获取网页数据,处理get提交方式的request请求乱码
		String username = request.getParameter("username");
		username = new String(username.getBytes("iso8859-1"),"utf-8");
		String password = request.getParameter("password");
		password = new String(password.getBytes("iso8859-1"), "utf-8");
		
		//将数据封装到user类中
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		//调用service层方法
		User existUser = new UserService().login(user);
		//System.out.println(existUser);
		if(existUser != null){
			//request.setAttribute("msg", "恭喜你,登陆成功！");
			//request.getRequestDispatcher("successServlet").forward(request, response);
			request.getRequestDispatcher("successServlet").include(request, response);
			request.getAttribute("msg");
		}else{
			request.setAttribute("msg", "对不起，用户名或密码错误！");
			request.getRequestDispatcher("failServlet").forward(request, response);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}