package cn.itcast.request;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import cn.itcast.domain.User;
import cn.itcast.service.UserService;

public class RegisterServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");
		
		//获取网页数据
		Map<String, String[]> parameterMap = request.getParameterMap();
		
		ServletContext servletContext = this.getServletContext();
		request.setAttribute("username", request.getParameter("username"));
		
		//将获取到的数据封装到javabean
		User user = new User();
		try {
			BeanUtils.populate(user, parameterMap);
			//单独封装hobby
			/*String[] parameterValues = request.getParameterValues("hobby");//获取多个值
			for (String s : parameterValues) {
				System.out.print(s+" ");
			}
			String hobby = Arrays.toString(parameterValues);
			user.setHobby(hobby);*/
			for (String key : parameterMap.keySet()) {
				System.out.print(key+":"+Arrays.toString(parameterMap.get(key)));
				  }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("封装hobby异常"+e);
		} 
		//调用service层
		UserService userService = new UserService();
		userService.register(user);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}