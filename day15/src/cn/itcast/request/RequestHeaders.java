package cn.itcast.request;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RequestHeaders extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");
		//获取请求消息中所有头字段
		Enumeration<String> headerNames = request.getHeaderNames();
		//使用循环遍历所有请求头，并通过getHeader()方法获取一个指定名称的头字段
		while(headerNames.hasMoreElements()){
			String headerName = headerNames.nextElement();
			response.getWriter().print(headerName+":"+request.getHeader(headerName)+"<br>");
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}