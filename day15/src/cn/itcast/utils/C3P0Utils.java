package cn.itcast.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

/**
 * C3P0工具类
 * 
 * @author Never Say Never
 * @date 2016年11月26日
 * @version V1.0
 */
public class C3P0Utils {
    // 1.使用无参的方法，它会默认加载c3p0-config.xml里面的<default-config>配置项
    // 如果指定了参数，那么他会加载c3p0-config.xml里面的<named-config>配置项
    private static ComboPooledDataSource dataSource = new ComboPooledDataSource();

    /**
     * 提供数据源方法
     */
    public static DataSource getDataSource() {
        return dataSource;
    }

    /**
     * 获得连接的方法
     */
    public static Connection getConnnection() {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * 释放资源方法
     */

    public static void release(Connection conn, PreparedStatement pstmt, ResultSet rs) {
        if (rs != null)
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        if (pstmt != null)
            try {
                pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        if (conn != null)
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
    }

}
