package cn.itcast.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ArrayListHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import cn.itcast.domain.User;
import cn.itcast.utils.C3P0Utils;

public class UserDao {

	public void register(User user) {
		// 获取数据库链接对象
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		//定义sql语句
		String sql = "insert into user values (null,?,?,?)";
		String[] params = {user.getUsername(),user.getPassword(),user.getHobby()};
		try {
			int i = qr.update(sql, params);
		} catch (SQLException e) {
			throw new RuntimeException("更新数据异常!"+e);
		}
	}

	public User login(User user) {
		// TODO Auto-generated method stub
		User query = null;
		//获取数据库连接对象
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		//定义sql语句
		String sql = "select * from user where username=? and password=?";
		//使用可变参数执行查询
		try {
			query = qr.query(sql,new BeanHandler<User>(User.class), user.getUsername(),user.getPassword());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("查询数据异常"+e);
		}
		return query;
	}
}
