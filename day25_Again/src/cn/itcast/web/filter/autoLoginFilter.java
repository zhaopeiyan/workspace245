package cn.itcast.web.filter;

import java.io.IOException;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.domain.User;
import cn.itcast.service.UserService;
import cn.itcast.utils.CookieUtils;

public class autoLoginFilter implements Filter {

	public autoLoginFilter() {

	}

	public void destroy() {

	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {

		//先对servlet进行强转，让其能使用HttpServlet中的方法
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		
		User user = new User();
		//先判断是否有用户session存在,存在就直接放行，不存在则进else
		if(request.getSession().getAttribute("existUser") != null){
			chain.doFilter(request, response);
			System.out.println("session里有数据不用读cookie了");
		}else{
			//获取所有cookie
			Cookie[] cookies = request.getCookies();
			Cookie cookie = CookieUtils.getCookieByName(cookies, "autoLogin");
			//没有session判断是否有cookie,如果有     判断cookie内的数据是否在数据库中还有效
			if(cookie != null){
				System.out.println("读cookie中的数据");
				String value = cookie.getValue().toString();
				String[] split = value.split(":");
				user.setUsername(split[0]);
				user.setPassword(split[1]);
				System.out.println(split[0]);
				System.out.println(split[1]);
				User existUser = new UserService().login(user);
				if(existUser.getUsername() != null && existUser.getPassword() != null){
					//如果cookie正确放行,并将数据保存到session
					System.out.println("数据正确保存到session");
					request.getSession().setAttribute("existUser", existUser);
					chain.doFilter(request, response);
				}else{
					System.out.println("数据不正确，直接放行不进行登陆");
					chain.doFilter(request, response);
				}
			}else{
				System.out.println("cookie里没有数据");
				chain.doFilter(request, response);
			}
		}
	}

	public void init(FilterConfig fConfig) throws ServletException {

	}

}