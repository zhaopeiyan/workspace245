package cn.itcast.web.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import cn.itcast.domain.User;
import cn.itcast.service.UserService;

public class LoginServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//解决相应中文乱码
		response.setContentType("text/html;charset=utf-8");
		//解决post请求乱码，只对请求体有效
		request.setCharacterEncoding("utf-8");

		User user = new User();
		//1、从键盘获取数据
		try {
			BeanUtils.populate(user, request.getParameterMap());
		} catch (Exception e) {
			throw new RuntimeException("BeanUtils封装数据异常"+e);
		}
		//2、调用service层处理业务逻辑
		UserService us = new UserService();
		User existUser = us.login(user);
		//3、判断是否登陆成功
		//4、如果查询结果不为空，则将数据保存到cookie和session中,如果不为空则不进行任何操作
		if(existUser != null){
			if(request.getParameter("autoLogin") != null){
				//session
				request.getSession().setAttribute("existUser", existUser);
				//cookie
				System.out.println("保存到cookie和session");
				Cookie cookie = new Cookie("autoLogin",user.getUsername()+":"+user.getPassword());
				cookie.setMaxAge(60*60);
				response.addCookie(cookie);
			}
			request.setAttribute("existUser", existUser);
			response.sendRedirect("index.jsp");
		}else{
			System.out.println("++++++++++++++++++++++++++++++++=");
//			request.setAttribute("msg", "<h3 style='color:red'>登陆失败</h3>");
			request.setAttribute("msg","<h3 style='color:red'>用户名或密码错误</h3>");
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
