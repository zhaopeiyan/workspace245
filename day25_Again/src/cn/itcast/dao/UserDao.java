package cn.itcast.dao;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import cn.itcast.domain.User;
import cn.itcast.utils.C3P0Utils;

public class UserDao {

	public User login(User user) {
		User existUser = null;
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "select * from tbl_user where username=? and password=?";
		try {
			existUser = qr.query(sql, new BeanHandler<>(User.class), user.getUsername(),user.getPassword());
		} catch (Exception e) {
			throw new RuntimeException("查询数据异常" + e);
		}
		return existUser;
	}

}
