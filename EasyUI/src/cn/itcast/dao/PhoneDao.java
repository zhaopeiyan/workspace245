package cn.itcast.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import cn.itcast.domain.Phone;

public interface PhoneDao {

	void save(Phone phone);

	List<Phone> findAll(DetachedCriteria criteria, int startIndex, Integer rows);

	Integer findTotal(DetachedCriteria criteria);

	Phone findById(DetachedCriteria criteria);

	void update(Phone phone);

	void delete(Phone phone);

}
