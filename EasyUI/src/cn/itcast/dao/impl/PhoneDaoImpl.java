package cn.itcast.dao.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;

import cn.itcast.dao.PhoneDao;
import cn.itcast.domain.Phone;

public class PhoneDaoImpl extends HibernateDaoSupport implements PhoneDao{

	@Override
	public void save(Phone phone) {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().save(phone);
	}

	@Override
	public Integer findTotal(DetachedCriteria criteria) {
		// TODO Auto-generated method stub
		Long total = 0l;
		List<Long> list = (List<Long>) this.getHibernateTemplate().findByCriteria(criteria);
		if(list!=null&&list.size()>0){
			total = list.get(0);
		}
		return total.intValue();
	}


	@Override
	public List<Phone> findAll(DetachedCriteria criteria, int startIndex, Integer rows) {
		// TODO Auto-generated method stub
		return (List<Phone>) this.getHibernateTemplate().findByCriteria(criteria, startIndex, rows);
	}

	@Override
	public Phone findById(DetachedCriteria criteria) {
		// TODO Auto-generated method stub
		
		List<Phone> list = (List<Phone>) this.getHibernateTemplate().findByCriteria(criteria);
		if(list!=null&&list.size()>0){
			return list.get(0);
		}
		return null;
	}

	@Override
	public void update(Phone phone) {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().update(phone);
	}

	@Override
	public void delete(Phone phone) {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().delete(phone);
	}
}
