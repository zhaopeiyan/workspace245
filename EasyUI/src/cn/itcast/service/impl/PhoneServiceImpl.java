package cn.itcast.service.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;

import cn.itcast.dao.PhoneDao;
import cn.itcast.domain.Phone;
import cn.itcast.service.PhoneService;
import cn.itcast.vo.PageBean;

public class PhoneServiceImpl implements PhoneService {
	
	private PhoneDao phoneDao;
	public void setPhoneDao(PhoneDao phoneDao) {
		this.phoneDao = phoneDao;
	}
	
	@Override
	public void save(Phone phone) {
		// TODO Auto-generated method stub
		phoneDao.save(phone);
	}

	@Override
	public PageBean<Phone> findAll(Integer page, Integer rows) {
		// TODO Auto-generated method stub
		PageBean<Phone> pageBean = new PageBean<>();
		DetachedCriteria criteria = DetachedCriteria.forClass(Phone.class);
		criteria.setProjection(Projections.rowCount());

		Integer total = phoneDao.findTotal(criteria);
		pageBean.setTotal(total);
		criteria.setProjection(null);
		int startIndex = (page - 1) * rows;
		
		List<Phone> list = phoneDao.findAll(criteria,startIndex,rows);
		pageBean.setRows(list);
		return pageBean;
	}

	@Override
	public Phone findById(DetachedCriteria criteria) {
		// TODO Auto-generated method stub
		return phoneDao.findById(criteria);
	}

	@Override
	public void update(Phone phone) {
		// TODO Auto-generated method stub
		phoneDao.update(phone);
	}

	@Override
	public void delete(Phone phone) {
		// TODO Auto-generated method stub
		phoneDao.delete(phone);
	}
	
	
}
