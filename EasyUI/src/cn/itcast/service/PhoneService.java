package cn.itcast.service;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import cn.itcast.domain.Phone;
import cn.itcast.vo.PageBean;

public interface PhoneService {

	void save(Phone phone);

	PageBean<Phone> findAll(Integer page, Integer rows);

	Phone findById(DetachedCriteria criteria);

	void update(Phone phone);

	void delete(Phone phone);
	
}
