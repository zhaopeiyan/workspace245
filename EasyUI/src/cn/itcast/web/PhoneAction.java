package cn.itcast.web;

import java.io.IOException;
import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.domain.Phone;
import cn.itcast.service.PhoneService;
import cn.itcast.vo.PageBean;
import net.sf.json.JSONArray;
import net.sf.json.JSONString;
import net.sf.json.util.JSONStringer;

public class PhoneAction extends ActionSupport implements ModelDriven<Phone> {

	private PhoneService phoneService;
	public void setPhoneService(PhoneService phoneService) {
		this.phoneService = phoneService;
	}

	private Integer page;
	private Integer rows;
	public void setPage(Integer page) {
		this.page = page;
	}
	public void setRows(Integer rows) {
		this.rows = rows;
	}
	
	public String delPhone() throws IOException{
		System.out.println("======================================"+phone.getPid());
		boolean flag = true;
		try {
			phoneService.delete(phone);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			throw new RuntimeException("" + e);
		}
		ServletActionContext.getResponse().getWriter().write(flag+"");
		
		return NONE;
	}
	
	public String update() throws IOException{
		
		boolean flag = true;
		try {
			phoneService.update(phone);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			throw new RuntimeException("" + e);
		}
		ServletActionContext.getResponse().getWriter().write(flag+"");
		return NONE;
	}
	
	public String edit() throws IOException{
		
		DetachedCriteria criteria = DetachedCriteria.forClass(phone.getClass());
		criteria.add(Restrictions.eq("pid", phone.getPid()));
		
		Phone existPhone = phoneService.findById(criteria);
		
		/*JSONArray jsonArray = JSONArray.fromObject(existPhone);
		System.out.println("=====================================================");
		System.out.println(jsonArray.toString());
		System.out.println("=====================================================");
		ServletActionContext.getResponse().setContentType("text/html;charset=utf-8");
		ServletActionContext.getResponse().getWriter().write(jsonArray.toString());*/
		
		Gson gson = new Gson();
		String json = gson.toJson(existPhone);
		System.out.println("=====================================================");
		System.out.println(json);
		System.out.println("=====================================================");
		ServletActionContext.getResponse().setContentType("text/html;charset=utf-8");
		ServletActionContext.getResponse().getWriter().write(json);
		
		return NONE;
	}
	
	public String save() throws IOException{
		boolean flag = true;
		try {
			phoneService.save(phone);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
			throw new RuntimeException("" + e);
		}
		ServletActionContext.getResponse().getWriter().write(flag+"");
		return NONE;
	}
	
	public String findAll() throws IOException{
		PageBean<Phone> pageBean = phoneService.findAll(page,rows);
		
		/*JSONArray jsonArray = JSONArray.fromObject(pageBean);
		System.out.println("============================================================");
		System.out.println(jsonArray.toString());
		System.out.println("============================================================");
		ServletActionContext.getResponse().setContentType("text/html;charset=utf-8");
		ServletActionContext.getResponse().getWriter().write(jsonArray.toString());*/
		Gson gson = new Gson();
		String json = gson.toJson(pageBean);
		System.out.println("=====================================================");
		System.out.println(json);
		System.out.println("=====================================================");
		ServletActionContext.getResponse().setContentType("text/html;charset=utf-8");
		ServletActionContext.getResponse().getWriter().write(json);
		
		return NONE;
	}

	private Phone phone = new Phone();;
	@Override
	public Phone getModel() {
		// TODO Auto-generated method stub
		return phone;
	}

}
