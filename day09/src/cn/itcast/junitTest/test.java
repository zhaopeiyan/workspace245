package cn.itcast.junitTest;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

import cn.itcast.jdbcutils.jdbcUtils;

public class test {
	@Test
	public void demo1() {
		Connection conn = null;
		Statement st = null;
		ResultSet ra = null;

		try {
			// 获取连接
			conn = jdbcUtils.getConn();
			System.out.println(conn);
			// 获得语句执行者
			st = conn.createStatement();
			
			//执行语句
			int i = st.executeUpdate("insert into category(cid,cname) values ('c012','家电')");
			
			if(i > 0){
				System.out.println("更新成功！");
			}else{
				System.out.println("更新失败！");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
