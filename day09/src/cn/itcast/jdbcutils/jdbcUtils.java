package cn.itcast.jdbcutils;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/*
 * 获取连接
 * 
 */
public class jdbcUtils {
	
	private static String driver;
	private static String url;
	private static String user;
	private static String pwd;

	static {
		try {
			// 加载.properties文件 获得流InputStream
			InputStream is = jdbcUtils.class.getClassLoader().getResourceAsStream("jdbcInfo.properties");
			// 使用Properties处理流
			// 使用load方法加载指定的流
			Properties props = new Properties();

			props.load(is);
			is.close();
			
			// 使用getProperties(key),通过key获得需要的值
			driver = props.getProperty("driver");
			url = props.getProperty("url");
			user = props.getProperty("user");
			pwd = props.getProperty("pwd");
		
		} catch (Exception e) {
			throw new RuntimeException("读取配置文件失败" + e);
		}
	}

	// 注册驱动
	static {
		try {
			Class.forName(driver);
			System.out.println(driver +";"+ url +";"+ user +";"+ pwd);
		} catch (Exception e) {
			throw new RuntimeException("注册驱动失败！" + e);
		}
	}

	// 获得链接
	public static Connection getConn() {
		try {
			return DriverManager.getConnection(url, user, pwd);
		} catch (Exception e) {
			throw new RuntimeException("获取数据库连接失败！" + e);
		}
	}
}
