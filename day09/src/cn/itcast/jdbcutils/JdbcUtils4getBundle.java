package cn.itcast.jdbcutils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;

public class JdbcUtils4getBundle {
	
	private static String driver;
	private static String url;
	private static String user;
	private static String password;
	
	static {
		try {
			//1 加载配置文件,只能处理properties文件，只需要编写文件名，不需要扩展名
			ResourceBundle bundle = ResourceBundle.getBundle("jdbcInfo");
			
			//3 通过key获得value
			driver = bundle.getString("jdbc.driver");
			url = bundle.getString("jdbc.url");
			user = bundle.getString("jdbc.user");
			password = bundle.getString("jdbc.password");
			
			//4 注册驱动
			Class.forName(driver);
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}
	
	public static void main(String[] args) {
		System.out.println("123");
	}
	
	/**
	 * 获得连接
	 * @return
	 */
	public static Connection getConnection(){
		try {
			Connection conn = DriverManager.getConnection(url,user,password);
			return conn;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 释放资源
	 * @param conn
	 * @param st
	 * @param rs
	 */
	public static void closeResource(Connection conn ,Statement st , ResultSet rs ){
		try {
			if (rs != null) {
				rs.close();
			} 
		} catch (Exception e) {
			
		}
		
		try {
			if (st != null) {
				st.close();
			} 
		} catch (Exception e) {
			
		}
		
		try {
			if (conn != null) {
				conn.close();
			} 
		} catch (Exception e) {
			
		}
	}

}
