package cn.itcast.jdbcutils;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class jdbcUtils4ClassLoader {

	private static String driver;
	private static String url;
	private static String user;
	private static String pwd;

	static {
		// 读取配置文件
		try {
			Properties props1 = new Properties();
			//用次方式读取配置文件时，配置文件必须要放在src文件夹下
			InputStream is = jdbcUtils4ClassLoader.class.getClassLoader().getResourceAsStream("jdbcInfo.properties");
			props1.load(is);
			is.close();
			
			driver = props1.getProperty("driver");
			url = props1.getProperty("url");
			user = props1.getProperty("user");
			pwd = props1.getProperty("pwd");

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("读取配置文件失败" + e);
		}
	}
	
/*	static { // 静态代码块.类一加载进内,代码就被执行
		// 1.注册驱动
		try {
			Properties prop = new Properties(); //读取Properties文件的工具类
			  prop.load(new FileReader("jdbc.properties"));//加载配置文件
			//System.out.println(prop.getProperty("driver"));;
			 			
			
			//固定写法:JDBCUtils4Prop2.class.getClassLoader() 表示获取当前类对象的类加载器
			InputStream in = jdbcUtils4Class.class.getClassLoader().getResourceAsStream("jdbcInfo.properties");
			Properties prop = new Properties();
			prop.load(in);
			
			System.out.println("++++++++++++++++++");
			String driver = prop.getProperty("driver");
			url = prop.getProperty("url");
			user = prop.getProperty("name");
			pwd = prop.getProperty("pwd");
			Class.forName(driver);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}*/

	// 注册驱动
	static {

		try {
			Class.forName(driver);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("注册驱动失败" + e);
		}
	}

	public static Connection getConn() {
		// 获取数据库连接
		try {
			return DriverManager.getConnection(url, user, pwd);
		} catch (Exception e) {
			throw new RuntimeException("获取数据库链接失败"+e);
		}
	}
	
	public static void closeResource(){
		
	}
}
